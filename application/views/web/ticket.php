<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>.:SiMASTER - Sistem Informasi Management Aset IT & Support Center:.</title>
<link href="<?php echo base_url('assets/images/icon.png'); ?>"rel='shortcut icon' type='image/x-icon'/>
<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" media="all">
<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url('assets/jui/css/jquery-ui.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/jquery-ui.custom.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.css'); ?>" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('plugins/uniform/css/uniform.default.css'); ?>" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="<?php echo base_url('plugins/ibutton/jquery.ibutton.css'); ?>" media="screen">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/icomoon/style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main-style.css'); ?>" media="screen">


<!-- CLEditor -->
<link rel="stylesheet" href="<?php echo base_url('plugins/cleditor/jquery.cleditor.css'); ?>" media="screen">

<script type="text/javascript" src="<?php echo base_url('assets/web/jQuery-2.1.3.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  $("#kategori").change(function(){
      load_inv();
        });  
  });
$(document).ready(function(){
    $(".combobox").combobox();
});

function load_inv(){ 
    var group = $('#group').val();  
    var kategori=$("#kategori").val();
    $.ajax({
        url:"<?php echo base_url('web/tampil_inv');?>",
        data: "kategori=" + kategori+"&group="+group,        
        type  : 'GET',
        success: function(html) { 
           $("#inventaris").html(html);       
        }
    });
}
</script>
</head>
<body>

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="<?php echo base_url('web');?>">
								<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 150px; height: 25px;">
								     
							</a>
						</div>
						 <h4 class="box-title"><?php echo tanggal_new() ;?></h4>
					</div>
					
                    <div id="header-right" class="clearfix">      
					
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                             	<div class="thumbnail">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="<?php echo base_url('assets/images/menu.png'); ?>" alt="" style="width:90px; height: 30px;">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
										<li><a href="<?php echo base_url('web');?>"><i class="icon-home"></i><b> Frontend</b></a></li>
                                    	<li><a href="<?php echo base_url('web/profile');?>"><i class="icon-user"></i><b> My Profile</b></a></li>  					
                                        <li class="divider"></li>
                                        <li><a href="<?php echo base_url('login');?>"><i class="icon-key"></i><b> Login</b></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        

		<div id="content-wrap">
        	<div id="content-outer">
            	<div id="content-outer">
                	<div id="content-outer">
                        <div id="sidebar-separator"></div>
                        <section  id="main-content" class="clearfix">
                            <div class="page-header">
							<h2 align="center"><span class="semi-bold" >SIstem Informasi Managemen Aset IT & Support Center</span></h2>
									   <h3  align="center"><span class="title" style="color:blue "> <?php
									foreach ($group as $row) {
										echo "<div value='$row->gid'>".strtoupper($row->nama_pt)."</div>"; 
										echo "<div value='$row->gid'>".strtoupper($row->nama_group)."</div>";}
											?>
									</span></h3>
							</div>
      
								  <div class="btn-group"> <a href="<?php echo base_url('web');?>" class="btn btn-large btn-success">Home</a> </div>   
							  <div class="btn-group"> <a href="<?php echo base_url('login');?>" class="btn btn-large btn-primary">Login</a> </div> 
							  
							  <p>
	  
							<div class="row-fluid">
                                <div class="span6 widget">
                                    	<div class="widget-header"><span class="title">Permohonan Perbaikan Aset IT</span></div>
                                    <div class="widget-content">
										<br>									
										<p>Buat Permohonan Perbaikan untuk Inventaris Anda, Masukan Nomer Inventaris dan berikan informasi permasalahan yang anda anda alami pada penggunaan aset IT.</p>
										<br>
										<?php
										echo form_open('web/addticket');
										?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-vertical" method="post">
												<fieldset>
													<legend>Account Info</legend>
													<div class="control-group">
														<label class="control-label">Group Inventory <span class="required">*</span></label>
														<div class="controls">
														   <select name="group" type="text" class="span6" id="group">                    
													  <?php
														if (!empty($group)) {
																		   foreach ($group as $row) {
															echo "<option value='$row->gid'>".strtoupper($row->nama_group)."</option>";
														  }
														}
													  ?>
													</select>                             
													<?php echo form_error('group', '<div class="text-blue">', '</div>'); ?>	
														</div>
													</div>
													<div class="control-group">
													<label class="control-label">Nama Pemohon <span class="required">*</span></label>
														<div class="controls">
															  <div class="">
																<div type="text" class="span8">
																  <input type="text" onkeyup="this.value = this.value.toUpperCase()" name="pemohon" class="form-control" required oninvalid="setCustomValidity('Nama Pemohon masih kosong')" oninput="setCustomValidity('')" placeholder="Masukan nama anda" >
																</div>
															  </div><br><br>
															  <?php echo form_error('pemohon', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
															<label class="control-label">Level <span class="required">*</span></label>
															<div class="controls">
														 <select name="level"  class="span4 select2-select" id="level"> 
															<option value="" selected="selected">- Level Priority -</option>
															<option value="LOW" style="color:blue">LOW</option> 
															<option value="NORMAL" style="color:green" >NORMAL</option> 
															<option value="HIGH" style="color:red">HIGH</option> 
														  </select>                        
														</div> 
														<?php echo form_error('level', '<div class="text-red">', '</div>'); ?>                      
													</div>
													
												</fieldset>

													<fieldset>
													<legend>Asset Info</legend>
													<div class="control-group">
													<label class="control-label">Type Inventory <span class="required">*</span></label>
													<div class="controls">
													<select name="kategori"  class="span6 select2-select" id="kategori" >  
														<option value="" selected="selected">- Jenis Inventaris -</option>                 
														<option value="Laptop">LAPTOP</option> 
														<option value="Komputer">KOMPUTER</option> 
														<option value="Monitor">MONITOR</option> 
														<option value="Printer">PRINTER</option> 
														<option value="Network">NETWORK DEVICE</option> 
													</select>                             
													<?php echo form_error('kategori', '<div class="text-blue">', '</div>'); ?>   
													<label for="chosen1" generated="true" class="error" style="display:none;"></label>					
												  </div>	               	
													</div>
													
														<div class="control-group">
															<label class="control-label">No.Inventory <span class="required">*</span></label>
															<div class="controls">
														 <select name="inventaris"  class="span6" id="inventaris"> 
															<option value="" selected="selected">- No. Inventaris -</option>
														  </select>                        
														</div> 
														<?php echo form_error('inventaris', '<div class="text-red">', '</div>'); ?>                      
													</div>
													
													
													</fieldset>
													
													<fieldset >
													<legend>Problem Info</legend>

													<div class="control-group">
														<label class="control-label">Detail Permasalahan <span class="required">*</span></label>
														<div class="controls">
													
														 <textarea name="catatan" class="span12" placeholder=" Isi detail permasalahan" required oninvalid="setCustomValidity('Catatan Pemohon Harus di Isi !')"
														  oninput="setCustomValidity('')" id="cleditor"></textarea>
												
														<?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
													  </div><br>
															<label class="checkbox">
                                                                <input type="checkbox" id="souvenirs" name="souvenirs" class="uniform"> Ya, Kirim Ticket Saya.
                                                            </label>
													</div>
												</fieldset>
												<div class="form-actions">
													<button type="reset" name="submit" class="btn btn-warning pull-left"  > <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
													<button type="submit" name="submit" class="btn btn-success pull-right"  > <i class="icon-paper-airplane" aria-hidden="true" ></i> Kirim Permintaan</button>
											
												</div>
											</form>
										</div>		
									</div>
                                </div>
								
								 <div class="span6 widget">
                                    	<div class="widget-header"><span class="title">Total Ticket Perbaikan </span>
											<div class="toolbar">
												<div class="btn-group">
													<span class="btn"><i class="icon-tag"></i>  : <?php echo $no_permohonan; ?> Ticket Masuk</span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
													<span class="btn" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
												</div>
											</div>
										</div>
										
										<div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span12" name="q" value="">
															<span class="input-group-btn">
															</span>
														</div>
													</form>
                                            </div>
                                        </div>	
							
										<div class="widget-header"><span class="title">List Open Ticket</span>  
												<div class="toolbar">
												<span class="btn" ><i class="icon-tag"  style="color:white" ></i> <font color="white">:5 Ticket Open</font></span>
											</div>
										</div>	
										
										<div class="widget-content news">
											<ul class="thumbnails">												 
											<?php 
											  if ($ticket->num_rows()>0){
												foreach ($ticket->result() as $key =>$r) {
												  echo '
												<li>
													<div class="head">
														<h2 class="title"><font color="purpel">
														<span><b><font color="black">'.$r->no_permohonan.'</font></b></span> |
														<span>'.strtoupper($r->nama_pemohon),' / ',$r->no_inventaris.'</font></span></h2>
														<span>Status : <b><font color="red">'.$r->status.'</font></b></span> | 
														<span>Masuk : <b><font color="black">'.$r->tgl_permohonan.'</font></b></span> - 
														<span>Rating : </span><i class="icon-star" style="color:green"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i> |
														<span>Level : <b><font color="black">'.$r->level.'</font></b>
														
														
														</span> 
													</div>
														<a href="'.site_url('web/openticket/'.$r->no_permohonan).'" >
													<div class="thumbnail" >
													
															<img src='.base_url('assets/images/'.$r->gambar).' style="width: 70px; height: 40px;">  
          									
													</div></a>
													<div class="info">
														 <p><span><b><font color="black">'.$r->catatan_pemohon.'</font></b></span></p>
														 <p><span> Catatan perbaikan : <b><font color="red"> Menunggu respon Staff IT</font></b></span></p>
													</div> 
														
														  ';
														}
														echo $paging;
													  }else{
														echo'
														  <div class="alert alert-danger alert-dismissable">                
															<h4><i  class="icon-ok"></i> <b>Status Open Ticket Masih Kosong !</h4></b>
														  </div>
														';
													  }
													  ?>	
												</li>
											</ul>		
										</div>
												
										
											
									
											<div class="widget-header"><span class="title">List Ticket Prosses</span>  
													<div class="toolbar">
													<span class="btn" ><i class="icon-tag"  style="color:white" ></i> <font color="white">: 5 Ticket Open</font></span>
												</div>
											</div>		
											<div class="widget-content news">
												<ul class="thumbnails">													 
												<?php 
												  if ($ticketprogress->num_rows()>0){
													foreach ($ticketprogress->result() as $key =>$r) {
													  echo '
														<li>
														<div class="head">
															<h2 class="title"><font color="purpel">
															<span><b><font color="black">'.$r->no_permohonan.'</font></b></span> |
															<span>'.strtoupper($r->nama_pemohon),' / ',$r->no_inventaris.'</font></span></h2>
															<span>Status : <b><font color="orange">'.$r->status.'</font></b></span> | 
															<span>Masuk : <b><font color="black">'.$r->tgl_permohonan.'</font></b></span> - 
															<span>Rating : </span><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i><i class="icon-star"></i><i class="icon-star"></i> |
															<span>Level : <b><font color="black">'.$r->level.'</font></b></span> 
														</div>
														<a href="'.site_url('web/openticket/'.$r->no_permohonan).'">
														<div class="thumbnail">
															<img src='.base_url('assets/images/'.$r->gambar).' style="width: 70px; height: 40px;">  
														</div></a>
														<div class="info">
															 <p><span><b><font color="black">'.$r->catatan_pemohon.'</font></b></span></p>
															 <p><span> Catatan perbaikan : <b><font color="orange">'.$r->catatan_perbaikan.'</font></b></span></p>
														</div> 
															
															  ';
															}
															echo $paging;
														  }else{
															echo'
															  <div class="alert alert-default alert-dismissable">                
																<h4><i  class="icon-ok"></i> <b>Status Ticket On Prosses Masih Kosong !</h4></b>
															  </div>
															';
														  }
														  ?>	
													</li>
												</ul>		
											</div>
											
										<div class="widget-header"><span class="title">List Close Ticket</span>  
												<div class="toolbar">
													<span class="btn" ><i class="icon-tag"  style="color:white" ></i> <font color="white">: 5 Ticket Close</font></span>
												</div>
										</div>		
										<div class="widget-content news">
											<ul class="thumbnails">													 
											<?php 
											  if ($ticketclose->num_rows()>0){
												foreach ($ticketclose->result() as $key =>$r) {
												  echo '
												<li>
													<div class="head">
														<h2 class="title"><font color="purpel">
														<span><b><font color="black">'.$r->no_permohonan.'</font></b></span> |
														<span>'.strtoupper($r->nama_pemohon),' / ',$r->no_inventaris.'</font></span></h2>
														<span>Status : <b><font color="green">'.$r->status.'</font></b></span> | 
														<span>Selesai : <b><font color="black">'.$r->tgl_selesai.'</font></b></span> - 
														<span>Rating : </span><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i><i class="icon-star" style="color:green"></i> |
														<span>Level : <b><font color="black">'.$r->level.'</font></b></span> 
													</div>
													<a href="'.site_url('web/openticket/'.$r->no_permohonan).'" >
													<div class="thumbnail">
														<img src='.base_url('assets/images/'.$r->gambar).' style="width: 70px; height: 40px;" >  
													</div></a>
													<div class="info">
														 <p><span><b><font color="black">'.$r->catatan_pemohon.'</font></b></span></p>
														 <p><span> Catatan perbaikan : <b><font color="green"> '.$r->catatan_perbaikan.'</font></b></span></p>
													</div> 
														
														  ';
														}
														echo $paging;
													  }else{
														echo'
														  <div class="alert alert-success alert-dismissable">                
															<h4><i  class="icon-ok"></i> <b>Status Close Ticket Masih Kosong !</h4></b>
														  </div>
														';
													  }
													  ?>	
														
													</div>
												</li>
											</ul>
										</div>
										
									</div> 
								
                            </div>
                        </section>
					</div>
				</div>	
	</div>	
					<footer id="footer">
					<div class="footer-left">Copyright&nbsp;&copy; 2019. All Rights Reserved. EKO SETIAWAN - 1115R0532</div>
					<div class="footer-right"><p><b>SiMASTER</b> <i> Version 1.1 Build 010719</i></p></div>
					</footer>



<!-- Core Scripts -->

	<script src="<?php echo base_url('assets/js/libs/jquery-1.8.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.placeholder.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.mousewheel.min.js'); ?>"></script>
    
    <!-- Template Script -->
    <script src="<?php echo base_url('assets/js/template.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/setup.js'); ?>"></script>

    <!-- Customizer, remove if not needed -->
    <script src="<?php echo base_url('assets/js/customizer.js'); ?>"></script>

    <!-- Uniform Script -->
    <script src="<?php echo base_url('plugins/uniform/jquery.uniform.min.js'); ?>"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="<?php echo base_url('assets/jui/js/jquery-ui-1.9.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/jquery-ui.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/jui/jquery.ui.touch-punch.min.js'); ?>"></script>
    
    <!-- Plugin Scripts -->
   
    <!-- iButton -->
    <script src="<?php echo base_url('plugins/ibutton/jquery.ibutton.min.js'); ?>"></script>
     
    <!-- Demo Scripts -->
    <script src="<?php echo base_url('assets/js/demo/boxes.js'); ?>"></script>


 <!-- CLEditor -->
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.icon.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.table.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.xhtml.min.js'); ?>"></script>
	<!-- Demo Scripts -->
    <script src="<?php echo base_url('assets/js/demo/wysiwyg.js'); ?>"></script>

</body>

</html>
