<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>.:SiMASTER - Sistem Informasi Management Aset IT & Support Center:.</title>
<link href="<?php echo base_url('assets/images/icon.png'); ?>"rel='shortcut icon' type='image/x-icon'/>
<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url('assets/jui/css/jquery-ui.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/jquery-ui.custom.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.css'); ?>" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('plugins/uniform/css/uniform.default.css'); ?>" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="<?php echo base_url('plugins/ibutton/jquery.ibutton.css'); ?>" media="screen">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/icomoon/style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main-style.css'); ?>" media="screen">


<!-- CLEditor -->
<link rel="stylesheet" href="<?php echo base_url('plugins/cleditor/jquery.cleditor.css'); ?>" media="screen">

</head>
<body>

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="<?php echo base_url('web');?>">
								<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 150px; height: 25px;">
							</a>
						</div>
					</div>
                    <div id="header-right" class="clearfix">                     
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                             	<div class="thumbnail">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="<?php echo base_url('assets/images/menu.png'); ?>" alt="" style="width:90px; height: 30px;">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
										<li><a href="<?php echo base_url('web');?>"><i class="icon-home"></i><b> Frontend</b></a></li>
                                    	<li><a href="<?php echo base_url('web/profile');?>"><i class="icon-user"></i><b> My Profile</b></a></li>  							
                                        <li class="divider"></li>
                                        <li><a href="<?php echo base_url('login');?>"><i class="icon-key"></i><b> Login</b></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        


<div id="content-wrap">
		
			<div id="content-outer">
		
								<div class="row-fluid">
											<?php 
											echo form_open('web/addcomment'); 
											?>  
	                                <div class="span12 widget">
                                        <div class="widget-header">
                                            <span class="title"><i class="icon-tag"></i> Detail Ticket Open Lice Chat</span>
											<div class="toolbar">
												<span class="btn" ><i class="icon-tag"  style="color:white" ></i> <font color="white"> Status Ticket :  <?php echo $this->uri->segment(2);?> 
												</font></span>
												</div>
                                        </div>
                                        <div class="widget-content form-container">
                                                <fieldset>
                                                    <legend><h3 ><span class="title" class="semi-bold">Ticket # <?php echo $this->uri->segment(3);?></span></h3></legend>
                                                    <div class="alert alert-info">
                                                        Silahkan melakukan live chat dan menanyakan apa yang menjadi masalah anda via live chat dibawah ini.
                                                    </div>
                                                    <div class="control-group">
                                                       
                                                    </div>
                                                    <div class="control-group">
													<div class="open_ticket_rating pull-left">Current Rating on this Ticket </div>
                                                        <div class="rating">
																		<i class="" style="color:green"></i> 
																		<i class="" style="color:green"></i> 
																		<i class="" style="color:green"></i> 
																		<i class="" ></i> 
																		<i class="" ></i> <span style="color:green"> : 3 (Pretty Good)</span>
																		</div>
																		
                                                    </div>
													<div class="control-group">
													
                                                        <div class="rating">
																		<i class="icon-heart" style="color:green"></i> 
																		<i class="icon-heart" style="color:green"></i> 
																		<i class="icon-heart" style="color:green"></i> 
																		<i class="icon-heart ratingcolor" ></i> 
																		<i class="icon-heart ratingcolor"></i> 
																		</div>
																
																		
																		<i class="" ></i> <span style="color:black">Average Respones Speed : </span>
																		<div class="rating">
																		<i class="icon-time"  ></i>
																		<span style="color:red" > 36.9 Minutes</span></div>
                                                    </div>	

													
                                                </fieldset>
                         
                                       
                                        </div>
                                    </div>
                                </div>
		
		


									
									
							<div class="row-fluid">
								<div class="span12">
										<div class="widget">
                                            <div class="widget-header">
                                            <span class="title">
                                                <i class="icon-comments"></i> History Proses Perbaikan
                                            </span>
                                            <div class="toolbar">
                                                <span class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </span>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#"><i class="icol-lightbulb"></i> Available</a></li>
                                                    <li><a href="#"><i class="icol-cross-shield-2"></i> Busy</a></li>
                                                    <li><a href="#"><i class="icol-clock"></i> Away</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icol-disconnect"></i> Disconnect</a></li>
                                                </ul>
                                            </div>
                                        </div>
												
											 
                                            <div class="widget-content chat-box">
                                            	<ul class="thumbnails">
												<?php 
											if(!empty($detail)){
												foreach ($detail->result() as $r) {
													if($r->user=="Admin"){
												  echo '
	                                                <li class="others">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/icon.png').' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="ticket_problem">'.$r->catatan.'</div></strong></p>
															<p>Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</p>
	                                                    </div>
	                                                </li>
													';
													}else {
														echo '
													
													<li class="me">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/'.$r->foto).' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="ticket_problem">'.$r->catatan.'</div></strong></p>
															<p><div class="time">Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</div></p>
	                                                    </div>
	                                                </li>
													'; }
												  }
												}              
											  ?>   
								</ul>
								
								
								

                                                <div class="message-form">
                                                    <div class="row-fluid">
																<div class="control-group">
																	<label class="control-label">Komentar <span class="required">*</span></label>
																	<div class="controls">
																
																	 <textarea name="catatan" class="span12" placeholder="Balas pesan anda disini atau tambahkan catatan" 
																	 required oninvalid="setCustomValidity('Komentar harus di Isi sebelum dikirim!')"
																	  oninput="setCustomValidity('')" ></textarea>
																	  
																 <input type="hidden" name="kode" value="<?php echo $this->uri->segment(3);?>">
																 
																	<?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
																  </div><br>
																</div>
															  
															<div class="form-actions">
															
																<a href="<?php echo site_url('web'); ?>" name="submit" class="btn ticket_btn pull-right"><i class=" icon-bended-arrow-left" aria-hidden="true"></i> Kembali</a> 
																<button type="reset" name="submit" class="btn btn-warning pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
																<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-paper-airplane" aria-hidden="true" ></i> Kirim</button>
															</div>
															</div>
														
													</div>
												</div>
                                            </div>
                                        </div>
										
							</div>
							
							
							
			</div>

		</div>













					<footer id="footer">
					<div class="footer-left">Copyright&nbsp;&copy; 2019. All Rights Reserved. EKO SETIAWAN - 1115R0532</div>
					<div class="footer-right"><p><b>SiMASTER</b> <i> Version 1.1 Build 010719</i></p></div>
					</footer>
 </div>



<!-- Core Scripts -->

	<script src="<?php echo base_url('assets/js/libs/jquery-1.8.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.placeholder.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.mousewheel.min.js'); ?>"></script>
    
    <!-- Template Script -->
    <script src="<?php echo base_url('assets/js/template.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/setup.js'); ?>"></script>

    <!-- Customizer, remove if not needed -->
    <script src="<?php echo base_url('assets/js/customizer.js'); ?>"></script>

    <!-- Uniform Script -->
    <script src="<?php echo base_url('plugins/uniform/jquery.uniform.min.js'); ?>"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="<?php echo base_url('assets/jui/js/jquery-ui-1.9.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/jquery-ui.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/jui/jquery.ui.touch-punch.min.js'); ?>"></script>
    
    <!-- Plugin Scripts -->
   
    <!-- iButton -->
    <script src="<?php echo base_url('plugins/ibutton/jquery.ibutton.min.js'); ?>"></script>
     
    <!-- Demo Scripts -->
    <script src="<?php echo base_url('assets/js/demo/boxes.js'); ?>"></script>





 
 <!-- CLEditor -->
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.icon.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.table.min.js'); ?>"></script>
    <script src="<?php echo base_url('plugins/cleditor/jquery.cleditor.xhtml.min.js'); ?>"></script>
	<!-- Demo Scripts -->
    <script src="<?php echo base_url('assets/js/demo/wysiwyg.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/common-script.js'); ?>"></script>
</body>

</html>
