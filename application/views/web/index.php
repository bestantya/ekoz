<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>.:SiMASTER - Sistem Informasi Management Aset IT & Support Center:.</title>
<link href="<?php echo base_url('assets/images/icon.png'); ?>" rel='shortcut icon' type='image/x-icon'/>
<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url('assets/jui/css/jquery-ui.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/jquery-ui.custom.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.css'); ?>" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('plugins/uniform/css/uniform.default.css'); ?>" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="<?php echo base_url('plugins/ibutton/jquery.ibutton.css'); ?>" media="screen">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/icomoon/style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main-style.css'); ?>" media="screen">


</head>
<body>

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="<?php echo base_url('web');?>">
								<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 150px; height: 25px;">
							</a>
						</div>
					</div>
                    <div id="header-right" class="clearfix">                     
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                             	<div class="thumbnail">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="<?php echo base_url('assets/images/menu.png'); ?>" alt="" style="width:90px; height: 30px;">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
										<li><a href="<?php echo base_url('#');?>"><i class="icon-home"></i><b> Frontend</b></a></li>
                                    	<li><a href="<?php echo base_url('web/profile');?>"><i class="icon-user"></i><b> My Profile</b></a></li>                                   
                                        <li class="divider"></li>
                                        <li><a href="<?php echo base_url('login');?>"><i class="icon-key"></i><b> Login</b></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
		
		<div id="content-wrap">
        	<div id="content-outer">
            	<div id="content-outer">
                	<div id="content-outer">
                        <div id="sidebar-separator"></div>
                        <section  id="main-content" class="clearfix">
							<h4 class="title"><?php echo tanggal_new() ;?></h4>
                        	<div id="main-header" class="page-header">
                                
                                <ul><h3><span class="title"><b>SiMASTER | </b><span style="color:silver"> Sistem Informasi Management Aset IT & Support Center </span></h4></h3></span></ul>
                                
                            </div>
                            <div class="page-header">
							
							<h4 align="center"><p><b>Kini telah tersedia aplikasi pengaduan gangguan Komputer, Laptop, Printer dan perangkat IT lainnya.</p>
							<p>Anda mengisi permintaan perbaikan disini, kami akan secepatnya memproses permohonan anda!</b></p><h4>
							<h2 align="center"><p>PILIH KANTOR ATAU LOKASI GEDUNG ANDA :</b></p><h2>
							</div>
								<?php 
          						 if (!empty($group)){
          							foreach ($group as $g) {
          							 echo '
			  
									<div class="span3 widget">
										<div class="widget-header">
											<span class="title">'.$g->nama_group.'</span>
										</div>
										
										<a href='.base_url('web/ticket/'.$g->gid).'>
										<div class="widget-content user" align="center">
											<div class="thumbnail" >
          										  <img src='.base_url('assets/images/'.$g->logo).' style="width: 210px; height: 200px;">         										
											</div></a>
											<span align="left"><h4><i class="icos-marker"></i> '.$g->alamat.' </span><p>
													<p><span><i class="icos-home"></i> '.$g->nama_alias.'</span></p></h4>
											<div class="info">											
												
													
												<div></div>
											</div>
										</div>
									
									</div>
										';
          							}
          						 } 
          						 ?>
			  
                        </section>
							</div>
									<div class="widget">
                                            <div class="widget-header">
                                                <span class="title">PENGUMAMAN dan INFORMASI</span>
                                                <div class="toolbar">
                                                    <div class="btn-group">
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
													<span class="btn" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
												</div>
                                                </div>
                                            </div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                                <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span12" name="q" value="">
															<span class="input-group-btn">
															</span>
														</div>
													</form>
                                            </div>
                                            </div>
                                            <div class="widget-content news">
											<ul class="thumbnails">
												<li>
													<div class="head">
														<h2 class="title"><a href="">Download Mozilla Firefox 69.0.1 (64-bit) -Windows </a></h2>
														<span class="date">Oktober 06, 2019</span> <span class="tags"><a href="#">Technology</a>, <a href="#">Browser</a></span>
														
													</div>
													<div class="thumbnail">
														<img src="<?php echo base_url('assets/images/artikel/Firefox Logo.png'); ?>" >
													</div>
													<div class="info">
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
														<p>Link Download : <b><a target='_blank' href="https://mozilla-firefox.id.uptodown.com/windows">https://mozilla-firefox.id.uptodown.com/windows </a></b></p>
														<p>Penulis : <b>Administrator</b></p>
													</div>
												</li>
												<li>
													<div class="head">
														<h2 class="title"><a href="">Pemberitahuan penggunaan aplikasi ERP</a></h2>
														<span class="date">September 04, 2019</span> <span class="tags"><a href="#">Pemberitahuan</a>, <a href="#">ERP</a></span>
													</div>
													<div class="thumbnail">
														<img src="<?php echo base_url('assets/images/artikel/erp.jpg'); ?>" >
													</div>
													<div class="info">
														<p>ERP adalah singkatan dari Enterprise Resource Planning yang dalam bahasa Indonesianya sering disebut dengan Perencanaan Sumber Daya Perusahaan.</p>
														<p><b>ERP atau Enterprise Resource Planning</b> adalah Sistem Informasi yang berorientasi Akuntansi untuk mengidentifikasikan dan merencanakan sumber daya perusahaan untuk membuat, mengirim dan memperhitungkan pesanan pelanggan. 
														Sedangkan definisi ERP pada wikipedia adalah Sistem Informasi yang diperuntukan bagi perusahaan manufaktur maupun jasa yang berperan mengintegrasikan dan mengotomasikan proses bisnis yang berhubungan dengan aspek operasi, produksi maupun distribusi di perusahaan yang bersangkutan.</p>
														<p>Penulis : <b>Administrator</b></p>
													</div>
												</li>
												<li>
													<div class="head">
														<h2 class="title"><a href="">Jadwal libur menyambut hari raya idul fitri 1440H/2019M PT.Sadua Indo – HANSOLL.</a></h2>
														<span class="date">Mei 29, 2019</span> <span class="tags"><a href="#">Pengumuman</a>, <a href="#">Lebaran</a></span>
													</div>
													<div class="thumbnail">
														<img src="<?php echo base_url('assets/images/artikel/lebaran.jpg'); ?>" >
													</div>
													<div class="info">
														<p>Dalam rangka menyambut hari raya idul fitri 1440H/2019M PT.Sadua Indo – HANSOLL akan melaksanakan libur lebaran mulai tanggal 30 Mei – 09 Juni 2019,dan masuk kerja kembali tanggal 10 Juni 2019.</p>
														<p>Maka dari itu saya selaku Staff IT meminta kepada seluruh departemen untuk melakukan hal-hal berikut ini sebelum memulai libur lebaran :</p>
															<p>1.	Matikan Komputer dan perangkat yang terhubung,</p>
																•	Matikan CPU
																•	Matikan Monitor
																•	Matikan UPS (Bila ada)
																•	Matikan Printer, dan
																•	Matikan Semua Perangkat yang tidak digunakan
															<p>2. Usahakan Perangkat Komputer di tutup dengan Kain atau Plastik untuk keamanan dan melindungi perangkat dari kotoran selama tidak dipakai. </p>
															<p>3. Cabut semua beban listrik yang ada di semua stop-kontak (ke jalur listrik utama). Hal ini penting untuk mencegah hal-hal yang tidak diinginkan, sekaligus menghemat listrik.</p>
															
														<p>Penulis : <b>Administrator</b></p>
													</div>
												</li>
											</ul>
										</div>
                                        </div>



							</div>

			</div>
			
		</div>
		<footer id="footer">
					<div class="footer-left">Copyright&nbsp;&copy; 2019. All Rights Reserved. EKO SETIAWAN - 1115R0532</div>
					<div class="footer-right"><p><b>SiMASTER</b> <i> Version 1.1 Build 010719</i></p></div>
					</footer>
    </div>



<!-- Core Scripts -->
	<script src="<?php echo base_url('assets/js/libs/jquery-1.8.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.placeholder.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.mousewheel.min.js'); ?>"></script>
    
    <!-- Template Script -->
    <script src="<?php echo base_url('assets/js/template.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/setup.js'); ?>"></script>

    <!-- Customizer, remove if not needed -->
    <script src="<?php echo base_url('assets/js/customizer.js'); ?>"></script>

    <!-- Uniform Script -->
    <script src="<?php echo base_url('plugins/uniform/jquery.uniform.min.js'); ?>"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="<?php echo base_url('assets/jui/js/jquery-ui-1.9.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/jquery-ui.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/jui/jquery.ui.touch-punch.min.js'); ?>"></script>
    
    <!-- Plugin Scripts -->
   
    <!-- iButton -->
    <script src="<?php echo base_url('plugins/ibutton/jquery.ibutton.min.js'); ?>"></script>
     
    <!-- Demo Scripts -->
    <script src="<?php echo base_url('assets/js/demo/boxes.js'); ?>"></script>


</body>

</html>
