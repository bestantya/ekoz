<!-- <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatable/datatables.min.css"/>
<style media="screen">
  .table{
    width: 100% !important;
  }
  .dataTables_wrapper  {
      /* margin:10px 10px 5px 30px; */
      margin-top: 10px;
  }

  @media (max-width: 575.98px) {
  .table-responsive-sm {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-sm > .table-bordered {
    border: 0;
  }
}

@media (max-width: 767.98px) {
  .table-responsive-md {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-md > .table-bordered {
    border: 0;
  }
}

@media (max-width: 991.98px) {
  .table-responsive-lg {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-lg > .table-bordered {
    border: 0;
  }
}

@media (max-width: 1199.98px) {
  .table-responsive-xl {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-xl > .table-bordered {
    border: 0;
  }
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive > .table-bordered {
  border: 0;
}

  .dataTable thead {
      border-top: 1px solid #ccc !important;
  }
  .pagination {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  padding-left: 0;
  list-style: none;
  border-radius: 0.25rem;
}

.page-link {
  position: relative;
  display: block;
  padding: 0.5rem 0.75rem;
  margin-left: -1px;
  line-height: 1.25;
  color: #007bff;
  background-color: #fff;
  border: 1px solid #dee2e6;
}

.page-link:hover {
  color: #0056b3;
  text-decoration: none;
  background-color: #e9ecef;
  border-color: #dee2e6;
}

.page-link:focus {
  z-index: 2;
  outline: 0;
  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
}

.page-link:not(:disabled):not(.disabled) {
  cursor: pointer;
}

.page-item:first-child .page-link {
  margin-left: 0;
  border-top-left-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}

.page-item:last-child .page-link {
  border-top-right-radius: 0.25rem;
  border-bottom-right-radius: 0.25rem;
}

.page-item.active .page-link {
  z-index: 1;
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
}

.page-item.disabled .page-link {
  color: #6c757d;
  pointer-events: none;
  cursor: auto;
  background-color: #fff;
  border-color: #dee2e6;
}

.pagination-lg .page-link {
  padding: 0.75rem 1.5rem;
  font-size: 1.25rem;
  line-height: 1.5;
}

.pagination-lg .page-item:first-child .page-link {
  border-top-left-radius: 0.3rem;
  border-bottom-left-radius: 0.3rem;
}

.pagination-lg .page-item:last-child .page-link {
  border-top-right-radius: 0.3rem;
  border-bottom-right-radius: 0.3rem;
}

.pagination-sm .page-link {
  padding: 0.25rem 0.5rem;
  font-size: 0.875rem;
  line-height: 1.5;
}

.pagination-sm .page-item:first-child .page-link {
  border-top-left-radius: 0.2rem;
  border-bottom-left-radius: 0.2rem;
}

.pagination-sm .page-item:last-child .page-link {
  border-top-right-radius: 0.2rem;
  border-bottom-right-radius: 0.2rem;
}
</style>
<div id="sidebar-separator"></div>

     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Archived Inventory</a>
                                    </li>
                                </ul>

                                <h1 id="main-heading">
                                	Data Archived Inventory<span> Disini anda bisa melakukan pengelolaan data Archived Inventory.</b> </span>
                                </h1>
                </div>


			<div id="main-content">


						<div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab" id="b_01"><i class="icos-laptop"></i> LAPTOP</a></li>
                                    	<li><a href="#" data-target="#1" data-toggle="tab" id="b_02"><i class="icos-computer"></i> KOMPUTER</a></li>
                                    	<li><a href="#" data-target="#2" data-toggle="tab" id="b_03"><i class=" icos-monitor"></i> MONITOR</a></li>
										<li><a href="#" data-target="#3" data-toggle="tab" id="b_04"><i class="icos-printer"></i> PRINTER</a></li>
										<li><a href="#" data-target="#4" data-toggle="tab" id="b_05"><i class="icos-link"></i> DEVICE SUPPORT</a></li>
										<li class="pull-right"><a href="javascript:history.back()" ><i class="fa fa-remove"></i></a></li>
                                    </ul>


									<div class="tab-content">
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Laptop Tidak Terpakai
                                                    </label>
                                                </div>
                                            </div>



<div class="analytics-tab-content">
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Laptop
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>

													</form>
                                            </div>
                                        </div>


							<div class="table-responsive">
                                    <table id="table_laptop" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th width="13%">Nama Pengguna</th>
														<th>Departemen</th>
														<th>Sub.Dept</th>
														<th width="10%">Nama Laptop</th>
														<th>Spesifikasi</th>
														<th>Status</th>
														<th>Detail</th>
														<th>Delete</th>
													</tr>
                                                    </thead>



                                    </table>
                            </div>
						</div>


</div>
</div>
									<div id="1" class="tab-pane">
                                        	 <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Komputer Tidak Terpakai
                                                    </label>
                                                </div>
                                              </div>
<div class="analytics-tab-content">
<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Komputer
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                           <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>

													</form>
                                            </div>
                                        </div>

							<div class="table-responsive">
                                    <table id="table_komputer" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th>Nama Pengguna</th>
															<th>Departemen</th>
															<th>Sub.Dept</th>
															<th>Manufacture</th>
															<th>Spesifikasi</th>
														<th>Status</th>
														<th>Detail</th>
														<th>Delete</th>
													</tr>
                                                    </thead>




                                    </table>
                            </div>
						</div>




 </div>
 </div>

                                    	<div id="2" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Monitor Tidak Terpakai
                                                    </label>
                                                </div>
                                            </div>
<div class="analytics-tab-content">


<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Monitor
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>

													</form>
                                            </div>
                                        </div>


							<div class="table-responsive">
                                    <table id="table_monitor" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th>Nama Pengguna</th>
														  <th>Departemen</th>
														  <th>Sub.Dept</th>
														  <th>Jenis Monitor</th>
														  <th>Spesifikasi</th>
														<th>Status</th>
														<th>Detail</th>
														<th>Delete</th>
													</tr>
                                                    </thead>



                                    </table>
                            </div>
						</div>




 </div>
  </div>

                                    	<div id="3" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Printer Tidak Terpakai
                                                    </label>
                                                </div>
                                            </div>

<div class="analytics-tab-content">


							<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Printer
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                           <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>

													</form>
                                            </div>
                                        </div>

							<div class="table-responsive">
                                    <table id="table_printer" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th>Nama Pengguna</th>
														  <th>Departemen</th>
														  <th>Sub.Dept</th>
														  <th>Jenis printer</th>
														  <th>Spesifikasi</th>
														<th>Status</th>
														<th>Detail</th>
														<th>Delete</th>
													</tr>
                                                    </thead>


                                    </table>
                            </div>
						</div>




 </div>
	  </div>

                                    	<div id="4" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Device Support Tidak Terpakai
                                                    </label>
                                                </div>
                                            </div>

<div class="analytics-tab-content">


<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Device Support
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>

													</form>
                                            </div>
                                        </div>


							<div class="table-responsive">
                                    <table id="table_device" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th>Device Type</th>
														  <th>Spesifikasi</th>
														  <th>Lokasi Penggunaan</th>
														<th>Status</th>
														<th>Detail</th>
														<th>Delete</th>
													</tr>
                                                    </thead>


                                    </table>
                            </div>
						</div>




 </div>
	  </div>

			</div>
	</section>

</html>
<script type="text/javascript" src="<?php echo base_url();?>assets/datatable/datatables.min.js"></script>
    <script>
    $(document).ready( function () {
    table = $('#table_laptop').DataTable( {
        // "lengthChange": false,
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 50, 100, -1],
            [10, 50, 100, "All"]
        ],
        pageLength: 10,
        buttons: [
            { extend: 'pageLength', className: 'btn default' },
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "order": [[1, "asc"]], //Initial order.
        // Load data
        "ajax": {
            "url": "<?php echo base_url('archived/show_laptop');?>",
            "type": "POST"
        },
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ,-2 ], //last 2 col
                "orderable": false, //set not orderable
            }
        ],
        "oLanguage": {
                        "sSearch": "Pencarian Data :  ",
                        "sZeroRecords": "Tidak ada data",
                        "sEmptyTable": "Tidak ada data"
                      }
    } );

    table_k = $('#table_komputer').DataTable( {
        // "lengthChange": false,
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 50, 100, -1],
            [10, 50, 100, "All"]
        ],
        pageLength: 10,
        buttons: [
            { extend: 'pageLength', className: 'btn default' },
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "processing": true,
        "serverSide": true,
        "order": [[1, "asc"]], //Initial order.
        // Load data
        "ajax": {
            "url": "<?php echo base_url('archived/show_pc');?>",
            "type": "POST"
        },
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ,-2 ], //last 2 col
                "orderable": false, //set not orderable
            }
        ],
        "oLanguage": {
                        "sSearch": "Pencarian Data :  ",
                        "sZeroRecords": "Tidak ada data",
                        "sEmptyTable": "Tidak ada data"
                      }
    } );

    table_m = $('#table_monitor').DataTable( {
        // "lengthChange": false,
        "processing": true,
        "serverSide": true,
        "order": [[1, "asc"]], //Initial order.
        // Load data
        "ajax": {
            "url": "<?php echo base_url('archived/show_monitor');?>",
            "type": "POST"
        },
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ,-2 ], //last 2 col
                "orderable": false, //set not orderable
            }
        ],
        "oLanguage": {
                        "sSearch": "Pencarian Data :  ",
                        "sZeroRecords": "Tidak ada data",
                        "sEmptyTable": "Tidak ada data"
                      }
    } );

    table_p = $('#table_printer').DataTable( {
        // "lengthChange": false,
        "processing": true,
        "serverSide": true,
        "order": [[1, "asc"]], //Initial order.
        // Load data
        "ajax": {
            "url": "<?php echo base_url('archived/show_printer');?>",
            "type": "POST"
        },
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ,-2 ], //last 2 col
                "orderable": false, //set not orderable
            }
        ],
        "oLanguage": {
                        "sSearch": "Pencarian Data :  ",
                        "sZeroRecords": "Tidak ada data",
                        "sEmptyTable": "Tidak ada data"
                      }
    } );

    table_d = $('#table_device').DataTable( {
        // "lengthChange": false,
        "processing": true,
        "serverSide": true,
        "order": [[1, "asc"]], //Initial order.
        // Load data
        "ajax": {
            "url": "<?php echo base_url('archived/show_device');?>",
            "type": "POST"
        },
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ -1 ,-2 ], //last 2 col
                "orderable": false, //set not orderable
            }
        ],
        "oLanguage": {
                        "sSearch": "Pencarian Data :  ",
                        "sZeroRecords": "Tidak ada data",
                        "sEmptyTable": "Tidak ada data"
                      }
    } );

    } );

    $( "#b_01" ).click(function() {
      table.ajax.reload(null,false); //reload datatable ajax
      $("table_laptop").css("width","100%");
    });
    $( "#b_02" ).click(function() {
      table_k.ajax.reload(null,false); //reload datatable ajax
      $("table_komputer").css("width","100%");
    });
    $( "#b_03" ).click(function() {
      table_m.ajax.reload(null,false); //reload datatable ajax
      $("table_monitor").css("width","100%");
    });
    $( "#b_04" ).click(function() {
      table_p.ajax.reload(null,false); //reload datatable ajax
      $("table_printer").css("width","100%");
    });
    $( "#b_05" ).click(function() {
      table_d.ajax.reload(null,false); //reload datatable ajax
      $("table_device").attr("style", "width:100%")
    });
</script>
