<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah User</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah User <span> Disini anda bisa melakukan pengelolaan data user.</b> </span>
                                </h1>
                            </div>              
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Users</span>
                                     </div>
										<?php echo form_open('user/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Username <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="u_name" 
																  class="form-control" required oninvalid="setCustomValidity('Username Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Username Login" >
																</div>
															  <?php echo form_error('u_name', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													
													<div class="control-group">
													<label class="control-label">Nama Lengkap <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6" name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Lengkap Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan nama lengkap anda" >
																</div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
										
													<div class="control-group">
														<label class="control-label">Group User <span class="required">*</span></label>
														<div class="controls">
														   <select id="input17" name="group" class="select2-select-00 span6">                    
													  <?php
														if (!empty($record)) {
															foreach ($record as $r) {
															echo "<option value=".$r->gid.">".$r->nama_group."</option>";
																}
																}
															  ?>
															</select>                             
															<?php echo form_error('group', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>

													<div class="control-group">
															<label class="control-label">User Level <span class="required">*</span></label>
															<div class="controls">
														 <select name="level"  class="span6 select2-select" id="level"> 
															<option value='User'>User</option>
															<option value='Manager'>Manager</option>
															<option value='Staff'>Staff</option>
															<option value='Administrator'>Administrator</option> 
														  </select>                        
														</div> 
														<?php echo form_error('level', '<div class="text-red">', '</div>'); ?>                      
													</div>

													<div class="control-group">
													<label class="control-label">Password <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="password" class="span6"  name="passwd" 
																  class="form-control" required oninvalid="setCustomValidity('Password Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Password anda" >
															  <?php echo form_error('passwd', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													
    												<div class="control-group">
													<label class="control-label">Foto <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="foto" id="foto"
																  class="form-control" required oninvalid="setCustomValidity('Foto Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Foto Anda"  />
																</div>
															  <?php echo form_error('foto', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('user'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>

