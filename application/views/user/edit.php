<div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Edit User</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Edit User <span> Disini anda bisa melakukan pengelolaan data user.</b> </span>
                                </h1>
                            </div>
 
                            <div id="main-content">
                               <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
										<div class="widget">
                                            <div class="widget-header">
                                                <span class="title">Edit Data Users</span>
                                            </div>
											<?php echo form_open('user/edit'); ?> 
                                            <div class="widget-content form-container">
                                                <form class="form-horizontal" id="validate-4" method="post">
												
    												<div class="control-group">
													<label class="control-label" >Username <span class="required">*</span></label>
														<div class="controls">
															  <div class="">
																<input type="hidden"  name="id" value="<?php echo $record['id_user'] ?>" >
																  <input type="text" disabled  class="span4"  name="u_name" 
																  class="form-control" required oninvalid="setCustomValidity('Username tidak boleh kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Username saat Login" value="<?php echo $record['username']; ?>" >
															  </div>
															  <?php echo form_error('u_name', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label" >Nama Lengkap <span class="required">*</span></label>
														<div class="controls">
															  <div class="">
																  <input type="text" class="span4" name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Lengkap Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Lengkap anda" value="<?php echo $record['nama_user']; ?>" >
															  </div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
                                                        <label class="control-label" >Group User <span class="required">*</span></label>
                                                        <div class="controls">
                                                            <select id="input17"name="group" class="select2-select-00 span6">                      
																<?php
																if (!empty($group)) {
																	foreach ($group as $g) {
																		echo "<option value='$g->gid'";
																		echo $record['gid'] == $g->gid ? 'selected' : '';
																		echo">$g->nama_group</option>";  }
																}	?>
															</select>
															<?php echo form_error('group', '<div class="text-red">', '</div>'); ?>
                                                        </div>
    												</div>
				
    												<div class="control-group">
                                                        <label class="control-label" >User Level <span class="required">*</span></label>
                                                        <div class="controls">
                                                            <select id="input17" name='level' class="select2-select-00 span6">
    															<?php 
																  if ($record['role']=="User"){
																	echo "<option value='User' selected>User</option>
																		<option value='Manager'>Manager</option>
																		<option value='Staff'>Staff</option>
																		<option value='User'>User</option>";

																  }else if($record['role']=="Manager"){
																	echo "<option value='User'>User</option>
																		<option value='Manager' selected>Manager</option>
																		<option value='Staff'>Staff</option>
																		<option value='Administrator'>Administrator</option>";
																		
																  }else if($record['role']=="Staff"){
																	echo "<option value='User'>User</option>
																		<option value='Manager'>Manager</option>
																		<option value='Staff' selected>Staff</option>
																		<option value='Administrator'>Administrator</option>";	
																  }else {
																	echo "<option value='User'>User</option>
																		<option value='Manager'>Manager</option>
																		<option value='Staff'>Staff</option>
																		<option value='Administrator selected'>Administrator</option>";
																  }
																?> 
    														</select>
															<?php echo form_error('level', '<div class="text-red">', '</div>'); ?>
    														<span class="help-block">Pilih Level / Hak ases Username</span>
                                                        </div>
    												</div>
													
													<div class="control-group">
													<label class="control-label" >Password <span class="required">*</span></label>
														<div class="controls">
															  <div >
																  <input type="password" class="span4" name="passwd" 
																  class="form-control" required oninvalid="setCustomValidity('Password Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Password">
																<?php echo form_error('passwd', '<div class="text-blue">', '</div>'); ?>
															  </div> 
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Foto <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="foto" id="foto"
																  class="form-control" required oninvalid="setCustomValidity('Foto Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Foto Anda" value="<?php echo $record['foto']; ?>" >
																</div>
															  <?php echo form_error('foto', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
    	                                              <div class="form-actions">
                                                        <button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Save </button>
                                                        <a href="<?php echo site_url('user'); ?>" class="btn  btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
                                                    </div>
																								
                                                </form>
                                            </div>
        	                            </div>
									</div>
						
				
					</section>
		 
</html>