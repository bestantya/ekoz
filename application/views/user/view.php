<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">User Setting</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data User <span> Disini anda bisa melakukan pengelolaan data user.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Users
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('user/add'); ?>"  class="btn btn-primary icon-add-contact"> Tambah User </a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                              <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>

										

							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
                                                    <tr>
													<th>No.</th>
													<th>Nama Pengguna</th>
													<th>Username</th>
													<th>Level User</th>
													<th>User Group</th>
													<th>Foto</th> 													
													<th>Terakhir Login</th> 													
													<th>Edit</th>   
													<th>Delete</th>                                 
													</tr>
                                                    </thead>
                                             <?php
										   $no=1;                       
										   foreach ($record as $r){
										   $gid=$this->db->get_where('tb_group',array('gid'=>$r->gid))->row_array();  
											   echo"
												   <tr>
												   <td>$no</td>
												   <td>".$r->nama_user."</td>
												   <td>".$r->username."</td>
												   <td>".$r->role."</td>
												   <td>".$gid['nama_group']."</td>   
													<td>".$r->foto."</td>													   
												   <td>".$r->last_login."</td>												   
												   <td>" . anchor('user/edit/' . $r->id_user, ' Edit ','class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title=" Edit" ') . "</td>
												   <td>" . anchor('user/delete/' . $r->id_user, ' Hapus ','class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete" ', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
												   </tr>";
											   $no++;
										   }
										   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>