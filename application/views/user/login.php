<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>.:SiMASTER - Sistem Informasi Management Aset IT & Support Center:.</title>
<link href="<?php echo base_url('assets/images/icon.png'); ?>"rel='shortcut icon' type='image/x-icon'/>
<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('plugins/uniform/css/uniform.default.css'); ?>" media="screen">

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/icomoon/style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/login.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('plugins/zocial/zocial.css'); ?>" media="screen">


</head>

<body>


    <div id="login-wrap">

		<div id="login-ribbon"><i class="icon-lock"></i></div>

		<div id="login-buttons">
			<div class="btn-wrap">
				<button type="button" class="btn btn-inverse" data-target="#login-form"><i class="icon-key"></i></button>
			</div>
			<div class="btn-wrap">
				<button type="button" class="btn btn-inverse" data-target="#register-form"><i class="icon-edit"></i></button>
			</div>
			<div class="btn-wrap">
				<button type="button" class="btn btn-inverse" data-target="#forget-form"><i class="icon-question-sign"></i></button>
			</div>
		</div>

		<div id="login-inner" class="login-inset">

			<div id="login-circle">
				<section id="login-form" class="login-inner-form" data-angle="0">
								<div align="center" ><a class="brand" href="<?php echo base_url('web');?>"><br>
								<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 140px; height: 20px; color:white"></a>
								<p><small style="color:white">Sistem informasi Managemen Aset IT & Suppor Center</small></p>
								</div>
					<h1>Login</h1>
					
					<?php
                        echo form_open('login/login');                       
                        if (validation_errors() || $this->session->flashdata('error')) {
                        ?>

                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Warning!</strong>
                            <?php echo validation_errors(); ?>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>    
                    <?php } ?>
					<form class="form-vertical" >
						<div class="control-group-merged">
							<div class="control-group">
								<input type="text" placeholder="Username" name="username" id="input-username" class="big required">
							</div>
							<div class="control-group">
								<input type="password" placeholder="Password" name="password" id="input-password" class="big required">
							</div>
						</div>
						<div class="control-group">
							<label class="checkbox">
								<input type="checkbox" name="Login[remember]" class="uniform"> Remember me
							</label>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-success btn-block btn-large">Login</button>
						</div>
					</form>
					
				
					
				</section>
				<section id="register-form" class="login-inner-form" data-angle="90">
					<h1>Register</h1>
					<?php echo form_open('login/add'); ?>
					<form class="form-vertical" action="post">
						<div class="control-group">
							<label class="control-label">Username</label>
							<div class="controls">
								<input type="text" name="u_name" class="required" placeholder="Username anda">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Fullname</label>
							<div class="controls">
								<input type="text" name="nama" class="required" placeholder="Nama Lengkap anda">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Password</label>
							<div class="controls">
								<input type="password" name="passwd" class="required" placeholder="Password anda">
							</div>
						</div>
						<div disabled class="control-group">
							<label disabled class="control-label">Type Group</label>
							<div class="controls">
								<select  class="required" name="group" placeholder="Group1">
									<option>1</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">User Level </label>
							<div class="controls">
								<select class="required" name="level">
									<option>User</option>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" name="submit" class="btn btn-danger btn-block btn-large">Register</button>
						</div>
					</form>
				</section>
				<section id="forget-form" class="login-inner-form" data-angle="180">
					<h1>Reset Password</h1>
					<form class="form-vertical" action="dashboard.html">
						<div class="control-group">
							<div class="controls">
								<input type="text" name="Reset[email]" class="big required email" placeholder="Enter Your Email...">
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-danger btn-block btn-large">Reset</button>
						</div>
					</form>
				</section>
			</div>




		</div>

    </div>

	<!-- Core Scripts -->
	<script src="<?php echo base_url('assets/js/libs/jquery-1.8.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/libs/jquery.placeholder.min.js'); ?>"></script>
    
    <!-- Login Script -->
    <script src="<?php echo base_url('assets/js/login.js'); ?>"></script>

    <!-- Validation -->
    <script src="<?php echo base_url('plugins/validate/jquery.validate.min.js'); ?>"></script>

    <!-- Uniform Script -->
    <script src="<?php echo base_url('plugins/uniform/jquery.uniform.min.js'); ?>"></script>

</body>

</html>
