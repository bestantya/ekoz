<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Departemen</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Departemen <span> Disini anda bisa melakukan pengelolaan data master departemen.</b> </span>
                                </h1>
                </div>            
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Departemen</span>
                                     </div>
										<?php echo form_open('departemen/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Unit Group <span class="required">*</span></label>
														<div class="controls">
																<div >
																<input type='hidden' class="form-control" name="gid" value="<?php echo $this->session->userdata('gid'); ?>">
																  <input type="text" disabled  class="span6"  name="group" 
																  class="form-control" required value="<?php echo $this->session->userdata('group'); ?>">															  
																</div>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Nama Departeman/ Subdepartemen <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Departeman/ Subdepartemen Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Departeman/ Subdepartemen" >
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Departemen <span class="required">*</span></label>
														<div class="controls">
														   <select id="input17" name="parent" class="select2-select-00 span6">    
																<option value='0'>- Departemen -</option>														   
															<?php
																if (!empty($record)) {
																	foreach ($record as $r) {
																		echo "<option value=".$r->id_dept.">".$r->nama."</option>";                                        
																	}
																}
																?>
															  </select>                           
														</div>	
														<?php echo form_error('parent', '<div class="text-blue">', '</div>'); ?>
													</div>

												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('departemen'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
								
		</section>
</html>
