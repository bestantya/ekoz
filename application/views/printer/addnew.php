<div id="sidebar-separator"></div>
          <div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('printer'); ?>">Printer</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah Maintenance</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Maintenance Printer<span> Disini anda bisa melakukan pengelolaan data aset Printer.</b> </span>
                                </h1>
                            </div>    

<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Maintenance Aset Printer</span>
                                     </div>
										<?php echo form_open('printer/maintadd'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													
													     <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icon-print"></i> Information Detail Aset Printer</th>
														</tr>
														</thead>
														
														
														<tbody>
														<tr>
															<th>No. Inventaris :</th>
															<td style="width:80%"><?php echo $record['kode_printer'] ?></td>
															<input type="hidden"  name="no_inv" value="<?php echo $record['kode_printer'] ?>" >
														</tr>
														<tr>
															<th>Jenis Inventaris :</th>
															<td>Printer</td>                    
														</tr>

													  </tbody>
														
														  
														</table>

															</div>
															</div>
														</div>

													<div class="control-group">
														<label class="control-label">Maintenance Type <span class="required">*</span></label>
														<div class="controls">
														   <select id="type" name="type" class="select2-select-00 span6">   												   
																<option value="" selected="selected">- Select Maintenance Type-</option> 
																<option value="Hardware">Hardware</option>                               
																<option value="Software">Software</option> 
																<option value="Hardware & Software">Hardware & Software</option> 
																<option value="Network">Network/ Jaringan</option> 
															</select> 															
															<?php echo form_error('type', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tanggal Permohonan <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-basic" type="datetime" name="tgl_permohonan" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tanggal Permohonan harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_permohonan', '<div class="text-red">', '</div>'); ?>
                                                    </div>	
													
													<div class="control-group">
													<label class="control-label">Catatan Pemohon <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan" 
																  class="form-control" required oninvalid="setCustomValidity('Catatan Pemohon Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Catatan Permohonan"></textarea>
																</div>
															  <?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
														</div>
													</div> 
												
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tanggal Selesai <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="datetime" name="tgl_selesai" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tanggal Selesai harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_selesai', '<div class="text-red">', '</div>'); ?>
                                                    </div>						

													<div class="control-group">
													<label class="control-label">Catatan Perbaikan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan_perbaikan" 
																  class="form-control" required oninvalid="setCustomValidity('Catatan Perbaikan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Catatan Perbaikan"></textarea>
																</div>
															  <?php echo form_error('catatan_perbaikan', '<div class="text-red">', '</div>'); ?>
														</div>
													</div> 

													<div class="control-group">
														<label class="control-label">Nama Suplier/Rekanan <span class="required">*</span></label>
														<div class="controls">
														   <select  name="supplier" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Nama Suplier/Rekanan -</option>														   
																	<?php
																		if (!empty($supplier)) {
																			foreach ($supplier as $row) {
																				echo "<option value='$row->nama_supplier'>".strtoupper($row->nama_supplier)."</option>";                                        
																			}
																		}
																	?>
																</select>
																*Note : di isi ketika service di rekanan																
															<?php echo form_error('supplier', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div> 

													<div class="control-group">
													<label class="control-label">Cost /Biaya <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6"  name="biaya" 
																  class="form-control" required oninvalid="setCustomValidity('Cost/Biaya Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Cost/Biaya Maintenance" >
															  <?php echo form_error('biaya', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="javascript:history.back()" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
						</div>
                    </div>

		</section>
</html>

