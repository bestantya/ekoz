<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah Aset Printer</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Aset Printer<span> Disini anda bisa melakukan pengelolaan data aset Printer.</b> </span>
                                </h1>
                            </div>              
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Aset Printer</span>
                                     </div>
										<?php echo form_open('printer/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
														<label class="control-label">Pengguna Printer <span class="required">*</span></label>
														<div class="controls">
														   <select id="pengguna" name="pengguna" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Printer-</option>														   
																<?php
																if (!empty($pengguna)) {
																	foreach ($pengguna as $row) {
																		echo "<option value=".$row->id_pengguna.">".strtoupper($row->nama_pengguna)."</option>";                                        
																	}
																}
																?>    
															</select>																
															<?php echo form_error('pengguna', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">Jenis Printer <span class="required">*</span></label>
														<div class="controls">
														   <select id="jenis" name="jenis" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Printer -</option>														   
																<option value='DESKJET'>DESKJET</option>
																<option value='LASERJET'>LASERJET</option>
																<option value='DOTMATRIX'>DOTMATRIX</option>
																<option value='ALL-IN'>ALL-IN</option> 
																<option value='SCANER'>SCANER</option>
																<option value='FAX'>FAX</option>                                  
															</select>																
															<?php echo form_error('jenis', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Printer" ></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
											
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventaris <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_inv" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
                                                    </div>						

													<div class="control-group">
													<label class="control-label">Harga Beli <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6"  name="harga" 
																  class="form-control" required oninvalid="setCustomValidity('Harga Beli Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Harga Beli Printer" >
															  <?php echo form_error('harga', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													  													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('printer'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>
