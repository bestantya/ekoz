<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('printer'); ?>">Printer</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Mutasi Inventaris</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Mutasi Inventaris<span> Disini anda bisa melakukan pengelolaan data mutasi aset Printer.</b> </span>
                                </h1>
                            </div>              
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Mutasi Data Aset Printer</span>
                                     </div>
										<?php echo form_open('printer/edithistory'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

												<div class="control-group">
													   <label class="control-label">No. Inventaris <span class="required">*</span></label>
														<input type="hidden"  class="span6" name="kode" value="<?php echo $record['no_inventaris'] ?>" >
														<input type="text" class="span6" name="no_inv" disabled class="form-control" id="inputError" value="<?php echo $record['no_inventaris']; ?>" >
														 <input type="hidden"  class="span6" name="kode" value="<?php echo $record['id_history'] ?>" >
													</div> 

													<div class="control-group">
													<label class="control-label">Type Printer <span class="required">*</span></label>
														<div class="controls">
															<select id="jenis" name="jenis" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Printer -</option>														   
																<option value='DESKJET'>DESKJET</option>
																<option value='LASERJET'>LASERJET</option>
																<option value='DOTMATRIX'>DOTMATRIX</option>
																<option value='ALL-IN'>ALL-IN</option> 
																<option value='SCANER'>SCANER</option>
																<option value='FAX'>FAX</option>                               
															</select>	
															  <?php echo form_error('jenis_printer', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
																								
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Printer" ><?php echo $record['spesifikasi']; ?></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>                     
                          
						  							<div class="control-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">
															<option value='' selected="selected">- Pilih Status Aset -</option>		
																<?php 
																  if ($record['status']=="Mutasi"){
																	echo "	<option value='Mutasi' selected>Mutasi</option>
																			<option value='Dipinjamkan'>Dipinjamkan</option>
																			<option value='Kembali'>Kembali</option>
																			<option value='Buat Baru'>Inventory Baru</option>";
																  }else if ($record['status']=="Dipinjamkan"){
																	echo "	<option value='Mutasi' >Mutasi</option>
																			<option value='Dipinjamkan' selected>Dipinjamkan</option>
																			<option value='Kembali'>Kembali</option>
																			<option value='Buat Baru'>Inventory Baru</option>";
																  }else if ($record['status']=="Kembali"){
																	echo "	<option value='Mutasi'>Mutasi</option>
																			<option value='Dipinjamkan'>Dipinjamkan</option>
																			<option value='Kembali' selected>Kembali</option>
																			<option value='Buat Baru'>Inventory Baru</option>";
																  }else{
																	echo " 	<option value='Mutasi'>Mutasi</option>
																			<option value='Dipinjamkan'>Dipinjamkan</option>
																			<option value='Kembali'>Kembali</option>
																			<option value='Buat Baru'  selected>Inventory Baru</option>";
																  }
																?>  
															</select>
															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>
						  					
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tanggal Mutasi <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_update" value="<?php echo $record['tgl_update']; ?>" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_update', '<div class="text-red">', '</div>'); ?>
                                                    </div>
							
							
													<div class="control-group">
														<label class="control-label">Pengguna Printer <span class="required">*</span></label>
														<div class="controls">
														   <select  name="pengguna" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Printer -</option>														   
															<?php   
															  $gid=$this->session->userdata('gid');                                     
																$pengguna=$this->db->query("SELECT tb_pengguna.id_pengguna,tb_pengguna.nama_pengguna,tb_departemen.gid FROM tb_pengguna INNER JOIN tb_departemen ON tb_departemen.id_dept = tb_pengguna.id_dept 
																  WHERE tb_departemen.gid ='$gid' ORDER BY tb_pengguna.nama_pengguna ASC");
																foreach ($pengguna->result() as $r) {
																	echo "<option value='$r->id_pengguna'";
																	echo $record['id_pengguna'] == $r->id_pengguna ? 'selected' : '';
																	echo">".strtoupper($r->nama_pengguna)."</option>";
																}
																?>   
															</select>																
															<?php echo form_error('pengguna', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div> 
							
													<div class="control-group">
													<label class="control-label">Catatan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan" 
																  class="form-control" required oninvalid="setCustomValidity('Catatan Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Catatan Printer" ><?php echo $record['note']; ?></textarea>
																</div>
															  <?php echo form_error('catatan', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div> 	
													
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="javascript:history.back()" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
								
                                </div>
		</section>
</html>
