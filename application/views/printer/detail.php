<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('printer'); ?>">Printer</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Detail Aset Printer</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Detail aset Printer <span> Disini anda bisa melakukan pengelolaan data aset Printer.</b> </span>
                                </h1>
                </div>

				<div id="main-content">
					<div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab"><i class="icon-list-2"></i> DETAIL</a></li>
                                    	<li><a href="#" data-target="#math" data-toggle="tab"><i class="icon-edit"></i> EDIT</a></li>
                                    	<li><a href="#" data-target="#fb" data-toggle="tab"><i class=" icon-tools"></i> MAINTENANCE</a></li>
										<li><a href="#" data-target="#revenue" data-toggle="tab"><i class="icon-history"></i> HISTORY</a></li>
										<li class="pull-right"><a href="<?php echo site_url('printer'); ?>" class="text-muted"><i class="fa fa-remove"></i></a></li>
                                    </ul>
									
                                    <div class="tab-content">
									
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Printer
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="analytics-tab-content">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icon-print"></i> Information Detail Aset Printer</th>
														</tr>
														</thead>
                                            	<tbody>
														<tr>
															<th>No. Inventaris :</th>
															<td style="width:80%"><?php echo $recordall['kode_printer'] ?></td>
														</tr>
														<tr>
															<th>Pengguna :</th>
															<td><?php echo anchor('pengguna/edit/'.$recordall['id_pengguna'],$recordall['nama_pengguna']);?></td>                       
														</tr>
														<tr>
															<th>Type Printer :</th>
															<td><?php echo $recordall['jenis_printer']?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $recordall['spesifikasi']?></td>                    
														</tr>
														<tr>
															<th>Tgl. Inventaris :</th>
															<td><?php echo tgl_lengkap($recordall['tgl_inv'])?></td>                    
														</tr>
														<tr>
															<th>Status :</th>
															<td><?php echo $recordall['status']?></td>                    
														</tr>														
														<tr>
															<th>Note/ Catatan :</th>
															<td><?php echo $recordall['note']?></td>                    
														</tr>																											
														<tr>
															<th>Harga Beli :</th>
															<td><?php echo 'Rp '.rupiah($recordall['harga_beli'])?></td>                    
														</tr>

													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
                                    	<div id="math" class="tab-pane">
                                        	 <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Edit Informasi Aset Printer
                                                    </label>
                                                </div>
                                              </div>
										
								

					<div class="analytics-tab-header clearfix">
						 <div class="col-md-5">
                        <?php echo form_open('printer/edit'); ?>                            
                        <div class="box-body">
                                <div class="control-group">
                                   <label class="control-label">No. Inventaris <span class="required">*</span></label>
                                    <input type="hidden"  class="span6" name="kode" value="<?php echo $record['kode_printer'] ?>" >
                                    <input type="text" class="span6" name="no_inv" disabled class="form-control" id="inputError" value="<?php echo $record['kode_printer']; ?>" >
                                </div>                                            
													<div class="control-group">
														<label class="control-label">Pengguna Printer <span class="required">*</span></label>
														<div class="controls">
														   <select  name="pengguna" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Printer -</option>														   
																 <?php
																	$gid=$record['gid'];
																	$pengguna=$this->db->query("SELECT tb_pengguna.id_pengguna,tb_pengguna.nama_pengguna,tb_departemen.gid FROM tb_pengguna INNER JOIN tb_departemen ON tb_departemen.id_dept = tb_pengguna.id_dept 
																		WHERE tb_departemen.gid ='$gid' ORDER BY tb_pengguna.nama_pengguna ASC");
																	foreach ($pengguna->result() as $r) {
																		echo "<option value='$r->id_pengguna'";
																		echo $record['id_pengguna'] == $r->id_pengguna ? 'selected' : '';
																		echo">".strtoupper($r->nama_pengguna)."</option>";
																	}
																	?>    
															</select>																
															<?php echo form_error('pengguna', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>  
													
													<div class="control-group">
														<label class="control-label">Jenis Printer <span class="required">*</span></label>
														<div class="controls">
														   <select  name="jenis" class="select2-select-00 span6">														   
																<option value='<?php echo $record['jenis_printer']; ?>'><?php echo $record['jenis_printer'];'selected' ?></option>
																<option value='DESKJET'>DESKJET</option>
																<option value='LASERJET'>LASERJET</option>
																<option value='DOTMATRIX'>DOTMATRIX</option>
																<option value='ALL-IN'>ALL-IN</option> 
																<option value='SCANER'>SCANER</option> 
																<option value='FAX'>FAX</option>      
															</select>																
															<?php echo form_error('jenis', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div> 
																								
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Printer" ><?php echo $record['spesifikasi']; ?></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventari <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_inv" value="<?php echo $record['tgl_inv']; ?>" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_inv', '<div class="text-red">', '</div>'); ?>
                                                    </div>			

													<div class="form-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">
															<option value='' selected="selected">- Pilih Status Aset -</option>		
																<?php                                            
																$status=$this->db->get("tb_status");
																foreach ($status->result() as $r) {
																	echo "<option value='$r->nama_status'";
																	echo $record['status'] == $r->nama_status ? 'selected' : '';
																	echo">".$r->nama_status."</option>";
																}
																?>
															</select>
															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>  
																					<div class="control-group">
													<label class="control-label">Note/ Catatan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="note" 
																  class="form-control" required oninvalid="setCustomValidity('Note / Catatan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Note / Catatan Status Inventory" ><?php echo $record['note']; ?></textarea>
																</div>
															  <?php echo form_error('note', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
						
													<div class="control-group">
													<label class="control-label">Harga Beli <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6"  name="harga" value="<?php echo $record['harga_beli']; ?>"
																  class="form-control" required oninvalid="setCustomValidity('Harga Beli Printer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Harga Beli Printer" >
															  <?php echo form_error('harga', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
								
													</div>
													<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('printer'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>

												</div>
											
											</form>
											</div>
											 </div>				
								 </div>	
															
										
                                    	<div id="fb" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Data Maintenance Aset Printer
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="analytics-tab-content">
											
											
											 <div class="col-md-10"> 
                                            	<h4>Maintenance [ <a><?php echo anchor('printer/maintadd/'.$recordall['kode_printer'],'Add New') ?> ]
												  <a href="<?php echo base_url('printer/print_maintenance/'.$recordall['kode_printer']) ?>" target="_blank" ><i class="btn btn-primary icon-print" data-toggle="tooltip" title="Cetak"> Print</i></a></h4>                          
												  <table class="table ">
												  <br>
													<tr>
													  <td><label>No Ticket</label></td>
													  <td><label>Tgl. Permohonan</label></td>
													  <td><label>Maintenance Type</label></td>
													  <td><label>Catatan Permohonan</label></td>                              
													  <td><label>Tgl. Selesai</label></td>
													  <td style="text-align:right"><label>Biaya/ Cost</label></td>
													  <td style="text-align:center"><label>Aksi</label></td>
													</tr>                            
													 <?php 
														foreach ($service as $s) {
														  echo"
															<tr>
															  <td>".anchor('maintenance/detail/'.$s->no_permohonan,$s->no_permohonan)."</td>
															  <td>".tgl_lengkap($s->tgl_permohonan)."</td>
															  <td>".$s->jenis_permohonan."</td>
															  <td>".$s->catatan_pemohon."</td>                                      
															  <td>".tgl_lengkap($s->tgl_selesai)."</td>
															  <td style='text-align:right'>".rupiah($s->biaya)."</td>
															  <td style='text-align:center'>".anchor('maintenance/detail/' . $s->no_permohonan, '<i class="btn btn-danger icon-edit" data-toggle="tooltip" title="Edit"></i>') ."</td>

															</tr> ";
														}

													 ?>                      
												  </table>
												  </div>
						  
						  
                                            </div>
                                        </div>
										
                                    	<div id="revenue" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Data History Pemakai Aset Printer
                                                    </label>
                                                </div>
                                            </div>
											<div class="analytics-tab-content">
											
													 <div class="col-md-10">  
												  <h4>History / Mutasi [ <a><?php echo anchor('printer/history/'.$recordall['kode_printer'],'Add New') ?></a> ]</h4>                         
												  <table class="table ">
												  <br>
													<tr>
													  <td><label>Tanggal</label></td>
													  <td><label>Admin</label></td>
													  <td><label>Status</label></td>
													  <td><label>User Pengguna</label></td>                              
													  <td><label>Note</label></td>
													  <td><label>Aksi</label></td>                              
													</tr>                            
													 <?php 
														foreach ($history as $s) {
														  echo"
															<tr>                                     
															  <td>".tgl_lengkap($s->tgl_update)."</td>
															  <td>".$s->admin."</td>
															  <td>".$s->status."</td>                                      
															  <td>".anchor('pengguna/edit/'.$s->id_pengguna,$s->nama_pengguna)."</td>
															  <td>".$s->note."</td> 
															  <td>" . anchor('printer/edithistory/' . $s->id_history, '<i class="btn btn-danger icon-edit" data-toggle="tooltip" title="Edit"></i>')." | <a href=".base_url('printer/print_history/'.$s->id_history)." target='_blank' ><i class='btn btn-success icon-print' data-toggle='tooltip' title='Print'></i></td>                                     
															</tr> ";
														}
													 ?>                      
												  </table>
												</div>
						
											</div>
										</div>
                                    </div>
              

                  </div>
                </div>


	</section>	 
</html>