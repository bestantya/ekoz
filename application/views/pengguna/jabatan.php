<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Jabatan</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Jabatan <span> Disini anda bisa melakukan pengelolaan data master jabatan.</b> </span>
                                </h1>
                </div>            
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Jabatan</span>
                                     </div>
										<?php echo form_open('pengguna/addjabatan'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Nama Jabatan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama_jabatan" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Jabatan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Jabatan" >
																</div>
															  <?php echo form_error('nama_jabatan', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Job Desk <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="job" 
																  class="form-control" required oninvalid="setCustomValidity('Job Desk Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Job Desk" ></textarea>
																</div>
															  <?php echo form_error('job', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('pengguna/add'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
								
		</section>
</html>
