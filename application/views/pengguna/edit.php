<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Pengguna</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Pengguna <span> Disini anda bisa melakukan pengelolaan data master pengguna.</b> </span>
                                </h1>
                </div>            
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Pengguna</span>
                                     </div>
										<?php echo form_open('pengguna/edit'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">
													<div class="control-group">
													<label class="control-label">Kode Pengguna <span class="required">*</span></label>
														<div class="controls">
																<div >
																<input type="hidden"  name="kode" value="<?php echo $record['id_pengguna'] ?>" >
																  <input type="text" disabled class="span6"  name="idpengguna" 
																  class="form-control" id="inputError" value="<?php echo $record['id_pengguna']; ?>" >
																</div>

														</div>
													</div>

													<div class="control-group">
													<label class="control-label">NIK <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nik" 
																  class="form-control" required oninvalid="setCustomValidity('NIK Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nomor Induk Karyawan" 
																  value="<?php echo $record['nik']; ?>" >
																</div>
															  <?php echo form_error('nik', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Nama Pengguna <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Pengguna Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Pengguna" 
																  value="<?php echo $record['nama_pengguna']; ?>" >
																</div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Jabatan <span class="required">*</span></label>
														<div class="controls">
														   <select id="input17"name="jabatan" class="select2-select-00 span6">                    
															<?php
																foreach ($jabatan as $j) {
																	echo "<option value='$j->id_jabatan'";
																	echo $record['id_jabatan'] == $j->id_jabatan ? 'selected' : '';
																	echo">$j->nama_jabatan</option>";
																}
																?>
															</select>                           
														</div><?php echo form_error('jabatan', '<div class="text-red">', '</div>'); ?>	
														<a href="<?php echo site_url('pengguna/addjabatan'); ?>" class="btn btn-primary"> 
														<i class="icon-plus" aria-hidden="true" ></i> Add Jabatan </a>
														<?php echo form_error('jabatan', '<div class="text-red">', '</div>'); ?>
													</div>
											
													<div class="control-group">
														<label class="control-label">Departemen <span class="required">*</span></label>
														<div class="controls">
														    <select name="dept" class="select2-select-00 span6" id="dept">														   
															<?php 
																$departemen=$this->db->get_where('tb_departemen', array('parent'=>0,'gid'=>$record['gid']));                                   
																foreach ($departemen->result() as $d) {
																	echo "<option value='$d->id_dept'";
																	if ($record['parent']==0){
																		echo $record['id_dept'] == $d->id_dept ? 'selected' : '';
																	}else{
																	   echo $record['parent'] == $d->id_dept ? 'selected' : ''; 
																	}                                        
																	echo">$d->nama</option>";
																}
																?>
															  </select>                           
														</div><?php echo form_error('departemen', '<div class="text-red">', '</div>'); ?>	
														<a href="<?php echo site_url('pengguna/adddept'); ?>" class="btn btn-primary"> 
														<i class="icon-plus" aria-hidden="true" ></i> Add Departemen </a>
														<?php echo form_error('departemen', '<div class="text-red">', '</div>'); ?>
													</div>
													
													<div class="control-group">
															<label class="control-label">Sub. Departemen <span class="required">*</span></label>
															<div class="controls">
														 <select name="subdept"  class="span6 select2-select" id="subdept"> 
															<option value=''>- Pilih Sub. Departemen -</option>
																<?php
																if (!empty($record)) {
																	foreach ($record as $r) {
																		echo "<option value=".$r->id_dept.">".$r->parent."</option>";                                        
																	}
																}
																?>
															</select>                        
														</div>                
													</div>

												<div class="control-group">
													<label class="control-label">Ruang/Kantor <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="kantor" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Ruang/Kantor Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Ruang/Kantor (ex: Ruang Main office HRD)" value="<?php echo $record['ruang_kantor']; ?>" >
																</div>
															  <?php echo form_error('kantor', '<div class="text-blue">', '</div>'); ?>
														</div>
												</div>
														
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('pengguna'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>

		</section>
</html>

