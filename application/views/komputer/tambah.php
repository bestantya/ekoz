<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah Aset Komputer</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Aset Komputer<span> Disini anda bisa melakukan pengelolaan data aset komputer.</b> </span>
                                </h1>
                            </div>              
 
<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Aset Komputer</span>
                                     </div>
										<?php echo form_open('komputer/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
														<label class="control-label">Pengguna Komputer <span class="required">*</span></label>
														<div class="controls">
														   <select id="dept" name="pengguna" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Komputer -</option>														   
																<?php
																if (!empty($pengguna)) {
																	foreach ($pengguna as $row) {
																		echo "<option value=".$row->id_pengguna.">".strtoupper($row->nama_pengguna)."</option>";                                        
																	}
																}
																?>    
															</select>																
															<?php echo form_error('pengguna', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Brand/Mainboard PC <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="merek" 
																  class="form-control" required oninvalid="setCustomValidity('Merek/brand Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Merek/Brand ex : HP , LENOVO" >
																</div>
															  <?php echo form_error('merek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Komputer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Komputer" ></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Serial Number <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6" name="sn" 
																  class="form-control" required oninvalid="setCustomValidity('Serial Number Komputer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Serial Number Komputer" >
																</div>
															  <?php echo form_error('sn', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
										
											
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventari <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_inv" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
                                                    </div>						

													<div class="control-group">
													<label class="control-label">Harga Beli <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6"  name="harga" 
																  class="form-control" required oninvalid="setCustomValidity('Harga Beli Komputer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Harga Beli Komputer" >
															  
															  </div>
															  <?php echo form_error('harga', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">IP Address <span class="required">*</span></label>
														<div class="controls">
																  <div class="input-group">
																  <div class="input-group-addon">
																	<i class="iconicon-laptop"></i>
																  </div>
																  <input name="ip" type="text" class="span6"  data-inputmask="'alias': 'ip'" data-mask required/>
																</div>
															  <?php echo form_error('ip', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>  		 													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('komputer'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>



