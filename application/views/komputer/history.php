<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('komputer'); ?>">Komputer</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Mutasi Inventaris</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Mutasi<span> Disini anda bisa melakukan pengelolaan data mutasi aset komputer.</b> </span>
                                </h1>
                            </div>              
 

				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Mutasi Data Komputer</span>
                                     </div>
										<?php echo form_open('komputer/history'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">
											
										
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icos-computer"></i> Information Detail Pengguna Aset Komputer</th>
														</tr>
														</thead>
                                            	<tbody>
														<tr>
															<th>No. Inventaris :</th>
															<td style="width:80%"><?php echo $recordall['kode_komputer'] ?></td>
															<input type="hidden"  name="no_inv" value="<?php echo $recordall['kode_komputer'] ?>" >
															<input type="hidden"  name="pengguna_awal" value="<?php echo $recordall['id_pengguna'] ?>" >
														</tr>
														<tr>
															<th>Pengguna Lama :</th>
															<td><?php echo $recordall['nama_pengguna']?></td>                    
														</tr>
														<tr>
															<th>Brand Komputer:</th>
															<td><?php echo $recordall['nama_komputer']?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $recordall['spesifikasi']?></td>                    
														</tr>
														<tr>
															<th>Serial Number :</th>
															<td><?php echo $recordall['serial_number']?></td>                    
														</tr>
														<tr>
															<th>Tgl. Inventaris :</th>
															<td><?php echo tgl_lengkap($recordall['tgl_inv'])?></td>                    
														</tr>
	
													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>
                                       
						  							<div class="control-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">                              
																<option value='Mutasi'>Mutasi</option> 
																<option value='Dipinjamkan'>Dipinjamkan</option> 
																<option value='Kembali'>Kembali</option>
																<option value='Buat Baru'>Inventory Baru</option>                                                            
															</select>

															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>
						  					
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventari <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_update" value="" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_update', '<div class="text-red">', '</div>'); ?>
                                                    </div>
							
							
													<div class="control-group">
														<label class="control-label">Pengguna Baru <span class="required">*</span></label>
														<div class="controls">
														   <select  name="pengguna" class="select2-select-00 span6">   
															<option value='' selected="selected">- Pilih Pengguna Komputer -</option>														   
																<?php
																if (!empty($pengguna)) {
																	foreach ($pengguna as $row) {
																		echo "<option value=".$row->id_pengguna.">".strtoupper($row->nama_pengguna)."</option>";                                        
																	}
																}
																?>
															</select>																
															<?php echo form_error('pengguna', '<div class="text-red">', '</div>'); ?>	
														</div>
													</div>  
							
													<div class="control-group">
													<label class="control-label">Catatan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan" 
																  class="form-control" required oninvalid="setCustomValidity('Catatan Komputer Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Catatan/Keterangan Komputer" ></textarea>
																</div>
														</div>
													</div> 	

												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="javascript:history.back()" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>                                  


                                </div>
		</section>
</html>
