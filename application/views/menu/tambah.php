<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah Menu</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Menu <span> Disini anda bisa melakukan pengelolaan data menu.</b> </span>
                                </h1>
                            </div>              
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Menu</span>
                                     </div>
										<?php echo form_open('menu/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Nama Menu <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Menu Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Menu" >
																</div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													
													<div class="control-group">
													<label class="control-label">Icon <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6" name="icon" 
																  class="form-control" required oninvalid="setCustomValidity('Icon Menu Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Icon Menu" >
																</div>
															  <?php echo form_error('icon', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
										
													<div class="control-group">
													<label class="control-label">Link <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6" name="link" 
																  class="form-control" required oninvalid="setCustomValidity('Link Menu Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Link Menu" >
																</div>
															  <?php echo form_error('link', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Kat. Menu <span class="required">*</span></label>
														<div class="controls">
														   <select id="input17"name="kat_menu" class="select2-select-00 span6">   
															<option value='0'>Menu Utama</option>														   
														  <?php
															if (!empty($record)) {
																foreach ($record as $r) {
																	echo "<option value=".$r->id_menu.">".$r->nama_menu."</option>";                                        
																}
															}
															?></select>
														</div>
													</div>
													
													<div class="control-group">
															<label class="control-label">Level <span class="required">*</span></label>
															<div class="controls">
														 <select name="role"  class="span6 select2-select" id="role"> 
															<option value='User'>User</option>
															<option value='Manager'>Manager</option>
															<option value='Staff'>Staff</option>
															<option value='Administrator'>Administrator</option> 
														  </select>                        
														</div> 
														<?php echo form_error('role', '<div class="text-red">', '</div>'); ?>                      
													</div>												
										
													<div class="control-group">
															<label class="control-label">Status <span class="required">*</span></label>
															<div class="controls">
														 <select name="aktif"  class="span6 select2-select" id="aktif"> 
															<option value='Y'>Aktif</option>
															<option value='N'>Non Aktif</option>
														  </select>                        
														</div> 
														<?php echo form_error('aktif', '<div class="text-red">', '</div>'); ?>                      
													</div>	
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('menu'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
	
                     </div>
		</section>
</html>




                       
    