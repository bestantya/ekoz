<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Menu Setting</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data Menu <span> Disini anda bisa melakukan pengelolaan data menu.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Menu
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('menu/add'); ?>"  class="btn btn-primary icon-list"> Tambah Menu </a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>


							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
                                                    <tr>
													<th>No.</th>
													<th>Nama Menu</th>
													<th>Icon</th>
													<th>Link</th>
													<th>Kat. Menu</th>   
													<th>Level</th>
													<th>Status</th>  													
													<th>Edit</th>   
													<th>Delete</th>                                 
													</tr>
                                                    </thead>
                                              <?php
											   $no=1;
											   function chek($id) {
													$CI = get_instance();
													$result = $CI->db->get_where('tb_menu', array('id_menu' => $id))->row_array();
													return $result['nama_menu'];
												}
											   foreach ($record as $r){    
												$katmenu = $r->parent == 0 ? 'Menu Utama' : chek($r->parent);
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->nama_menu."</td>
													   <td>".$r->icon."</td>
													   <td>".$r->link."</td>
													   <td>".$katmenu."</td>  
													   <td>".$r->role."</td>
													   <td>".$r->aktif."</td>													   
													   <td>" . anchor('menu/edit/' . $r->id_menu, ' Edit ','class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title=" Edit" ') . "</td>
													   <td>" . anchor('menu/delete/' . $r->id_menu, ' Hapus ','class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete" ', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>

