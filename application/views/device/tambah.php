<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Tambah Aset Device Support</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Aset Device Support<span> Disini anda bisa melakukan pengelolaan data aset Device Support.</b> </span>
                                </h1>
                            </div>              
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Aset Device Support</span>
                                     </div>
										<?php echo form_open('device/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">No. Inventory <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="kode" 
																  class="form-control" required oninvalid="setCustomValidity('Kode Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Kode Device. ex:NET-PMT-001, CCT-PMT-001" >
															  </div>
															  <?php echo form_error('kode', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Device Type <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="jenis" 
																  class="form-control" required oninvalid="setCustomValidity('Jenis Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Jenis Device. ex:Switch, HUB, Router, CCTV, Dll" >
															  </div>
															  <?php echo form_error('jenis', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Device Support Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Device Support" ></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
											
													<div class="control-group">
													<label class="control-label">Lokasi Penggunaan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="lokasi" 
																  class="form-control" required oninvalid="setCustomValidity('lokasi Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan lokasi Device." >
															  </div>
															  <?php echo form_error('lokasi', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
											
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventaris <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_inv" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
                                                    </div>						

													  													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('device'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>
