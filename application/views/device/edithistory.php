<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('device'); ?>">Device Support</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Mutasi Inventaris</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Mutasi Inventaris<span> Disini anda bisa melakukan pengelolaan data mutasi aset Device Support.</b> </span>
                                </h1>
                            </div>              
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Mutasi Data Aset Device Support</span>
                                     </div>
										<?php echo form_open('device/edithistory'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													
                                            	<tbody>
														<tr>
															<th>No. Inventaris :</th>
															<td style="width:70%"><?php echo $record['no_inventaris'] ?></td>
															  <input type="hidden"  name="no_inv" value="<?php echo $record['no_inventaris'] ?>" >
															  <input type="hidden"  name="id" value="<?php echo $record['id_history'] ?>" >
														</tr>
														<tr>
															<th>Type Device :</th>
															<td><?php echo $record['jenis_network']?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $record['spesifikasi']?></td>                    
														</tr>
													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>                                       
	                
													<div class="control-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">                                  
																<option value='Mutasi'>Mutasi</option> 
																<option value='Buat Baru'>Buat Baru</option>                                                              
															</select>

															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>
                          
						  												  					
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tanggal Mutasi <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_update" value="<?php echo $record['tgl_update']; ?>" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_update', '<div class="text-red">', '</div>'); ?>
                                                    </div>
							
							
													<div class="control-group">
													<label class="control-label">Lokasi Baru <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="lokasi" 
																  class="form-control" required oninvalid="setCustomValidity('Lokasi Device Support Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Lokasi Device Support" value="<?php echo $record['lokasi']; ?>" >
															  <?php echo form_error('lokasi', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													 
							
													<div class="control-group">
													<label class="control-label">Catatan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan" 
																  class="form-control" required oninvalid="setCustomValidity('Catatan Monitor Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Catatan Monitor" ><?php echo $record['note']; ?></textarea>
																</div>
															  <?php echo form_error('catatan', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div> 	
													
													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="javascript:history.back()" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
								
                                </div>
		</section>
</html>
