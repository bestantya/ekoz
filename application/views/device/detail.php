<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('device'); ?>">Device Suport</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Detail Aset Device Suport</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Detail Aset Device Suport <span> Disini anda bisa melakukan pengelolaan data aset Device Suport.</b> </span>
                                </h1>
                </div>

				<div id="main-content">
					<div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab"><i class="icon-list-2"></i> DETAIL</a></li>
                                    	<li><a href="#" data-target="#math" data-toggle="tab"><i class="icon-edit"></i> EDIT</a></li>                                    
										<li><a href="#" data-target="#revenue" data-toggle="tab"><i class="icon-history"></i> HISTORY</a></li>
										<li class="pull-right"><a href="<?php echo site_url('device'); ?>" class="text-muted"><i class="fa fa-remove"></i></a></li>
                                    </ul>
									
                                    <div class="tab-content">
									
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Informasi Aset Device Suport
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="analytics-tab-content">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icol-chart-organisation"></i> Information Detail Aset Device Suport</th>
														</tr>
														</thead>
                                            	<tbody>
														<tr>
															<th>No. Inventaris :</th>
															<td style="width:80%"><?php echo $recordall['kode_network'] ?></td>
														</tr>
														<tr>
															<th>Type Device Suport :</th>
															<td><?php echo $recordall['jenis_network']?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $recordall['spesifikasi']?></td>                    
														</tr>
														<tr>
															<th>Tgl. Inventaris :</th>
															<td><?php echo tgl_lengkap($recordall['tgl_inv'])?></td>                    
														</tr>
														<tr>
															<th>Lokasi :</th>
															<td><?php echo $recordall['lokasi']?></td>                    
														</tr>
														<tr>
															<th>Status :</th>
															<td><?php echo $recordall['status']?></td>                    
														</tr>																											
														<tr>
															<th>Harga Beli :</th>
															<td><?php echo 'Rp '.rupiah($recordall['harga_beli'])?></td>                    
														</tr>

													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
                                    	<div id="math" class="tab-pane">
                                        	 <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Edit Informasi Aset Device Suport
                                                    </label>
                                                </div>
                                              </div>
										
								

										<div class="analytics-tab-header clearfix">
											 <div class="col-md-5">
											<?php echo form_open('device/edit'); ?>                            
											<div class="box-body">
													<div class="control-group">
													   <label class="control-label">No. Inventaris <span class="required">*</span></label>
														<input type="hidden"  class="span6" name="kode" value="<?php echo $record['kode_network'] ?>" >
														<input type="text" class="span6" name="no_inv" disabled class="form-control" id="inputError" value="<?php echo $record['kode_network']; ?>" >
													</div>                                            
													
													<div class="control-group">
													<label class="control-label">Device Type <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="jenis" 
																  class="form-control" required oninvalid="setCustomValidity('Jenis Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Jenis Device ex: Switch, HUB, Router" value="<?php echo $record['jenis_network']; ?>" >															  
															  </div>
															  <?php echo form_error('jenis_network', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Device" ><?php echo $record['spesifikasi']; ?></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
											
													<div class="control-group">
													<label class="control-label">Lokasi Penggunaan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="lokasi" 
																  class="form-control" required oninvalid="setCustomValidity('Lokasi Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan lokasi Device." value="<?php echo $record['lokasi']; ?>" >															  
															  </div>
															  <?php echo form_error('lokasi', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
											
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventaris <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_inv" value="<?php echo $record['tgl_inv']; ?>" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
                                                    </div>	
													
													<div class="form-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">
															<option value='' selected="selected">- Pilih Status Aset -</option>		
																 <?php                                            
																	$status=$this->db->get("tb_status");
																	foreach ($status->result() as $r) {
																	  echo "<option value='$r->nama_status'";
																	  echo $record['status'] == $r->nama_status ? 'selected' : '';
																	  echo">".$r->nama_status."</option>";
																	}
																  ?>
															</select>
															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>													

													<div class="control-group">
													<label class="control-label">Harga Beli <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6"  name="harga" 
																  class="form-control" required oninvalid="setCustomValidity('Harga Beli Device Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Harga Beli Device" value="<?php echo $record['harga_beli']; ?>">
															  <?php echo form_error('harga', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
								
													</div>
													<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('device'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>

												</div>
											
											</form>
											</div>
											 </div>				
								 </div>	
								 
																
                                    	<div id="revenue" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Data History Pemakai Aset Device Suport
                                                    </label>
                                                </div>
                                            </div>
											<div class="analytics-tab-content">
											
													 <div class="col-md-10">  
												  <h4>History / Mutasi [ <a><?php echo anchor('device/history/'.$recordall['kode_network'],'Add New') ?></a> ]</h4>                         
												  <table class="table ">
												  <br>
													<tr>
													  <td><label>Tanggal</label></td>
													  <td><label>Admin</label></td>
													  <td><label>Status</label></td>
													  <td><label>Lokasi</label></td>                             
													  <td><label>Note</label></td>
													  <td><label>Aksi</label></td>                              
													</tr>                            
													 <?php 
														foreach ($history as $s) {
														  echo"
															<tr>                                     
															  <td>".tgl_lengkap($s->tgl_update)."</td>
															  <td>".$s->admin."</td>
															  <td>".$s->status."</td>                                      
															  <td>".$s->lokasi."</td>
															  <td>".$s->note."</td> 
															  <td>" . anchor('device/edithistory/' . $s->id_history, '<i class="btn btn-danger icon-edit" data-toggle="tooltip" title="Edit"></i>')." | <a href=".base_url('device/print_history/'.$s->id_history)." target='_blank' ><i class='btn btn-success icon-print' data-toggle='tooltip' title='Print'></i></td>                                     
															</tr> ";
														}
													 ?>                      
												  </table>
												</div>
						
											</div>
										</div>
                                    </div>
              

                  </div>
                </div>


	</section>	 
</html>