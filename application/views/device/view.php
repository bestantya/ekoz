<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Device Suport</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data Device Suport<span> Disini anda bisa melakukan pengelolaan data device suport.</b> </span>
                                </h1>
                </div>
 
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Aset Device Suport
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('device/add'); ?>"  class="btn btn-primary icon-plus"> Tambah Aset Device Suport</a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                           <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>


							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th>Device Type</th>
														<th>Spesifikasi</th> 
														<th>Lokasi Penggunaan</th>                              
														<th>Status</th>                           
														<th>Edit</th>   
														<th>Delete</th>                                  
													</tr>
                                                    </thead>
                                              <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
														<td>".$r->kode_network."</td>
														<td>".$r->jenis_network."</td>
														<td>".$r->spesifikasi."</td>
														<td>".$r->lokasi."</td>
														<td>".$r->status."</td>														
													   <td>" . anchor('device/detail/' . $r->kode_network, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="Detail"></i>') . "</td>
													   <td>" . anchor('device/delete/' . $r->kode_network, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
											   
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>

