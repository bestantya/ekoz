<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-atom"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Supplier</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Master Supplier <span> Disini anda bisa melakukan pengelolaan data master supplier.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Users
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('supplier/add'); ?>"  class="btn btn-primary icon-add-contact"> Tambah Supplier </a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>


							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
                                                    <tr>
													<th>No.</th>
													<th>Nama Supplier</th>
													<th>Alamat </th>
													<th>Telepon</th>                                                              
													<th>Edit</th>   
													<th>Delete</th>                                  
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->nama_supplier."</td>
													   <td>".$r->alamat_supplier."</td>
													   <td>".$r->telepon."</td>							   					   
													   <td>" . anchor('supplier/edit/' . $r->id_supplier, '<i class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('supplier/delete/' . $r->id_supplier, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>
