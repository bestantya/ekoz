<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Supplier</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Supplier <span> Disini anda bisa melakukan pengelolaan data master supplier.</b> </span>
                                </h1>
                </div>            
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Supplier</span>
                                     </div>
										<?php echo form_open('supplier/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Nama Supplier <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Supplier Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Supplier" >
																</div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													
													<div class="control-group">
													<label class="control-label">Alamat <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="alamat" 
																  class="form-control" required oninvalid="setCustomValidity('Alamat Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Alamat" ></textarea>
																</div>
															  <?php echo form_error('alamat', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													


													<div class="control-group">
													<label class="control-label">Telepon <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="telepon" 
																  class="form-control" required oninvalid="setCustomValidity('No Telepon Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan No Telepon " >
															  <?php echo form_error('telepon', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
    													
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('supplier'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>
