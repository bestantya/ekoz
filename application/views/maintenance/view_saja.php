<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-bars"></i>Laporan
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Maintenance</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Maintenance <span> Disini anda bisa melakukan pengelolaan data perbaikan aset.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Maintenance Aset
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                                <form class="search-form">
                                                    <input type="text" class="span12 search-query" placeholder="Ketik data yang anda cari disini,..">
                                                </form>
                                            </div>

							<div class="widget-content table-container">
                                    <table id="demo-dtable-02" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th>No Permohonan</th>
														<th width="10%">No. Inventaris</th>
														<th>Jenis Inventaris</th> 
														<th>Tgl. Permohonan</th>
														<th>Maintenance Type</th>                                   
														<th>Catatan Permohonan</th>                           
														<th>Status</th>														
														<th>Level</th>
														<th>View</th> 
														<th>Print</th>    
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->no_permohonan."</td>
											   			<td>".$r->no_inventaris."</td>
														<td>".$r->nama_kategori."</td>
														<td>".$r->tgl_permohonan."</td>
														<td>".$r->jenis_permohonan."</td>	
														<td>".$r->catatan_pemohon."</td>	
														<td>".$r->status."</td>	
														<td>".$r->level."</td>															
													   <td>" . anchor('maintenance/detailsaja/' . $r->no_permohonan, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('maintenance/cetak/' .  $r->no_permohonan, '<i class="btn btn-sm btn-success icon-print" data-toggle="tooltip" title="Print"></i>') . "</td>
													   
													   </tr>";
												   $no++;
											   }
											   ?>
											   
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>


