<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-paper-airplane"></i>Ticket
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('maintenance'); ?>">Maintenance</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Detail Maintenance</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Detail Maintenance <span> Disini anda bisa melakukan pengelolaan data maintenance.</b> </span>
                                </h1>
                </div>

				<div id="main-content">
					<div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab"><i class="icon-list-2"></i> DETAIL</a></li>
                                    	<li><a href="#" data-target="#math" data-toggle="tab"><i class="icon-edit"></i> EDIT</a></li>
										<li class="pull-right"><a href="<?php echo site_url('maintenance'); ?>" class="text-muted"><i class="fa fa-remove"></i></a></li>
                                    </ul>
									
                                    <div class="tab-content">
									
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Data Maintenance
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="analytics-tab-content">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icon-tools"></i> Information Detail Data Maintenance</th>
														</tr>
														</thead>
                                            	<tbody>
														<tr>
															<th>No Permohonan/ Ticket :</th>
															<td style="width:80%"><?php echo $recordall['no_permohonan'] ?></td>
														</tr>
														<tr>
															<th>Tgl. Permohonan :</th>
															<td><?php echo tgl_lengkap($recordall['tgl_permohonan'])?></td>                  
														</tr>
														<tr>
															<th>Maintenance Type :</th>
															<td><?php echo $recordall['jenis_permohonan']?></td>                    
														</tr>
														<tr>
															<th>Kategori Inventaris :</th>
															<td><?php echo $recordall['nama_kategori']?></td>                    
														</tr>
														<tr>
															<th>No Inventaris :</th>
															<td><?php echo anchor(''.$recordall['nama_kategori'].'/detail/'.$recordall['no_inventaris'],$recordall['no_inventaris'])?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $inv['spesifikasi']?></td>                    
														</tr>

														 <tr>
														  <th>User Pengguna :</th>
														  <td><?php echo $inv['nama_pengguna']?></td>                   
														</tr>
														<tr>
														  <th>Catatan Permohonan :</th>
														  <td><?php echo $recordall['catatan_pemohon']?></td>                    
														</tr>
														<tr>
														  <th>Level :</th>
														  <td>
														  <?php 
															  if ($recordall['level'] =="LOW"){
																  $level="<span class='label label-success'>" . $recordall['level']. "</span>";
															  }elseif($recordall['level'] =="HIGH") {
																  $level="<span class='label label-important'>" .$recordall['level']."</span>";
															  }else{
																   $level="<span class='label label-warning'>" .$recordall['level']."</span>";
															  }  
															echo $level;
															?>                             
														  </td> 
														  </tr>
														<tr>
														  <th>Tgl. Selesai :</th>
														  <td><?php echo tgl_lengkap($recordall['tgl_selesai'])?></td>                    
														</tr>
														 <tr>
														  <th>Catatan Perbaikan :</th>
														  <td><?php echo $recordall['catatan_perbaikan']?></td>                    
														</tr>
														<tr>
														  <th>Nama Supplier :</th>
														  <td><?php echo strtoupper($recordall['nama_supplier'])?></td>                    
														</tr>
														<tr>
														  <th>Biaya :</th>
														  <td><?php echo "Rp ".rupiah($recordall['biaya'])?></td>                    
														</tr>
														 
														<tr>
															<th>Status :</th>
															<td>
														  <?php 
															  if ($recordall['status'] =="OPEN"){
																  $status="<span class='label label-important'>" . $recordall['status']. "</span>";
															  }elseif($recordall['status'] =="CLOSED") {
																  $status="<span class='label label-success'>" .$recordall['status']."</span>";
															  }else{
																   $status="<span class='label label-warning'>" .$recordall['status']."</span>";
															  }  
															echo $status;
															?>                             
														  </td>                     
														</tr>
														
														
													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>
            
                        
                                       
									 </div>   
							


			  <div class="analytics-tab-content">
								  
										<div class="widget">
                                            <div class="widget-header">
                                                <span class="title">
                                                    <i class="icon-comments"></i> History Proses Perbaikan
                                                </span>
												                                            <div class="toolbar">
                                                <span class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </span>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#"><i class="icol-lightbulb"></i> Available</a></li>
                                                    <li><a href="#"><i class="icol-cross-shield-2"></i> Busy</a></li>
                                                    <li><a href="#"><i class="icol-clock"></i> Away</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icol-disconnect"></i> Disconnect</a></li>
                                                </ul>
                                            </div>
                                            </div>
												
											 
                                            <div class="widget-content chat-box">
                                            	<ul class="thumbnails">
												<?php 
											if(!empty($detail)){
												foreach ($detail->result() as $r) {
													if($r->user=="Admin"){
												  echo '
	                                                <li class="others">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/icon.png').' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="">'.$r->catatan.'</div></strong></p>
															<p>Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</p>
	                                                    </div>
	                                                </li>
													';
													}else {
														echo '
													
													<li class="me">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/'.$r->foto).' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="">'.$r->catatan.'</div></strong></p>
															<p><div class="time">Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</div></p>
	                                                    </div>
	                                                </li>
													'; }
												  }
												}              
											  ?>   
								</ul>
								
								
								

                                                <div class="message-form">
												<?php echo form_open('maintenance/chat_simpan'); ?>  
												  <form class="form-vertical">
                                                    <div class="row-fluid">
																<div class="input-group">
																	<label class="control-label">Komentar <span class="required">*</span></label>
																	<div class="controls">
																
																	 <textarea name="catatan" class="span12" placeholder="Balas pesan anda disini atau tambahkan catatan" 
																	 required oninvalid="setCustomValidity('Komentar harus di Isi sebelum dikirim!')"
																	  oninput="setCustomValidity('')" ></textarea>
																	  
																 <input type="test" hidden name="kode" value="<?php echo $recordall['no_permohonan'] ?>">
																 
															
																  </div>
																  	<?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
																  <br>
																</div>
															  
															<div class="form-actions">
															
																<a href="<?php echo site_url('maintenance'); ?>" name="submit" class="btn ticket_btn pull-right"><i class=" icon-bended-arrow-left" aria-hidden="true"></i> Kembali</a> 
																<button type="reset" name="submit" class="btn btn-warning pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
																<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-paper-airplane" aria-hidden="true" ></i> Kirim</button>
															</div>
															</div>
														
													</div>
													
													 </form>
												</div>
                                            </div>
							</div>	</div>  



									
                        <div id="math" class="tab-pane">
                                        	 <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Edit Informasi Maintenance
                                                    </label>
                                                </div>
                                              </div>            	
										
									<div class="analytics-tab-header clearfix">
											<div class="col-md-5">
												<?php echo form_open('maintenance/update'); ?>                            
												<div class="box-body">
												<div class="control-group">
												   <label class="control-label">No Permohonan/ Ticket <span class="required">*</span></label>
													<input type="hidden"  class="span6" name="no_permohonan" value="<?php echo $recordall['no_permohonan'] ?>" >
													<input type="text" class="span6" name="no_permohonan" disabled class="form-control" id="inputError" value="<?php echo $recordall['no_permohonan']; ?>" >
												</div>                                            
							   
													<div class="control-group">
													<label class="control-label">Jenis Inventaris <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" disabled class="span6"  name="kategori" value="<?php echo $recordall['nama_kategori']; ?>" 
																  class="form-control" required oninvalid="setCustomValidity('Merek/brand Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Merek/Brand ex : ASUS, LENOVO" >
																</div>
														</div>
													</div>
															
													<div class="control-group">
													<label class="control-label">No.Inventaris <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" disabled class="span6"  name="no_inv" value="<?php echo $recordall['no_inventaris']; ?>" 
																  class="form-control" required oninvalid="setCustomValidity('Merek/brand Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Merek/Brand ex : ASUS, LENOVO" >
																</div>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Maintenance Type <span class="required">*</span></label>
														<div class="controls">
														   <select id="inventaris" name="type" class="select2-select-00 span6">   												   
																<option value='<?php echo $recordall['jenis_permohonan']; ?>'><?php echo $recordall['jenis_permohonan'];'selected' ?></option>     
																<option value="Hardware">Hardware</option>                               
																<option value="Software">Software</option> 
																<option value="Hardware & Software">Hardware & Software</option> 
																<option value="Network">Network/ Jaringan</option> 
															</select> 															
															<?php echo form_error('type', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tgl. Inventari <span class="required">*</span></label>
                                                        <div class="controls"  class="datepicker-inline">
															<input id="zebradp-basic" disabled  type="text" name="tgl_inv" value="<?php echo tgl_lengkap($recordall['tgl_permohonan'])?> " class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_inv', '<div class="text-red">', '</div>'); ?>
                                                    </div>	
													
													<div class="control-group">
													<label class="control-label">Note/ Catatan Pemohon <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan" 
																  class="form-control" required oninvalid="setCustomValidity('Note / Catatan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Note / Catatan Status Pemohon " ><?php echo $recordall['catatan_pemohon']; ?></textarea>
																</div>
															  <?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label">Level <span class="required">*</span></label>
														<div class="controls">
															<select name="level" class="select2-select-00 span6">
															<option value='<?php echo $recordall['level']; ?>'><?php echo $recordall['level'];'selected' ?></option>        
															  <option value="LOW">LOW</option>                               
															  <option value="NORMAL">NORMAL</option> 
															  <option value="HIGH">HIGH</option>
															</select>
															<?php echo form_error('level', '<div class="text-red">', '</div>'); ?>	
													</div> 
													
													<div class="control-group">
                                                        <label class="control-label" for="dp-cmy">Tanggal Sekarang</label>
                                                        <div class="datepicker-inline"></div>
														<br>
                                                        <label class="control-label" for="dp-cmy">Tanggal Selesai <span class="required">*</span></label>
                                                        <div class="controls" class="datepicker-inline">
															<input id="zebradp-week" type="text" name="tgl_selesai" value="<?php echo $recordall['tgl_selesai']; ?>" class="span6" data-date-format="yyyy-mm-dd" required oninvalid="setCustomValidity('Tgl. Inventaris harus di isi')"
															oninput="setCustomValidity('')" placeholder="yyyy-mm-dd" >
                                                        </div>
														<?php echo form_error('tgl_selesai', '<div class="text-red">', '</div>'); ?>
                                                    </div>						

													<div class="control-group">
													<label class="control-label">Note/ Catatan Perbaikan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="catatan_perbaikan" 
																  class="form-control" required oninvalid="setCustomValidity('Note / Catatan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Note / Catatan Perbaikan " ><?php echo $recordall['catatan_perbaikan']; ?></textarea>
																</div>
															  <?php echo form_error('catatan_perbaikan', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Nama Supplier/Rekanan <span class="required">*</span></label>
														<div class="controls">
														   <select id="supplier" name="supplier" class="select2-select-00 span6">   												   
																<option value=''>- Select Nama Supplier -</option>     
																  <?php
																	foreach ($record as $k) {
																		echo "<option value='$k->nama_supplier'";
																		echo $recordall['nama_supplier'] == $k->nama_supplier ? 'selected' : '';
																		echo">$k->nama_supplier</option>";
																	}
																  ?>
															</select> 
															*Note : di isi ketika service di rekanan															
															<?php echo form_error('supplier', '<div class="text-red">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Cost /Biaya <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="number" class="span6" name="biaya" value="<?php echo $recordall['biaya']; ?>"
																  class="form-control" required oninvalid="setCustomValidity('Cost /Biaya Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Cost /Biaya" >
																</div>
															  <?php echo form_error('sn', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
										
										
													<div class="form-group">
														<label class="control-label">Status <span class="required">*</span></label>
														<div class="controls">
															<select name="status" class="select2-select-00 span6">
															<option value='<?php echo $recordall['status']; ?>'><?php echo $recordall['status'];'selected' ?></option>        
															  <option value="OPEN">OPEN</option>                               
															  <option value="PROCESS">PROCESS</option> 
															  <option value="PENDING">PENDING</option>
															  <option value="CLOSED">CLOSED</option>   
															</select>
															<?php echo form_error('status', '<div class="text-red">', '</div>'); ?>	
													</div> 
													</div>                                              
													</div>

                            						<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('maintenance'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>

												
												</div>
											
													</form>
													</div>
													 </div>				
										 </div>	

                                    </div>
                  </div>
             </div>


	</section>	 
</html>