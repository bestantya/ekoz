<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-bars"></i>Laporan
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('maintenance/view_saja'); ?>">Maintenance</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Detail Maintenance</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Detail Maintenance <span> Disini anda bisa melakukan pengelolaan data maintenance.</b> </span>
                                </h1>
                </div>

				<div id="main-content">
					<div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab"><i class="icon-list-2"></i> DETAIL</a></li>
										<li class="pull-right"><a href="<?php echo site_url('maintenance'); ?>" class="text-muted"><i class="fa fa-remove"></i></a></li>
                                    </ul>
									
                                    <div class="tab-content">
									
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Detail Data Maintenance
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="analytics-tab-content">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
													  <thead>
														<tr>
															<th colspan="2"><i class="icon-tools"></i> Information Detail Data Maintenance</th>
														</tr>
														</thead>
                                            	<tbody>
														<tr>
															<th>No Permohonan/ Ticket :</th>
															<td style="width:80%"><?php echo $recordall['no_permohonan'] ?></td>
														</tr>
														<tr>
															<th>Tgl. Permohonan :</th>
															<td><?php echo tgl_lengkap($recordall['tgl_permohonan'])?></td>                  
														</tr>
														<tr>
															<th>Maintenance Type :</th>
															<td><?php echo $recordall['jenis_permohonan']?></td>                    
														</tr>
														<tr>
															<th>Kategori Inventaris :</th>
															<td><?php echo $recordall['nama_kategori']?></td>                    
														</tr>
														<tr>
															<th>No Inventaris :</th>
															<td><?php echo anchor(''.$recordall['nama_kategori'].'/detail/'.$recordall['no_inventaris'],$recordall['no_inventaris'])?></td>                    
														</tr>
														<tr>
															<th>Spesifikasi :</th>
															<td><?php echo $inv['spesifikasi']?></td>                    
														</tr>

														 <tr>
														  <th>User Pengguna :</th>
														  <td><?php echo $inv['nama_pengguna']?></td>                   
														</tr>
														<tr>
														  <th>Catatan Permohonan :</th>
														  <td><?php echo $recordall['catatan_pemohon']?></td>                    
														</tr>
														<tr>
														  <th>Level :</th>
														  <td>
														  <?php 
															  if ($recordall['level'] =="LOW"){
																  $level="<span class='label label-success'>" . $recordall['level']. "</span>";
															  }elseif($recordall['level'] =="HIGH") {
																  $level="<span class='label label-important'>" .$recordall['level']."</span>";
															  }else{
																   $level="<span class='label label-warning'>" .$recordall['level']."</span>";
															  }  
															echo $level;
															?>                             
														  </td> 
														  </tr>
														<tr>
														  <th>Tgl. Selesai :</th>
														  <td><?php echo tgl_lengkap($recordall['tgl_selesai'])?></td>                    
														</tr>
														 <tr>
														  <th>Catatan Perbaikan :</th>
														  <td><?php echo $recordall['catatan_perbaikan']?></td>                    
														</tr>
														<tr>
														  <th>Nama Supplier :</th>
														  <td><?php echo strtoupper($recordall['nama_supplier'])?></td>                    
														</tr>
														<tr>
														  <th>Biaya :</th>
														  <td><?php echo "Rp ".rupiah($recordall['biaya'])?></td>                    
														</tr>
														 
														<tr>
															<th>Status :</th>
															<td>
														  <?php 
															  if ($recordall['status'] =="OPEN"){
																  $status="<span class='label label-important'>" . $recordall['status']. "</span>";
															  }elseif($recordall['status'] =="CLOSED") {
																  $status="<span class='label label-success'>" .$recordall['status']."</span>";
															  }else{
																   $status="<span class='label label-warning'>" .$recordall['status']."</span>";
															  }  
															echo $status;
															?>                             
														  </td>                     
														</tr>
														
														
													  </tbody>
												</table>
													</div>
                                                    </div>
                                                </div>
            
                        
                                       
									 </div>   
							


			  <div class="analytics-tab-content">
								  
										<div class="widget">
                                            <div class="widget-header">
                                                <span class="title">
                                                    <i class="icon-comments"></i> History Proses Perbaikan
                                                </span>
												                                            <div class="toolbar">
                                                <span class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </span>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#"><i class="icol-lightbulb"></i> Available</a></li>
                                                    <li><a href="#"><i class="icol-cross-shield-2"></i> Busy</a></li>
                                                    <li><a href="#"><i class="icol-clock"></i> Away</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#"><i class="icol-disconnect"></i> Disconnect</a></li>
                                                </ul>
                                            </div>
                                            </div>
												
											 
                                            <div class="widget-content chat-box">
                                            	<ul class="thumbnails">
												<?php 
											if(!empty($detail)){
												foreach ($detail->result() as $r) {
													if($r->user=="Guest"){
												  echo '
	                                                <li class="others">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/icon.png').' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="">'.$r->catatan.'</div></strong></p>
															<p>Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</p>
	                                                    </div>
	                                                </li>
													';
													}else {
														echo '
													
													<li class="me">
	                                                    <div class="thumbnail">
	                                                        <img src='.base_url('assets/images/'.$r->foto).' style="width: 70px; height: 65px;">  
	                                                    </div>
	                                                    <div class="message">
	                                                        <span class="name">'.strtoupper($r->user).'</span>
	                                                       	<p><strong><div class="">'.$r->catatan.'</div></strong></p>
															<p><div class="time">Pesan dikirim : '.tgl_lengkap($r->tgl_proses).' - '.timeAgo($r->tgl_proses).'</div></p>
	                                                    </div>
	                                                </li>
													'; }
												  }
												}              
											  ?>   
								</ul>
								
								
								

                                                <div class="message-form">
												<?php echo form_open('maintenance/chat_simpansaja'); ?>  
												  <form class="form-vertical">
                                                    <div class="row-fluid">
																<div class="input-group">
																	<label class="control-label">Komentar <span class="required">*</span></label>
																	<div class="controls">
																
																	 <textarea name="catatan" class="span12" placeholder="Balas pesan anda disini atau tambahkan catatan" 
																	 required oninvalid="setCustomValidity('Komentar harus di Isi sebelum dikirim!')"
																	  oninput="setCustomValidity('')" ></textarea>
																	  
																 <input type="test" hidden name="kode" value="<?php echo $recordall['no_permohonan'] ?>">
																 
															
																  </div>
																  	<?php echo form_error('catatan', '<div class="text-red">', '</div>'); ?>
																  <br>
																</div>
															  
															<div class="form-actions">
															
																<a href="<?php echo site_url('maintenance/view_saja'); ?>" name="submit" class="btn ticket_btn pull-right"><i class=" icon-bended-arrow-left" aria-hidden="true"></i> Kembali</a> 
																<button type="reset" name="submit" class="btn btn-warning pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
																<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-paper-airplane" aria-hidden="true" ></i> Kirim</button>
															</div>
															</div>
														
													</div>
													
													 </form>
												</div>
                                            </div>
							</div>	</div>  



                  </div>
             </div>


	</section>	 
</html>