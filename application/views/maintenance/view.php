<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-paper-airplane"></i>Ticket
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Maintenance</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Maintenance <span> Disini anda bisa melakukan pengelolaan data perbaikan aset.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Maintenance Aset
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('maintenance/add'); ?>"  class="btn btn-primary icon-plus"> Tambah Maintenance</a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>

							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th>No Permohonan</th>
														<th width="10%">No. Inventaris</th>
														<th>Jenis Inventaris</th> 
														<th>Tgl. Permohonan</th>
														<th>Maintenance Type</th>                                   
														<th>Catatan Permohonan</th>                           
														<th>Status</th>   
														<th>Level</th>
														<th>View</th> 
														<th>Print</th>    
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->no_permohonan."</td>
											   			<td>".$r->no_inventaris."</td>
														<td>".$r->nama_kategori."</td>
														<td>".$r->tgl_permohonan."</td>
														<td>".$r->jenis_permohonan."</td>	
														<td>".$r->catatan_pemohon."</td>	
														<td>".$r->status."</td>	
														<td>".$r->level."</td>															
													   <td>" . anchor('maintenance/detail/' . $r->no_permohonan, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('maintenance/cetak/' .  $r->no_permohonan, '<i class="btn btn-sm btn-success icon-print" data-toggle="tooltip" title="Print"></i>') . "</td>
													   
													   </tr>";
												   $no++;
											   }
											   ?>
											   
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>


