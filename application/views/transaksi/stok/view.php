<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-list"></i>Transaksi
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Stok Bareng</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Stok Data Barang <span> Disini anda bisa melakukan pengelolaan stok data barang.</b> </span>
                                </h1>
                </div>
<script src="<?php echo base_url('assets/web/js/datatables/jquery-1.11.3.min.js'); ?>"></script>
    <script>
    $(document).ready(function(){ 
		$.fn.dataTable.ext.errMode = 'throw'; 
        $('#tb-stok').dataTable( {                  
        "Processing": true, 
        "ServerSide": true,
        "iDisplayLength": 25,
        "oLanguage": {
                    "sSearch": "Search Data :  ",
                    "sZeroRecords": "No records to display",
                    "sEmptyTable": "No data available in table"
                    },
        "dom": 'Bfrtip',
        "select": true,
        "buttons": [
            {
                "extend": 'collection',
                "text": 'Export',
                "buttons": [
                    'copy',
                    'excel',
                    'pdf',
                    'print',
                ]
            }
            ],
        "ajax": "<?php echo base_url('stok/view_data_stok');?>",
        "columns": [
                { "mData": "no" },
                { "width":"12%","mData": "kode_barang" },
                { "mData": "nama_kategori" },                           
                { "mData": "merek_barang" },
                { "mData": "nama_barang" },                            
                { "className": "dt-body-left","width":"20%","mData": "spesifikasi" },                                           
                { "className": "dt-center","mData": "stok" }, 
                { "mData": "satuan" },
                { "mData": "view" },  
                ]
            } );
        });
</script>
                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List stok Data Barang
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                                <form class="search-form">
                                                    <input type="text" class="span12 search-query" placeholder="Ketik data yang anda cari disini,..">
                                                </form>
                                            </div>

							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">Kode Barang</th>
														<th>Kategori</th>
														<th>Nama</th>
														<th>Merek</th>
														<th>Spesifikasi</th>                                                               
														<th>Stok</th>
														<th>Satuan</th> 
														<th>View</th>                                  
													</tr>
                                                    </thead>

                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>
