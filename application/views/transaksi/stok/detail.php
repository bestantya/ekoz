<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-list"></i>Transaksi
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('stok'); ?>">Stok</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Kartu Stok Barang</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Kartu Stok Barang<span> Disini anda bisa melakukan pengelolaan data Kartu Stok Barang.</b> </span>
                                </h1>
                            </div>              
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Kartu Stok Barang</span>
                                     </div>
										<?php echo form_open('monitor/history'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">
											
										
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="widget-content table-container">
													  <table class="table table-striped table-detail-view">
														  <thead>
															<tr>
																<th colspan="2"><i class="icos-list"></i> Information Detail Kartu Stok Barang</th>
															</tr>
															</thead>
															<tbody>
															<tr>
																<th>Kode Barang :</th>
																<td style="width:80%"><?php echo $record['kode_barang']?></td>
															</tr>
															<tr>
																<th>Kategori Barang :</th>
																<td><?php echo $record['nama_kategori']?></td>                    
															</tr>
															<tr>
																<th>Nama Barang :</th>
																<td><?php echo $record['nama_barang']?></td>                    
															</tr>
															<tr>
																<th>Spesifikasi :</th>
																<td><?php echo $record['spesifikasi']?></td>                    
															</tr>
		
														  </tbody>
													</table>
													</div>
                                                 
                                                
													  <table class="table table-bordered table-striped">
														<thead>
														  <tr>
															<th>Tgl. Transaksi</th>
															<th>No. Transaksi</th>
															<th>Keterangan</th>
															<th style="width:10%" class="text-center">Qty Masuk</th>
															<th style="width:10%" class="text-center">Qty Keluar</th>
														  </tr>
														<thead>
														<tbody>
														  <tr>
															<?php 
															  $stok=chek_stok($record['kode_barang']);
															  foreach ($detail as $r ) {                    
																echo'
																  <tr>
																  <td>'.tgl_indo($r->tgl_transaksi).'</td>
																  <td>'.$r->kode_transaksi.'</td>
																  <td>'.$r->catatan.'</td>                            
																  <td class="text-center">'.$r->qty_masuk.'</td> 
																  <td class="text-center">'.$r->qty_keluar.'</td> 
																  </tr>';
															  }
															?>
														  </tr>
														  <tr>
															<th colspan="3" class="text-center">STOK AKHIR </th>
															<th colspan="2" class="text-center"><?php echo $stok ?></th>
														  </tr>              
														</tbody>
													  </table>          
												
													</div>
												</div>
						  					
											
												<div class="form-actions">
													<a href="<?php echo site_url('stok/detail_print/'.$record["kode_barang"].''); ?>" target="_blank" class="btn btn-success" aria-hidden="true"  data-toggle="tooltip" title="Print"><i class="icon-print"></i> Print</a>
													<a href="<?php echo site_url('stok'); ?>" data-toggle="tooltip" title="Cencel" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
												</div>
											</form>
										</div>		
									</div>                                  



       

                                </div>
		</section>
</html>





