<body onload="load_data_temp()"></body>
<script type="text/javascript" src="<?php echo base_url('assets/web/jQuery-2.1.3.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  $("#kategori").change(function(){
      loadbarang();
        });
  $("#namabarang").change(function(){
      loadspek();
        });
    });
</script>

<script type="text/javascript">

function loadbarang(){
    var kategori=$("#kategori").val();
    $.ajax({
        url:"<?php echo base_url('masuk/tampilbarang');?>",
        data:"kategori=" + kategori ,
        success: function(html) {
           $("#namabarang").html(html);
        }
    });
}

function loadspek() {
    var namabarang=$("#namabarang").val();
    $.ajax({
        url:"<?php echo base_url('masuk/tampilspek');?>",
        data:"namabarang=" + namabarang ,
        success: function(html) {
           $("#spek").html(html);
        }
    });
}

function add_barang(){
    var barang=$("#namabarang").val();
    var qty=$("#qty").val();
    var harga=$("#harga").val();
    var catatan=$("#catatan").val();
    if (barang==''){
        alert('Pilih Nama Barang');
        die;
    }else if(qty==''){
        alert('Masukan QTY Barang');
        die;
    }else if(harga==''){
        alert('Masukan Harga Barang');
        die;
    }else if(catatan==''){
        alert('Input Catatan Barang');
        die;
    }else{
        //  $.ajax({
        //     type:"POST",
        //     url:"<?php echo base_url('masuk/masuk_ajax');?>",
        //     // data:"barang="+barang+"&qty="+qty+"&harga="+harga+"&catatan="+catatan,
        //     data:[{
        //
        //     }]
        //     success:function(html){
        //         load_data_temp();
        //     }
        // });

        $.ajax({
            url: "<?php echo site_url('masuk/masuk_ajax')?>",
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                load_data_temp();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Opps, Error Data..');
            }
        });
    }

}

function load_data_temp(){
$.ajax({
    type:"GET",
    url:"<?php echo base_url('masuk/load_temp');?>",
    data:"",
    success:function(html){
        $("#view").html(html);
        }
    })
}

function hapus(id){
    $.ajax({
       type:"GET",
       url:"<?php echo base_url('masuk/hapus_temp');?>",
       data:"id="+id,
       success:function(html){
           $("#dataku"+id).hide(1000);
       }
    });
}
</script>

<div id="sidebar-separator"></div>

     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-list"></i> Transaksi
                                        <span class="divider">&raquo;</span>
                                    </li>
									<li>
                                    	<a href="<?php echo site_url('masuk'); ?>">Barang Masuk</a>
										<span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Transaksi Barang Masuk</a>
                                    </li>
                                </ul>

                                <h1 id="main-heading">
                                	Transaksi Barang Masuk <span> Disini anda bisa melakukan pengelolaan data transaksi barang masuk.</b> </span>
                                </h1>
                </div>


				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Barang</span>
                                     </div>
										<div class="widget-content form-container">

											<div class="box-tools">
													<legend><h3 class="box-title"><?php echo tanggal_new() ;?></h3>  <h3 ><span class="title" class="semi-bold">No. Transaksi : <?php echo $kode ?></span></h3></legend>
														  </div>

										<?php
										echo form_open('masuk/add');
										?>
													<form id="validate-4" class="form-horizontal" method="post">
													<div class="control-group">
													<label class="control-label">Nama Supplier <span class="required">*</span></label>
													<div class="controls">
													<select name="supplier"  class="span6 select2-select" id="supplier" >
														<option value="" selected="selected">- Pilih Supplier -</option>
														<?php
															if (!empty($supplier)) {
															foreach ($supplier as $row) {
																echo "<option value=".$row->id_supplier.">".strtoupper($row->nama_supplier)."</option>";
																}
															}
															?>
														</select>
													<?php echo form_error('supplier', '<div class="text-blue">', '</div>'); ?>
													<label for="chosen1" generated="true" class="error" style="display:none;"></label>
												  </div> </div>

													<div class="control-group">
													<label class="control-label">Nomor PO <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="no_po"
																  class="form-control" required oninvalid="setCustomValidity('Nomer PO Barang Anda Masih Kosong!')"
																  oninput="setCustomValidity('')" placeholder="Masukan Nomer PO Barang" >
																</div>
															  <?php echo form_error('no_po', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Barang <span class="required">*</span></label>
													<div class="controls">
														<select name="kategori"  class="span6 select2-select" id="kategori" >
															<option value="" selected="selected">- Select Category Barang -</option>
															<?php
																if (!empty($kategori)) {
																	foreach ($kategori as $row) {
																		echo "<option value=".$row->id_kategori.">".strtoupper($row->nama_kategori)."</option>";
																	}
																}
															?>
														</select>
														<?php echo form_error('kategori', '<div class="text-blue">', '</div>'); ?>
														<label for="chosen1" generated="true" class="error" style="display:none;"></label>
													  </div>
													</div>

														<div class="control-group">
															<div class="controls">
														 <select name="namabarang"  class="span6" id="namabarang">
															<option value="" selected="selected">- Select Barang -</option>
														  </select>
														</div>
														<?php echo form_error('namabarang', '<div class="text-red">', '</div>'); ?>
													</div>

													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" id="spek"
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Barang Anda Masih Kosong!')"
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Barang" ></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">Qty <span class="required">*</span></label>
													<div class="controls">
														<div >
														<input type="number" class="span6"  name="qty" id="qty"
															class="form-control" required oninvalid="setCustomValidity('Jumlah Beli Barang Anda Masih Kosong!')"
															oninput="setCustomValidity('')" placeholder="Masukan Jumlah Beli Barang" >
															</div>
															<?php echo form_error('qty', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>


													<div class="control-group">
														<label class="control-label">Harga Beli <span class="required">*</span></label>
													<div class="controls">
														<div >
														<input type="number" class="span6"  name="harga" id="harga"
															class="form-control" required oninvalid="setCustomValidity('Harga Beli Barang Anda Masih Kosong!')"
															oninput="setCustomValidity('')" placeholder="Masukan Harga Beli Barang" >
															</div>
															<?php echo form_error('harga', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">Keterangan <span class="required">*</span></label>
																<div class="controls"><div >
																<textarea rows="3"  class="span6" name="catatan" id="catatan"
																class="form-control" required oninvalid="setCustomValidity('Keterangan Barang Anda Masih Kosong!')"
																 oninput="setCustomValidity('')" placeholder="Masukan keterangan Barang" ></textarea>
																</div>
															<?php echo form_error('catatan', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
												<div class="form-actions">
												<button type="button" onclick="add_barang()" class="btn btn-primary" name="add"><i class="glyphicon glyphicon-save"></i> Add Barang</button>
													<button type="submit" name="submit" class="btn btn-success "> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('masuk'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>

											</form>
										</div>
									</div>

			<div class="box-body table-responsive">
                <div id="view">
                </div>
            </div>


			  </div>
		</section>
</html>
