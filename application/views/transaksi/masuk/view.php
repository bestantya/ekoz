<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-list"></i> Transaksi
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Barang Masuk</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Barang Masuk <span> Disini anda bisa melakukan pengelolaan data transaksi barang masuk.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Barang Masuk
                                    </span>
                                    <div class="toolbar">
									
										<div class="btn-group">
										
													<span class="btn"> <a href="<?php echo base_url('masuk/add'); ?>"  class="btn btn-primary icon-gift"> Tambah Barang Masuk</a></span>

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                                <form class="search-form">
                                                    <input type="text" class="span12 search-query" placeholder="Ketik data yang anda cari disini,..">
                                                </form>
                                            </div>
							
							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped"><?php echo $this->session->flashdata('result_hapus'); ?>
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Transaksi</th>
														<th>Tgl. Transaksi</th>
														<th>No. PO</th>
														<th>Manufacture</th>                                                               
														<th>Spesifikasi</th>
														<th>QTY</th>
														<th>Harga</th>                                 
														<th>Keterangan</th>                                
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->kode_transaksi."</td>
											   			<td>".$r->tgl_transaksi."</td>
														<td>".$r->no_po."</td>
														<td>".$r->merek_barang."</td>
														<td>".$r->spesifikasi."</td>	
														<td>".$r->qty."</td>
														<td>".$r->harga."</td>	
														<td>".$r->catatan."</td>															
													   <td>" . anchor('barang/edit/' . $r->kode_barang, '<i class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('barang/delete/' . $r->kode_barang, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>



