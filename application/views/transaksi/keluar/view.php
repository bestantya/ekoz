<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-list"></i> Transaksi
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Barang Keluar</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Barang Keluar <span> Disini anda bisa melakukan pengelolaan data transaksi barang keluar.</b> </span>
                                </h1>
                </div>

<script src="<?php echo base_url('assets/js/plugins/datatables/jquery-1.11.3.min.js'); ?>"></script>
    <script>
    $(document).ready(function(){  
		$.fn.dataTable.ext.errMode = 'throw'; 
        $('#tb-keluar').dataTable( {                  
        "Processing": true, 
        "ServerSide": true,
        "iDisplayLength": 25,
        "oLanguage": {
                    "sSearch": "Search Data :  ",
                    "sZeroRecords": "No records to display",
                    "sEmptyTable": "No data available in table"
                    },
        "dom": 'Bfrtip',
        "select": true,
        "buttons": [
            {
                "extend": 'collection',
                "text": 'Export',
                "buttons": [
                    'copy',
                    'excel',
                    'pdf',
                    'print',
                ]
            }
            ],
        "ajax": "<?php echo base_url('keluar/view_data_keluar');?>",
        "columns": [
                { "mData": "no" },
                { "width":"12%","mData": "kode_transaksi" },
                { "mData": "tgl_transaksi" },                           
                { "mData": "nama_barang" },                                            
                { "className": "dt-body-left","width":"20%","mData": "spesifikasi" },
                { "mData": "qty" },                                                                
                { "mData": "catatan" },
                { "mData": "nama_pengguna" },
                { "mData": "nama" },
                { "mData": "cetak" },
                ]
            } );
        });
</script>

    
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Barang Keluar
                                    </span>
                                    <div class="toolbar">
									
										<div class="btn-group">
										
													<span class="btn"> <a href="<?php echo base_url('keluar/add'); ?>"  class="btn btn-primary icon-gift"> Tambah Barang Keluar</a></span>

													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                                <form class="search-form">
                                                    <input type="text" class="span12 search-query" placeholder="Ketik data yang anda cari disini,..">
                                                </form>
                                            </div>
							
							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped"><?php echo $this->session->flashdata('result_hapus'); ?>
                                                    <thead>
													<tr>
														<th>No.</th>
														<th>No Transaksi</th>
														<th>Tgl. Transaksi</th>
														<th>Nama Barang</th>                                                               
														<th>Spesifikasi</th>
														<th>QTY</th>                              
														<th>Keterangan</th>                           
														<th>Penerima</th>   
														<th>Sub.Departemen</th> 
														<th>aksi</th>                                  
													</tr>
                                                    </thead>
															
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->kode_transaksi."</td>
											   			<td>".$r->tgl_transaksi."</td>
														<td>".$r->merek_barang."</td>
														<td>".$r->spesifikasi."</td>	
														<td>".$r->qty."</td>
														<td>".$r->catatan."</td>	
														<td>".$r->nama_pengguna."</td>	
														<td>".$r->nama."</td>
														<td>".$r->cetak."</td>														
													   <td>" . anchor('barang/edit/' . $r->kode_barang, '<i class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('barang/delete/' . $r->kode_barang, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>
