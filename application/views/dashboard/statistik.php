<div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Dashboard
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Statistik</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data Statistik <span> Disini anda bisa melakukan pengelolaan data statistik .</b> </span>
                                </h1>
                            </div>

  <div id="main-content">

      
                             <div id="dashboard-demo" class="tabbable analytics-tab paper-stack">
                                	<ul class="nav nav-tabs">
                                        <li class="active"><a href="#" data-target="#live" data-toggle="tab"><i class="icon-tag"></i> Live Status Ticket</a></li>
										<li><a href="#" data-target="#revenue" data-toggle="tab"><i class="icon-bars"></i> Perbandingan</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="live" class="tab-pane active">
                                            <div class="analytics-tab-header clearfix">
                                                <div class="form-inline">
                                                    <label class="checkbox text-nowrap">
                                                        Data live persentase jumlah ticket, tiket masuk, ticket proses dan ticket selesai di update setiap detik.
                                                        
                                                    </label>
														<div class="btn-toolbar pull-right">
															<a href="<?php echo base_url('maintenance'); ?>" class="btn btn-primary"><i class="icon-download-to-computer"></i> Detail Ticket</a>
														</div>
                                                </div>
                                            </div>					
											
                                            <div class="divider"></div>
                                            <div class="analytics-tab-content">
                                                <div class="row-fluid">
												 <span class="span3">
                                                        <span class="stat circular inline">
                                                            <span id="cs-1" data-provide="circular" data-fill-color="#0088cc" data-value="100 " data-decimals="" data-radius="75" data-percent="true" data-thickness="25"></span>
                                                             <p><span class="text">All TICKET </span> </p>
                                                        </span>
                                                    </span>
													
                                                    <span class="span3">
                                                        <span class="stat circular inline">
                                                            <span id="cs-1" data-provide="circular" data-fill-color="#e95d3c" data-value="5 " data-decimals="" data-radius="75" data-percent="true" data-thickness="25"></span>
                                                             <p><span class="text">NEW TICKET </span> </p>
                                                        </span>
                                                    </span>
                                                    <span class="span3">
                                                        <span class="stat circular inline">
                                                            <span id="cs-2" data-provide="circular" data-fill-color="#ffa200" data-value="20 " data-decimals="" data-radius="75" data-percent="true" data-thickness="25"></span>
                                                            <p><span class="text">TICKET PROCCES</span></p>
                                                        </span>
                                                    </span>
                                                    <span class="span3">
                                                        <span class="stat block circular inline">
                                                            <span id="cs-3" data-provide="circular" data-fill-color="#a5d166" data-value="75 " data-decimals="" data-radius="75" data-percent="true" data-thickness="25"></span>
                                                             <p><span class="text">CLOSE TICKET</span> </p>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
  
  
                                    	<div id="revenue" class="tab-pane">
                                        	<div class="analytics-tab-header clearfix">
                                            	<div id="demo-chart-03-toolbar" class="pull-left form-inline">
												
                                                	<label class="checkbox"><input class="uniform" type="checkbox" name="dm1-0" checked> Target Revenue</label>
                                                	<label class="checkbox"><input class="uniform" type="checkbox" name="dm1-1" checked> Actual Revenue</label>
                                                </div>
                                            	<div class="btn-toolbar pull-right">
                                                	<a class="btn btn-primary"><i class="icon-download-to-computer"></i> Export Data</a>
                                                	<div class="btn-group">
                                                    	<a class="btn dropdown-toggle" data-toggle="dropdown">
                                                        	<i class="icon-cog-2"></i> <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right">
                                                        	<li><a href="#"><i class="icol-tag"></i> About Insights</a></li>
                                                            <li><a href="#"><i class="icol-help"></i> Help</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
											<div class="analytics-tab-content">
												<div id="demo-chart-03" style="height: 381px; "></div>
											</div>
										</div>
                                    </div>
                                </div>   
                            
                                
								<div class="analytics-tab-content">
								<div class="row-fluid">
                                    <div class="span6 widget">
                                        <div class="widget-header">
                                            <span class="title"><i class="icon-pie-chart"></i> Donut Chart</span>
                                        </div>
                                        <div class="widget-content">
                                            <div id="demo-charts-05" style="height:300px; "></div>
                                        </div>
                                    </div>
                                	<div class="span6 widget">
                                    	<div class="widget-header">
                                        	<span class="title"><i class="icon-stats-up"></i> Line Chart with Bars</span>
                                        </div>
                                        <div class="widget-content">
                                        	<div id="demo-charts-02" style="height:300px; "></div>
                                        </div>
                                    </div>
                                </div>
							</div>	
                               
			</div>					
        </section>

</html>