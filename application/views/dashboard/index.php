<div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Dashboard
                                        <span class="divider">&raquo;</span>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Dashboard <span> Selamat datang kembali,. <b><?php echo $this->session->userdata('username'); ?>. </b> Terakhir anda login : <b><?php echo $this->session->userdata('last_login'); ?></b> </span>
                                </h1>
                            </div>

                     
                            	
                             <div id="main-content">
							 	  <div class="divider"></div>
                          <div class="analytics-tab-content">
                            <h4 class="sub"><span>Statsitik Data SiMASTER</span></h4>
                            	<ul class="stats-container">
                                    <li>
                                        <a href="#" class="stat summary" style="width: 100%; " >
                                            <span class="icon icon-circle bg-blue">
                                             <i class="icon-tags"></i>
											 </span>
											  <span class="digit">
                                                <span class="text"><b>TICKET PERBAIKAN</b></span>
                                                350
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat summary" style="width: 100%; " >
                                            <span class="icon icon-circle bg-red">
                                             <i class="icon-archive"></i>
											  </span>
											  <span class="digit">
                                                <span class="text"><b>ASSET PERUSAHAAN</b></span>
                                                250
                                            </span>
                                        </a>
                                    </li>
                                      <li>
                                        <a href="#" class="stat summary" style="width: 100%; " >
                                            <span class="icon icon-circle bg-green" >
                                                <i class="icon-users"></i>
                                            </span>
                                            <span class="digit">
                                                <span class="text"><b>USER/PENGGUNA</b></span>
                                                150
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat summary" style="width: 100%; " >
                                            <span class="icon icon-circle bg-orange" >
                                                <i class="icon-list-2"></i>
                                            </span>
                                            <span class="digit">
                                                <span class="text"><b>JOBS PENDING</b></span>
                                                41
                                            </span>
                                        </a>
                                    </li>
                                </ul> 
							 </div>

							  <div class="divider"></div>
                          <div class="analytics-tab-content">
                            <h4 class="sub"><span>Statsitik Data Aset Rusak</span></h4>
                                <ul class="stats-container">
                                    <li>
                                        <a href="#" class="stat circular">
                                            <span rel="tooltip" title="Laptop yang rusak" 
                                                data-provide="circular" 
                                                data-fill-color="#08c" 
                                                data-value="1" 
                                                data-max-value="10" 
                                                data-radius="64" 
                                                data-thickness="25" 
                                                data-percent="false"
                                            ></span>
                                            <span class="text">LAPTOP</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat circular">
                                            <span rel="tooltip" title="Komputer/PC yang rusak" 
                                                data-provide="circular" 
                                                data-fill-color="#769f5d"  
                                                data-value="1" 
                                                data-max-value="159" 
                                                data-radius="64" 
                                                data-thickness="25" 
                                                data-percent="false"
                                            ></span>
                                            <span class="text">KOMPUTER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat circular">
                                            <span rel="tooltip" title="Monitor yang rusak"                                                
												data-provide="circular" 
                                                data-fill-color="#e95d3c" 
                                                data-value="10" 
                                                data-max-value="180" 
                                                data-radius="64" 
                                                data-thickness="25" 
                                                data-percent="false"
                                            ></span>
                                            <span class="text" >MONITOR </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat circular">
                                            <span rel="tooltip" title="Printer yang rusak"                                                 
												data-provide="circular" 
                                                data-fill-color="#ffa200" 
                                                data-value="2" 
                                                data-max-value="40" 
                                                data-radius="64" 
                                                data-thickness="25" 
                                                data-percent="false"
                                            ></span>
                                            <span class="text">PRINTER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="stat circular">
                                            <span rel="tooltip" title="78% Disk Space Usage"                                                
												data-provide="circular" 
                                                data-fill-color="#666" 
                                                data-value="2" 
                                                data-max-value="60" 
                                                data-radius="64" 
                                                data-thickness="25" 
                                                data-percent="false"
                                            ></span>
                                            <span class="text">DEVICE SUPPORT</span>
                                        </a>
                                    </li>
                                </ul>
                          </div>	
					
                                
                                <div class="row-fluid">
                                    <div class="span6 widget">
                                        <div class="widget-header">
                                            <span class="title"><i class="icos-heart-favorite"></i> Statistik Anda Saat Ini</span>
                                        </div>
                                        <div class="widget-content summary-list">
                                            <ul>
                                                <li>
                                                    <span class="key"><i class="icon-support"></i> Support Tickets</span>
                                                    <span class="val">
                                                        <span class="text-nowrap">332</span>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="key"><i class="icon-tools"></i> Respon Service</span>
                                                    <span class="val">
                                                        <span class="text-nowrap">71% <i class="up icon-arrow-up"></i></span>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="key"><i class="icos-shopping-cart-2"></i> Waiting Respon</span>
                                                    <span class="val">
                                                        <span class="text-nowrap">29% <i class="down icon-arrow-down"></i></span>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="key"><i class="icon-windows"></i> Hak Ases Login</span>
                                                    <span class="val">
                                                        <span class="text-nowrap"><?php echo $this->session->userdata('role'); ?></span>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="key"><i class="icos-money"></i> Jabatan</span>
                                                    <span class="val">
                                                        <span class="text-nowrap"><?php echo $this->session->userdata('username'); ?></span>
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="key"><i class="icon-key"></i> Last Sign In</span>
                                                    <span class="val">
                                                        <span class="text-nowrap"><?php echo $this->session->userdata('last_login'); ?></span>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                	<div class="span6 widget">
                                        <div class="widget-header">
                                            <span class="title">
                                                <i class="icon-ok"></i> Daftar Pekerjaan Perbaikan
                                            </span>
                                            <div class="toolbar">
                                                <span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                            </div>
                                        </div>
                                        <div class="widget-content task-list">
                                            <ul>
                                                <li class="done">
                                                    <label class="checkbox"><input type="checkbox" class="uniform" checked></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 1
															<span class="label label-success">LOW</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>
                                                </li>
                                                <li>
                                                    <label class="checkbox"><input type="checkbox" class="uniform"></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 2
                                                            <span class="label label-warning">NORMAL</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>
                                                </li>
                                                <li class="done">
                                                    <label class="checkbox"><input type="checkbox" class="uniform" checked></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                           Kerusakan pada aset 3
                                                            <span class="label label-success">LOW</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>
                                                </li>
                                                <li>
                                                    <label class="checkbox"><input type="checkbox" class="uniform"></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 4
															 <span class="label label-success">LOW</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>
                                                </li>
                                                <li>
                                                    <label class="checkbox"><input type="checkbox" class="uniform" checked></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 5
															<span class="label label-important">HIGH</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>
                                                </li>
                                                <li>
													<label class="checkbox"><input type="checkbox" class="uniform" checked></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 6
															<span class="label label-warning">NORMAL</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>                                                    
                                                </li>
												<li>
													<label class="checkbox"><input type="checkbox" class="uniform"></label>
                                                    <span class="text">
                                                        <span class="text-nowrap">
                                                            Kerusakan pada aset 7
															
															<span class="label label-warning">NORMAL</span>
                                                        </span>
                                                    </span>
                                                    <a href="#" class="close">&times;</a>                                                    
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>



								
							</div>
							
							
							
							
        </section>

</html>