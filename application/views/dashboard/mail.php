<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Dashboard
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Pesan Masuk</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Daftar Group <span> Disini anda bisa melakukan pengelolaan data group.</b> </span>
                                </h1>
                </div>

                            
                            <div id="main-content">
                                <div class="mail">
                                    <div id="compose-mail" class="modal mail-modal fade hide" data-backdrop="false">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal">&times;</button>
                                            Compose Mail
                                        </div>
                                        <div class="modal-body">
                                            <div class="row-fluid">
                                                <form class="form-vertical span12">
                                                    <div class="control-group">
                                                        <label class="control-label">To</label>
                                                        <div class="controls">
                                                            <input type="text" class="span12">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">CC</label>
                                                        <div class="controls">
                                                            <input type="text" class="span12">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label">Subject</label>
                                                        <div class="controls">
                                                            <input type="text" class="span12">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <textarea class="span12"></textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#" class="btn btn-primary">Send Email</a>
                                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                                        </div>
                                    </div>
                                    <div id="view-mail" class="modal mail-modal fade hide" data-backdrop="false">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal">&times;</button>
                                            Game Splash: Can you survive the strange new world of Botanica?
                                        </div>
                                        <div class="modal-body">
                                            <div class="view-mail-header clearfix">
                                                <div class="mail-address pull-left">
                                                    <span class="view-mail-label">From:</span> neque.pellentesque@commodoipsumSuspendisse.com<br>
                                                    <span class="view-mail-label">To:</span> magna@commodoipsumSuspendisse.com<br>
                                                </div>
                                                <div class="btn-toolbar pull-right">
                                                    <a href="#" class="btn btn-mini"><i class="icon-reply"></i></a>
                                                    <a href="#" class="btn btn-mini"><i class="icon-arrow-right"></i></a>
                                                    <a href="#" class="btn btn-mini"><i class="icon-trash"></i></a>
                                                </div>
                                            </div>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat lorem vel felis placerat rutrum. Morbi aliquet, est ut porttitor venenatis, mauris velit mollis arcu, vel lobortis risus ipsum non augue. Maecenas quis bibendum lectus. Etiam auctor fermentum enim, quis dignissim mauris aliquam at. Maecenas volutpat elit et eros varius vulputate. Duis tristique tristique ipsum ac vestibulum. In id orci magna. Morbi gravida convallis quam, ut feugiat velit fringilla sed. Suspendisse potenti. Nulla metus odio, mattis non mollis non, rhoncus bibendum ante. In quis leo ut ante placerat convallis ut ac massa. Aliquam eu nisi vel ipsum aliquet aliquam. Donec sed libero vitae nisl egestas elementum. Donec id neque at sem suscipit vulputate nec vel velit. Sed risus odio, egestas nec mollis ac, ornare ac metus. Morbi sollicitudin purus a enim aliquam semper.</p>
                                            <p>Praesent ut sem ut diam auctor mollis vel at eros. Pellentesque lobortis auctor metus, non faucibus sem viverra sed. Morbi tristique porttitor urna ut fermentum. Fusce blandit ipsum et sapien laoreet suscipit. Ut dignissim, velit tempor accumsan feugiat, turpis neque ultricies eros, quis luctus nulla mi vitae tellus. Ut venenatis lectus sed ipsum accumsan tristique. Maecenas mattis sollicitudin est, ac laoreet ligula aliquet quis. Cras quis risus neque, quis auctor enim. Curabitur diam felis, viverra vitae blandit a, porttitor id quam. Curabitur sed felis eu tortor adipiscing facilisis.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#" class="btn" data-dismiss="modal">Close</a>
                                        </div>
                                    </div>
                                    <div class="navbar">
                                        <div class="navbar-inner">
                                            <button type="button" class="btn btn-primary btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <div class="btn-toolbar pull-left">
                                                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#compose-mail">Compose</a>
                                                <span class="btn-group">
                                                    <a class="btn" data-toggle="modal" href="#" data-target="#view-mail"><i class="icol-find"></i></a>
                                                    <a class="btn"><i class="icol-bin-closed"></i></a>
                                                    <a class="btn"><i class="icol-delete"></i></a>
                                                    <a class="btn"><i class="icol-arrow-out"></i></a>
                                                </span>
                                            </div>
                                            <div class="nav-collapse pull-right">
                                                <ul class="nav">
                                                    <li class="active"><a data-toggle="tab" data-target="#inbox" href="#"><i class="icol-inbox"></i> Inbox</a></li>
                                                    <li><a data-toggle="tab" data-target="#drafts" href="#"><i class="icol-page-white-edit"></i> Drafts</a></li>
                                                    <li><a data-toggle="tab" data-target="#sent" href="#"><i class="icol-page-white-get"></i> Sent</a></li>
                                                    <li><a data-toggle="tab" data-target="#spam" href="#"><i class="icol-page-red"></i> Spam</a></li>
                                                    <li><a data-toggle="tab" data-target="#trash" href="#"><i class="icol-bin-closed"></i> Trash</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mail-pages tab-content">
                                        <div id="inbox" class="tab-pane active">
                                            <table class="table table-striped table-checkable">
                                                <thead>
                                                    <tr>
                                                        <th class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </th>
                                                        <th>From</th>
                                                        <th>Subject</th>
                                                        <th>Date</th>
                                                        <th>Size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>neque.pellentesque@commodoipsumSuspendisse.com</td>
                                                        <td><a href="#">dictum.</a></td>
                                                        <td>Sep 21, 2012</td>
                                                        <td>177</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>felis@volutpatornare.ca</td>
                                                        <td><a href="#">mauris</a></td>
                                                        <td>Sep 5, 2012</td>
                                                        <td>1225</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>magna@rutrum.com</td>
                                                        <td><a href="#">tincidunt aliquam</a></td>
                                                        <td>Sep 10, 2012</td>
                                                        <td>1297</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>Integer.vitae@utaliquam.edu</td>
                                                        <td><a href="#">vitae odio</a></td>
                                                        <td>Sep 16, 2012</td>
                                                        <td>970</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>Donec@disparturient.edu</td>
                                                        <td><a href="#">magna. Nam ligula elit,</a></td>
                                                        <td>Sep 11, 2012</td>
                                                        <td>398</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="drafts" class="tab-pane">
                                            <table class="table table-striped table-checkable">
                                                <thead>
                                                    <tr>
                                                        <th class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </th>
                                                        <th>From</th>
                                                        <th>Subject</th>
                                                        <th>Date</th>
                                                        <th>Size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>neque.pellentesque@commodoipsumSuspendisse.com</td>
                                                        <td><a href="#">dictum.</a></td>
                                                        <td>Sep 21, 2012</td>
                                                        <td>177</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="sent" class="tab-pane">
                                            <table class="table table-striped table-checkable">
                                                <thead>
                                                    <tr>
                                                        <th class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </th>
                                                        <th>From</th>
                                                        <th>Subject</th>
                                                        <th>Date</th>
                                                        <th>Size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>neque.pellentesque@commodoipsumSuspendisse.com</td>
                                                        <td><a href="#">dictum.</a></td>
                                                        <td>Sep 21, 2012</td>
                                                        <td>177</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>felis@volutpatornare.ca</td>
                                                        <td><a href="#">mauris</a></td>
                                                        <td>Sep 5, 2012</td>
                                                        <td>1225</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="spam" class="tab-pane">
                                            <table class="table table-striped table-checkable">
                                                <thead>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>felis@volutpatornare.ca</td>
                                                        <td><a href="#">mauris</a></td>
                                                        <td>Sep 5, 2012</td>
                                                        <td>1225</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>magna@rutrum.com</td>
                                                        <td><a href="#">tincidunt aliquam</a></td>
                                                        <td>Sep 10, 2012</td>
                                                        <td>1297</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>Integer.vitae@utaliquam.edu</td>
                                                        <td><a href="#">vitae odio</a></td>
                                                        <td>Sep 16, 2012</td>
                                                        <td>970</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>Donec@disparturient.edu</td>
                                                        <td><a href="#">magna. Nam ligula elit,</a></td>
                                                        <td>Sep 11, 2012</td>
                                                        <td>398</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="trash" class="tab-pane">
                                            <table class="table table-striped table-checkable">
                                                <thead>
                                                    <tr>
                                                        <th class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </th>
                                                        <th>From</th>
                                                        <th>Subject</th>
                                                        <th>Date</th>
                                                        <th>Size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>neque.pellentesque@commodoipsumSuspendisse.com</td>
                                                        <td><a href="#">dictum.</a></td>
                                                        <td>Sep 21, 2012</td>
                                                        <td>177</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>felis@volutpatornare.ca</td>
                                                        <td><a href="#">mauris</a></td>
                                                        <td>Sep 5, 2012</td>
                                                        <td>1225</td>
                                                    </tr>

                                                    <tr>
                                                        <td class="checkbox-column">
                                                            <input type="checkbox" class="uniform">
                                                        </td>
                                                        <td>Suspendisse.dui.Fusce@magnaSuspendisse.edu</td>
                                                        <td><a href="#">auctor</a></td>
                                                        <td>Sep 7, 2012</td>
                                                        <td>392</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
	</section> 
</html>

