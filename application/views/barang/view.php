<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Barang</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data Barang <span> Disini anda bisa melakukan pengelolaan data barang.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Barang
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('barang/add'); ?>"  class="btn btn-primary icon-gift"> Tambah Barang </a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>

							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">Kode Barang</th>
														<th>Kategori</th>
														<th>Merek</th>
														<th>Nama</th>
														<th>Spesifikasi</th>
														<th>Satuan</th>                               
														<th>Edit</th>   
														<th>Delete</th>                                 
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   foreach ($record as $r){
												  
												   echo"
													   <tr>
													   <td>$no</td>
													   <td>".$r->kode_barang."</td>
											   			<td>".$r->nama_kategori."</td>
														<td>".$r->nama_barang."</td>
														<td>".$r->merek_barang."</td>
														<td>".$r->spesifikasi."</td>	
														<td>".$r->satuan."</td>														
													   <td>" . anchor('barang/edit/' . $r->kode_barang, '<i class="btn btn-info btn-sm icon-edit" data-toggle="tooltip" title="Edit"></i>') . "</td>
													   <td>" . anchor('barang/delete/' . $r->kode_barang, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>

