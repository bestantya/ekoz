<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Master
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Barang</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Tambah Barang <span> Disini anda bisa melakukan pengelolaan data master barang.</b> </span>
                                </h1>
                </div>            
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Tambah Data Barang</span>
                                     </div>
										<?php echo form_open('barang/add'); ?>
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" method="post">

													<div class="control-group">
													<label class="control-label">Nama Barang <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama" 
																  class="form-control" required oninvalid="setCustomValidity('Nama Barang Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Nama Barang" >
																</div>
															  <?php echo form_error('nama', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Kategori <span class="required">*</span></label>
														<div class="controls">
														   <select id="input17"name="kategori" class="select2-select-00 span6">                    
															<?php
															if (!empty($katBarang)) {
																foreach ($katBarang as $row) {
																	echo "<option value=".$row->id_kategori.">".$row->nama_kategori."</option>";                                        
																}
															}
															?>
															  </select>                           
															
														</div><?php echo form_error('kategori', '<div class="text-red">', '</div>'); ?>	
														<a href="<?php echo site_url('barang/addkategori'); ?>" class="btn btn-primary"> 
														<i class="icon-plus" aria-hidden="true" ></i> Add Kategori </a>
														<?php echo form_error('kategori', '<div class="text-red">', '</div>'); ?>
													</div>

													<div class="control-group">
													<label class="control-label">Merek <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="merek" 
																  class="form-control" required oninvalid="setCustomValidity('Merek Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Merek Barang" >
															  <?php echo form_error('merek', '<div class="text-blue">', '</div>'); ?>
															  </div>
														</div>
													</div>
													
													<div class="control-group">
													<label class="control-label">Spesifikasi <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <textarea rows="3"  class="span6" name="spek" 
																  class="form-control" required oninvalid="setCustomValidity('Spesifikasi Barang Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Masukan Spesifikasi Barang" ></textarea>
																</div>
															  <?php echo form_error('spek', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>	
													
													<div class="control-group">
															<label class="control-label">Satuan <span class="required">*</span></label>
															<div class="controls">
														 <select name="satuan"  class="span6 select2-select-00" id="satuan" > 
															<option value='PCS'>PCS</option>
															<option value='UNIT'>UNIT</option>
															<option value='PACK'>PACK</option>
															<option value='BUAH'>BUAH</option>
															<option value='METER'>METER</option>
															<option value='ROLL'>ROLL</option>
														  </select>                        
														</div> 
														<?php echo form_error('satuan', '<div class="text-red">', '</div>'); ?>                      
													</div>
														
												<div class="form-actions">
													<button type="submit" name="submit" class="btn btn-success pull-left"> <i class="icon-hdd" aria-hidden="true" ></i> Simpan </button>
													 <a href="<?php echo site_url('barang'); ?>" class="btn btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
													 <button type="reset" name="reset" class="btn pull-right"> <i class="icon-refresh" aria-hidden="true" ></i> Reset</button>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
								
		</section>
</html>




                   

