<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-archive"></i>Inventory
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Laptop</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Data Laptop <span> Disini anda bisa melakukan pengelolaan data laptop.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
                  <div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Laptop
                                    </span>
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('laptop/add'); ?>"  class="btn btn-primary icon-plus"> Tambah Aset Laptop</a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
														<label calss='control-label' ><?php echo $this->session->flashdata('result_hapus'); ?></label>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
                                               <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6" name="q" value="">
															<span class="input-group-btn">
															</span>
															<button class="btn btn-primary" type="submit">Search</button>
														</div>
														
													</form>
                                            </div>
                                        </div>

							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
													<tr>
														<th>No.</th>
														<th width="10%">No Inventaris</th>
														<th width="11%">Nama Pengguna</th>
														<th>Departemen</th>
														<th>Sub.Dept</th>                                                               
														<th width="10%">Nama Laptop</th>
														<th>Spesifikasi</th>
														<th width="10%">IP Address</th>                                 
														<th>Status</th>                           
														<th>Detail</th>  
														<th>Delete</th>                                 
													</tr>
                                                    </thead>
                                             <?php
											   $no=1;
											   function chek($id) {
													$CI = get_instance();
													$result = $CI->db->get_where('tb_departemen', array('id_dept' => $id))->row_array();
													return $result['nama'];
												}
											   foreach ($record as $r){    
												$deptnama= $r->parent == 0 ? 'DEPARTEMEN' : chek($r->parent);
												  
												   echo"
													   <tr>
													   <td>$no</td>
														<td>".$r->kode_laptop."</td>
														<td>".$r->nama_pengguna."</td>
											   			<td>".$deptnama."</td>	
														<td>".$r->nama."</td>
														<td>".$r->nama_laptop."</td>
														<td>".$r->spesifikasi."</td>	
														<td>".$r->network."</td>
														<td>".$r->status."</td>														
													   <td>" . anchor('laptop/detail/' . $r->kode_laptop, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="Detail"></i>') . "</td>
													   <td>" . anchor('laptop/delete/' . $r->kode_laptop, '<i class="btn btn-sm btn-danger icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Anda yakin ingin menghapus data ini?')")) . "</td>
													   </tr>";
												   $no++;
											   }
											   ?>
											   
                                    </table>
                            </div>
						</div>			
			</div>
	</section>
		 
</html>
