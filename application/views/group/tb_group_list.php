<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Group Setting</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Daftar Group <span> Disini anda bisa melakukan pengelolaan data group.</b> </span>
                                </h1>
                </div>

                     
			<div id="main-content">
				<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
						<div class="widget">
                                 <div class="widget-header">
                                    <span class="title">
                                        <i class="icol-table"></i> List Data Group
                                    </span>
									
                                    <div class="toolbar">
										<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('group/create'); ?>" class="btn btn-primary icon-plus"> Tambah Group </a></span>
													<span class="btn" rel="tooltip" ><i class="icon-refresh"></i></span>
                                                    <span class="btn dropdown-toggle" data-toggle="collapse" data-target="#toolbar-ex">
                                                        <i class="icon-search"></i>
                                                    </span>
                                        </div>
                                    </div>
								</div>
                                            <div id="toolbar-ex" class="toolbar form-toolbar collapse in">
												    <form action="<?php echo site_url('group/index'); ?>" class="form-inline" method="get">
														<div class="input-group">
															<input type="text" placeholder="Ketik data yang anda cari disini,.." class="form-control span6 " name="q" value="<?php echo $q; ?>">
															<span class="input-group-btn">
																<?php 
																	if ($q <> '')
																	{
																		?>
																		<a href="<?php echo site_url('group'); ?>" class="btn btn-default">Reset</a>
																		<?php
																	}
																?>
															  <button class="btn btn-primary" type="submit">Search</button>
															</span>
														</div>
													</form>
                                            </div>
										
										  
 
							<div class="widget-content table-container">
                                    <table id="demo-dtable-03" class="table table-striped">
                                                    <thead>
                                                    <tr >
													<th>No</th>
													<th>Nama Perusahaan</th>
													<th>Nama Group</th>
													<th>Nama Alias</th>
													<th>Alamat</th>
													<th>Logo</th>
													<th>Logo Dashboard</th>
													<th style="text-align:center" >Action</th>
													</tr>
                                                    </thead>
                                           <?php
											foreach ($group_data as $group)
											{
												?>
												<tr >
											<td><?php echo ++$start ?></td>
											<td><?php echo $group->nama_pt ?></td>
											<td><?php echo $group->nama_group ?></td>
											<td><?php echo $group->nama_alias ?></td>
											<td><?php echo $group->alamat ?></td>
											<td><?php echo $group->logo ?></td>
											<td><?php echo $group->logo_dashboard ?></td>
											<td style="text-align:center" width="200px">
												<?php 
												echo anchor(site_url('group/read/'.$group->gid),'Read', 'class="btn btn-success"'); 
												echo ' | '; 
												echo anchor(site_url('group/update/'.$group->gid),'Update','class="btn btn-warning"'); 
												echo ' | '; 
												echo anchor(site_url('group/delete/'.$group->gid),'Delete','class="btn btn-danger"','onclick="javasciprt: return confirm(\'Anda yakin ingin menghapus data ini?'); 
												?>
											</td>
										</tr>
												<?php
											}
											?>
                                    </table>
                            </div>
						</div>		
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div><?php echo $pagination ?>
		</div>
	</section> 
</html>



                
