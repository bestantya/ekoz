<div id="sidebar-separator"></div>
          <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Edit Group</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Edit Group <span> Disini anda bisa melakukan pengelolaan data group.</b> </span>
                                </h1>
                            </div>              
 
 
				<div id="main-content">
					<div class="alert fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong>Peringatan :</strong><br>
                          <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                           <div class="widget">
                                 <div class="widget-header">
                                    <span class="title">Edit Data Group</span>
                                     </div>
										
										<div class="widget-content form-container">
											<form id="validate-4" class="form-horizontal" action="<?php echo $action; ?>" method="post">
											
													<div class="control-group">
													<label class="control-label">Nama Perusahaan <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama_pt" id="nama_pt"
																  class="form-control" required oninvalid="setCustomValidity('Nama Perusahaan Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Nama Perusahaan" value="<?php echo $nama_pt; ?>" />
																</div>
															  <?php echo form_error('nama_pt', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Nama Group <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama_group" id="nama_group"
																  class="form-control" required oninvalid="setCustomValidity('Nama Group Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Nama Group" value="<?php echo $nama_group; ?>" />
																</div>
															  <?php echo form_error('nama_group', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
													
													
													<div class="control-group">
													<label class="control-label">Nama Alias <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="nama_alias" id="nama_alias"
																  class="form-control" required oninvalid="setCustomValidity('Nama Alias Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Nama Alias" value="<?php echo $nama_alias; ?>" />
																</div>
															  <?php echo form_error('nama_alias', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>													
										
													<div class="control-group">
													<label class="control-label">Alamat <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="alamat" id="alamat"
																  class="form-control" required oninvalid="setCustomValidity('Alamat Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Alamat" value="<?php echo $alamat; ?>" />
																</div>
															  <?php echo form_error('alamat', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Logo <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="logo" id="logo"
																  class="form-control" required oninvalid="setCustomValidity('Logo Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Logo" value="<?php echo $logo; ?>" />
																</div>
															  <?php echo form_error('logo', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>

													<div class="control-group">
													<label class="control-label">Logo Dashboard <span class="required">*</span></label>
														<div class="controls">
																<div >
																  <input type="text" class="span6"  name="logo_dashboard" id="logo_dashboard" 
																  class="form-control" required oninvalid="setCustomValidity('Logo Dashboard Anda Masih Kosong!')" 
																  oninput="setCustomValidity('')" placeholder="Logo Dashboard" value="<?php echo $logo_dashboard; ?>" />
																</div>
															  <?php echo form_error('logo_dashboard', '<div class="text-blue">', '</div>'); ?>
														</div>
													</div>
    												<input type="hidden" name="gid" value="<?php echo $gid; ?>" /> 	
												<div class="form-actions">
													<button type="submit"  class="btn btn-primary pull-left"> <?php echo $button ?></button>
                                                    <a href="<?php echo site_url('group'); ?>" class="btn  btn-warning pull-right"> <i class="icon-remove" aria-hidden="true" ></i> Cancel </a>
												</div>
											
											</form>
										</div>		
									</div>
                                </div>
		</section>
</html>


       
