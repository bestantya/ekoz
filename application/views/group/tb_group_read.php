<div id="sidebar-separator"></div>
                        
     <section id="main" class="clearfix">
				<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-cogs"></i>Setting
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="#">Group Setting</a>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Detail Group <span> Disini anda bisa melakukan pengelolaan data group.</b> </span>
                                </h1>
                </div>

                     
			<div class="span10 widget">
                                 <div class="widget-header">
                                    	<span class="title">
                                        <i class="icol-table"></i> Detail Data Departemen
                                    </span>
											
											<div class="toolbar">
												<div class="btn-group">
													<span class="btn"> <a href="<?php echo base_url('group'); ?>"  class="btn btn-warning icon-caret-left"> Kembali</a></span>
													<span class="btn dropdown-toggle" data-toggle="dropdown">
														<i class="icon-pencil"></i> Action <i class="caret"></i>
													</span>
													<ul class="dropdown-menu pull-right">
														<li><a href="<?php echo base_url('group'); ?>"><i class="icol-magnifier"></i> View Data All</a></li>
														<li><a href="#"><i class="icol-pencil"></i> Edit Data</a></li>
														<li><a href="#"><i class="icol-cross"></i> Delete Data</a></li>
													</ul>
												</div>
											</div>
                                        </div>
                                        <div class="widget-content table-container">
                                        	<table class="table table-striped table-detail-view">
                                            	<thead>
                                                	<tr>
                                                    	<th colspan="2"><i class="icol-chart-organisation"></i> Group Information</th>
                                                    </tr>
                                                </thead>
                                            	<tbody>
												<tr>
                                                    <th>Nama Perusahaan</th>
                                                        <td><?php echo $nama_pt; ?></td>
                                                    </tr>
                                                	<tr>
                                                    	<th>Nama Group</th>
                                                        <td><?php echo $nama_group; ?></td>
                                                    </tr>
                                                	<tr>
                                                    	<th>Nama Alias</th>
                                                        <td><?php echo $nama_alias; ?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Alamat</th>
                                                        <td><?php echo $alamat; ?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Logo</th>
                                                        <td><?php echo $logo; ?></td>
                                                    </tr>
                                                    <tr>
                                                    	<th>Logo Dashboardy</th>
                                                        <td><?php echo $logo_dashboard; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
   						</div>			
			</div>
	</section>
		 
</html>


   