<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>.:SiMASTER - Sistem Informasi Management Aset IT & Support Center:.</title>
<link href="<?php echo base_url('assets/images/icon.png'); ?>" rel='shortcut icon' type='image/x-icon'/>
<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url('assets/jui/css/jquery-ui.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/jquery-ui.custom.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.css'); ?>" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('plugins/uniform/css/uniform.default.css'); ?>" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="<?php echo base_url('plugins/ibutton/jquery.ibutton.css'); ?>" media="screen">

<!-- Circular Stat -->
<link rel="stylesheet" href="<?php echo base_url('custom-plugins/circular-stat/circular-stat.css'); ?>"media="screen">

<!-- Fullcalendar -->
<link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('plugins/fullcalendar/fullcalendar.print.css'); ?>" media="print">

<!-- Colorpicker -->
<link rel="stylesheet" href="<?php echo base_url('plugins/minicolors/jquery.minicolors.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('plugins/farbtastic/farbtastic.css'); ?>" media="screen">

<!-- Zebra Datepicker -->
<link rel="stylesheet" href="<?php echo base_url('plugins/zebradp/css/mooncake/zebra_datepicker.css'); ?>" media="screen">
<!-- PrettyPhoto -->
<link rel="stylesheet" href="<?php echo base_url('plugins/prettyphoto/css/prettyPhoto.css'); ?>" media="screen">

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/icomoon/style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main-style.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('assets/css/invoice.print.css'); ?>" media="print">
<!-- Main Layout Stylesheet -->
<script src="<?php echo base_url("assets/js/libs/jquery-1.8.3.min.js"); ?>"></script>

<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/libs/jquery.placeholder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/libs/jquery.mousewheel.min.js'); ?>"></script>

<!-- Template Script -->
<script src="<?php echo base_url('assets/js/template.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/setup.js'); ?>"></script>

<!-- Customizer, remove if not needed -->
<script src="<?php echo base_url('assets/js/customizer.js'); ?>"></script>

<!-- Uniform Script -->
<script src="<?php echo base_url('plugins/uniform/jquery.uniform.min.js'); ?>"></script>

<!-- jquery-ui Scripts -->
<script src="<?php echo base_url('assets/jui/js/jquery-ui-1.9.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jui/jquery-ui.custom.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jui/timepicker/jquery-ui-timepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/jui/jquery.ui.touch-punch.min.js'); ?>"></script>

<script src="<?php echo base_url('plugins/flot/jquery.flot.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/flot/plugins/jquery.flot.tooltip.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/flot/plugins/jquery.flot.pie.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/flot/plugins/jquery.flot.orderBars.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/flot/plugins/jquery.flot.resize.min.js'); ?>"></script>

<!-- Circular Stat -->
<script src="<?php echo base_url('custom-plugins/circular-stat/circular-stat.min.js'); ?>"></script>

<!-- SparkLine -->
<script src="<?php echo base_url('plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>

<!-- iButton -->
<script src="<?php echo base_url('plugins/ibutton/jquery.ibutton.min.js'); ?>"></script>

<!-- Full Calendar -->
<script src="<?php echo base_url('plugins/fullcalendar/fullcalendar.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/fullcalendar/gcal.js'); ?>"></script>

<!-- DataTables -->
<script src="<?php echo base_url('plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/TableTools/js/TableTools.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/FixedColumns/FixedColumns.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/datatables/jquery.dataTables.columnFilter.js'); ?>"></script>

<!-- Demo Scripts -->
<script src="<?php echo base_url('assets/js/demo/dashboard.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/dataTables.js'); ?>"></script>

<!-- Core Scripts -->
<script src="<?php echo base_url("assets/js/libs/jquery-1.8.3.min.js"); ?>"></script>
<!-- Zebra Datepicker -->
<script src="<?php echo base_url('plugins/zebradp/zebra_datepicker.min.js'); ?>"></script>

<!-- Colorpicker -->
<script src="<?php echo base_url('plugins/minicolors/jquery.minicolors.min.js'); ?>"></script>
<!-- <script src="<?php echo base_url('plugins/farbtastic/farbtastic.min.js'); ?>"></script> -->

 <!-- PrettyPhoto -->
<script src="<?php echo base_url('plugins/prettyphoto/js/jquery.prettyPhoto.min.js'); ?>"></script>

<!-- Freetile -->
<script src="<?php echo base_url('plugins/freetile/jquery.freetile.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/freetile/jquery.resize.min.js'); ?>"></script>

<!-- Demo Scripts -->
<script src="<?php echo base_url('assets/js/demo/ui_comps.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/calendar.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/charts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/statistic.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/gallery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/demo/mail.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>


</head>


<body data-show-sidebar-toggle-button="true" data-fixed-sidebar="false">
<div id="customizer">
        <div id="showButton"><i class="icon-cogs"></i></div>
        <div id="layoutMode">
            <label class="checkbox"><input type="checkbox" class="uniform" name="layout-mode" value="boxed"> Boxed</label>
        </div>
    </div>

    <div id="wrapper">
        <?php echo $_header; ?>


        <div id="content-wrap">
			<div id="content">

            	<div id="content-outer">
					<?php echo $_sidebar; ?>

                	<div id="content-inner">


						<?php echo $_content; ?>
					</div>
				</div>
			</div>
		</div>

         <footer id="footer">
			<div class="footer-left">Copyright&nbsp;&copy; 2019. All Rights Reserved. EKO SETIAWAN - 1115R0532</div>
			<div class="footer-right"><p><b>SiMASTER</b> <i> Version 1.1 Build 010719</i></p></div>

		</footer>

    </div>
</body>
