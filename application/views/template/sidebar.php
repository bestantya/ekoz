<aside id="sidebar">
		<nav id="navigation" class="collapse">
			<ul>
                <?php
				 // MENU ADMINISTRATOR	
            if ($this->session->userdata('role')=='Administrator'){
				 // USER
                $muser = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'User'));
                foreach ($muser->result() as $m) {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active ";
                            }else{
                                $class="";
                            }
						
						
						echo '<li class="active"> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='active inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active ";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
			
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active ";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
						 
						 
						 
						 
						 
                    }                
                }
				 // STAFF
                echo ''; 
                    $mstaff= $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Staff'));
                    foreach ($mstaff->result() as $m)  {
						
						
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li class="active"> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		

                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
				 // MANAGER
                echo ''; 
                    $mmgr= $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Manager'));
                    foreach ($mmgr->result() as $m)  {
						
						
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li class="active"> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		

                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
				 // ADMINISTRATOR
                echo ''; 
                    $admin = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Administrator'));
                    foreach ($admin->result() as $m)  {
						
						
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active nav-title";
                            }else{
                                $class="";
                            }
									echo '<li class="active"> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';

                      
									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		

                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }

				// MENU STAFF
			}else if ($this->session->userdata('role')=='Staff') {         
				// USER
                $muser = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'User'));
                foreach ($muser->result() as $m) {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active ";
                            }else{
                                $class="";
                            }
						
						
						echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
			
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
	 
                    }                
                }
				
				// STAFF
					echo ''; 
               $mstaff = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Staff'));
                foreach ($mstaff->result() as $m)  {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
				// MANAGER
				echo ''; 
                    $mmgr= $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Manager'));
                    foreach ($mmgr->result() as $m)  {
						
						
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		

                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
				
				// MENU MANAGER
			} else if ($this->session->userdata('role')=='Manager') {  
				// USER
					$muser = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'User'));
					foreach ($muser->result() as $m) {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active ";
                            }else{
                                $class="";
                            }
						
						
						echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
			
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
						 
						 
						 
						 
						 
                    }                
                }
				// MANAGER
				echo ''; 
					$mmgr = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'Manager'));
					foreach ($mmgr->result() as $m)  {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
			
			}// MENU USER
				else if ($this->session->userdata('role')=='User') {  
					$muser = $this->db->get_where('tb_menu', array('parent' => 0,'role'=>'User'));
					foreach ($muser->result() as $m)  {
					
                    // chek ada submenu atau tidak
					
                    $sub = $this->db->get_where('tb_menu', array('parent' => $m->id_menu));
                    if ($sub->num_rows() > 0) {
						
                        // buat menu + sub menu
                        $uri=$this->uri->segment(1);
                        $idclass=$this->db->get_where('tb_menu',array('link'=>$uri))->row_array();
                            if ($m->id_menu==$idclass['parent']){
                                $class="active";
                            }else{
                                $class="";
                            }
									echo '<li> <span class='.$class.'' . anchor($m->link, '<i class=" ' . $m->icon . '"> </i>
                          <span class="nav-title">'. strtoupper ($m->nama_menu) ) .'</span></span>';
					

									
											echo "<ul class='inner-nav'>";
											foreach ($sub->result() as $s) {
												$uri=$this->uri->segment(1);
												if ($s->link==$uri){
													$class1="active";
												}else{
													$class1="";
												}
												echo '<li class='.$class1.'> ' . anchor($s->link, ' 
												<i class="' . $s->icon . '" ></i>' . $s->nama_menu) . '</li></li>';
											}
											echo "</ul>";		
								echo '</li>';		
                    } else {
						
                        // single menu
                        $uri=$this->uri->segment(1);
                        if ($m->link==$uri){
                            $class2="active";
                        }else{
                            $class2="";
                        }
                        echo '<li class='.$class2.'>' . anchor($m->link, '<i class="' . $m->icon . '"></i>  
						 <span class="nav-title">' . strtoupper($m->nama_menu). '</span>') . '</li>';
                    }                
                }
			}
            ?>
			
		</ul>
     </nav>									
</aside>

