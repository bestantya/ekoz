 <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 125px; height: 20px;"></b>
							</a>
							   <h4 class="title"><?php echo tanggal_new() ;?></h4>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						<div id="header-search">
							<span id="search-toggle" data-toggle="dropdown">
								<i class="icon-search"></i>
							</span>
							<form class="navbar-search">
								<input type="text" class="input-group" placeholder="Pencarian . .">
							</form>
						</div>
						<div id="dropdown-lists">
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-exclamation-sign"></i></span>
    								<span class="item-label">Notifikasi</span>
    								<span class="item-count">2</span>
    							</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url('maintenance'); ?>">
                                                    <span class="thumbnail"><img src="<?php echo base_url('assets/images/pp.png'); ?>" alt=""></span>
                                                    <span class="details">
                                                        <strong>Mala</strong> Komputer saya restart sendiri pak,.minta tolong di cek ya,.
                                                        <span class="time">3 Menit yang lalu</span>
                                                    </span>
													
                                                </a>
                                            </li>
											<li>
                                                <a href="<?php echo base_url('maintenance'); ?>">
                                                    <span class="thumbnail"><img src="<?php echo base_url('assets/images/pp.png'); ?>" alt=""></span>
                                                    <span class="details">
                                                        <strong>Doni</strong> Email tidak bisa masuk pak dari tadi pagi minta tolong cek ya,.
                                                        <span class="time">13 Menit yang lalu</span>
                                                    </span>
													
                                                </a>
                                            </li>
                                           </ul>
                                    </li>
                                    <li><a href="<?php echo base_url('maintenance'); ?>">Lihat Semua Notifikasi</a></li>
                                </ul>
                            </div>
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-envelope"></i></span>
    								<span class="item-label">Pesan Masuk</span>
    								<span class="item-count">1</span>
    							</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url('dashboard/mail'); ?>">
                                                    <span class="thumbnail"><img src="<?php echo base_url('assets/images/pp.png'); ?>" alt=""></span>
                                                    <span class="details">
                                                        <strong>Dani</strong><br> Pak Saya minta layar yang lebih besar ya,.
                                                        <span class="time">10 Menit yang lalu</span>
                                                    </span>
                                                </a>
                                            </li>
                                           
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo base_url('dashboard/mail'); ?>">Lihat Semua Pesan</a></li>
                                </ul>
                            </div>
							
						</div>
                        
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                <span class="info">
                                	Selamat Datang,. <span style="color:red" ><b> <?php echo $this->session->userdata('nama'); ?> </b> </span> |
                                   Level Anda : <div class="name"> <?php echo $this->session->userdata('role'); ?></div></p>
								 
                                </span>
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="<?php echo base_url('assets/images/user/admin.png'); ?>" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	<li><a href="<?php echo site_url('dashboard/profile'); ?>"><i class="icol-user"></i> My Profile</a></li>
                                    	<li><a href="<?php echo site_url('maintenance'); ?>"><i class="icol-layout"></i> My Ticket</a></li>                                        
                                        <li class="divider"></li>
                                        <li><a href="<?php echo site_url('login/logout'); ?>"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="<?php echo site_url('login/logout'); ?>"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>