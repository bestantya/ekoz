<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('m_barang');
        chek_session();
    }
	public function index() {
        $data['record'] = $this->m_barang->list_barang()->result(); 
        $this->template->display('barang/view',$data);       
    }		
   
    public function view_data() {
		$ambildata = $this->m_barang->listBarang();  
        $no=1;
        foreach($ambildata as $r) { 
            $row[] = array(
                'no'=>$no++,
				'kode_barang' => $data['kode_barang'],
                'nama_kategori' =>$data['nama_kategori'],
				'nama_barang' => $data['nama_barang'],
                'merek_barang' =>$data['merek_barang'],
				'spesifikasi' => $data['spesifikasi'],
                'satuan' => '<center>' . $data['satuan'] . '</center>',
                'edit' => '<center><a href="' . base_url() . 'barang/edit/' . $data['id'] .'"><i class="icon-edit"></i></a></center>',
                'delete' => '<center><a href="' . base_url() . 'barang/hapus/' . $data['id'] .'" class="hapus" ><i class="icon-trash"></i></a></center>'
            );
        }        
        $result = array('data'=>$row);
        echo json_encode($result);
    }

    function add() {              
        $this->_set_rules();
        if ($this->form_validation->run() == true) {
            $data = array(
                'kode_barang' => $this->m_barang->kdotomatis(),
                'id_kategori' => $this->input->post('kategori'),
                'nama_barang' => $this->input->post('nama'),
                'merek_barang' => $this->input->post('merek'),
                'spesifikasi' => $this->input->post('spek'),
                'satuan' => $this->input->post('satuan'),
                'gid' => $this->session->userdata('gid')
            );
            $this->m_barang->simpan($data);
            redirect('barang');
        } else {  
            $data['katBarang'] = $this->m_barang->getKategori()->result();           
            $this->template->display('barang/tambah', $data);
        }
    }
	
    function addkategori() { 
        $this->form_validation->set_message('is_unique', '%s Sudah Ada');
        $this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required|is_unique[tb_kategori.nama_kategori]');
        if ($this->form_validation->run() == true) {
            $data = array(
                'nama_kategori' => $this->input->post('nama_kategori')               
            );
            $this->m_barang->simpankat($data);
            redirect('barang/add');
        } else {       
            $this->template->display('barang/kategori');
        }
    }
    function edit() {       
        if (isset($_POST['submit'])) {
            $this->_set_rules();
            if ($this->form_validation->run() == true) {
                $data = array(                            
                        'id_kategori' => $this->input->post('kategori'),
                        'nama_barang' => $this->input->post('nama'),
                        'merek_barang' => $this->input->post('merek'),
                        'spesifikasi' => $this->input->post('spek'),
                        'satuan' => $this->input->post('satuan'));
                $kode=$this->input->post('kode');
                $this->m_barang->edit($kode,$data);
                redirect('barang');                
            }else {
                $id = $this->input->post('kode');
                $data['katbarang'] = $this->m_barang->getKategori()->result();                            
                $data['record'] = $this->m_barang->getkode($id)->row_array();
                $this->template->display('barang/edit', $data); 
            } 
           }else{ 
                $id = $this->uri->segment(3);               
                $data['katbarang'] = $this->m_barang->getKategori()->result();                            
                $data['record'] = $this->m_barang->getkode($id)->row_array();
                $this->template->display('barang/edit', $data);
            }
    }
    function delete($kode_barang) {
        $this->m_barang->hapus($kode_barang);
		redirect('barang');
    }

    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama barang', 'required');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');
        $this->form_validation->set_rules('merek', 'Merek', 'required');
        $this->form_validation->set_rules('spek', 'Spesifikasi', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');        
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger alert-dismissable'>", "</div>");
    }

}
