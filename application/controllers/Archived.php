<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Archived extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('m_komputer','m_laptop','m_monitor','m_printer','m_network', 'm_querybuiler'));
        chek_session();
    }

    function index() {
	     $data['record'] = $this->m_laptop->semuagid_arsip()->result();
       $this->template->display('archived/inventory',$data);

    }

    function show_laptop(){
            if ($this->session->userdata('role')=='Administrator'){
                $gid="";
            }else{
                $gid=  $this->session->userdata('gid');
            }

            $table = 'v_laptop';
            $x = 1;
            $column_order = array(null, 'kode_laptop','nama_pengguna','nama_parent_dept','nama','nama_laptop', 'spesifikasi','status', null, null);
            $column_search = array('kode_laptop','nama_pengguna','nama_parent_dept','nama','nama_laptop','spesifikasi','status' );

            $list = $this->m_querybuiler->get_datatables($table, $column_order, $column_search, $gid);
            // echo $this->db->last_query(); die;
            $data = array();
            $no = $_POST['start'];

              foreach ($list as $aData) {
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = $aData->kode_laptop;
                    $row[] = $aData->nama_pengguna;
                    $row[] = $aData->nama_parent_dept;
                    $row[] = $aData->nama;
                    $row[] = $aData->nama_laptop;
                    $row[] = $aData->spesifikasi;
                    if ($aData->status =="RUSAK/NOT FIXABLE"){
                        $row[]="<span class='label label-danger'>" . $aData->status. "</span>";
                    }elseif($aData->status =="HILANG/DICURI") {
                        $row[]="<span class='label label-danger'>" .$aData->status."</span>";
                    }elseif($aData->status =="ARSIP/DISIMPAN") {
                        $row[]="<span class='label label-warning'>" .$aData->status."</span>";
                    } else {
                        $row[]="<span class='label label-success'>" .$aData->status."</span>";
                    }

                    $row[] = anchor('laptop/detail/' . $aData->kode_laptop, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="View Detail"></i>');
                    $row[] = anchor('laptop/delete/' . $aData->id_laptop, '<i class="btn btn-danger btn-sm icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')"));
                    $data[] = $row;
                }
            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->m_querybuiler->count_all($table),
                    "recordsFiltered" => $this->m_querybuiler->count_filtered($table, $column_order, $column_search),
                    "data" => $data,
                );
            echo json_encode($output);
    }

    function show_pc(){
            if ($this->session->userdata('role')=='Administrator'){
                $gid="";
            }else{
                $gid= $this->session->userdata('gid');
            }

            $table = 'v_komputer';
            $x = 1;
            $column_order = array(null, 'kode_komputer','nama_pengguna','nama_parent_dept','nama','nama_komputer', 'spesifikasi','status', null, null);
            $column_search = array('kode_komputer','nama_pengguna','nama_parent_dept','nama','nama_komputer','spesifikasi','status' );

            $list = $this->m_querybuiler->get_datatables($table, $column_order, $column_search, $gid);
            $data = array();
            $no = $_POST['start'];

              foreach ($list as $aData) {
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = $aData->kode_komputer;
                    $row[] = $aData->nama_pengguna;
                    $row[] = $aData->nama_parent_dept;
                    $row[] = $aData->nama;
                    $row[] = $aData->nama_komputer;
                    $row[] = $aData->spesifikasi;
                    if ($aData->status =="RUSAK/NOT FIXABLE"){
                        $row[]="<span class='label label-danger'>" . $aData->status. "</span>";
                    }elseif($aData->status =="HILANG/DICURI") {
                        $row[]="<span class='label label-danger'>" .$aData->status."</span>";
                    }elseif($aData->status =="ARSIP/DISIMPAN") {
                        $row[]="<span class='label label-warning'>" .$aData->status."</span>";
                    } else {
                        $row[]="<span class='label label-success'>" .$aData->status."</span>";
                    }
                    // $row[] = $aData->note;
                    $row[] = anchor('komputer/detail/' . $aData->kode_komputer, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="View Detail"></i>');
                    $row[] = anchor('komputer/delete/' . $aData->id_komputer, '<i class="btn btn-danger btn-sm icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')"));
                    $data[] = $row;
                }
            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->m_querybuiler->count_all($table),
                    "recordsFiltered" => $this->m_querybuiler->count_filtered($table, $column_order, $column_search),
                    "data" => $data,
                );
            echo json_encode($output);
    }

    function show_monitor(){
            if ($this->session->userdata('role')=='Administrator'){
                $gid="";
            }else{
                $gid= $this->session->userdata('gid');
            }

            $table = 'v_monitor';
            $x = 1;
            $column_order = array(null, 'kode_monitor','nama_pengguna','nama_parent_dept','nama','jenis_monitor', 'spesifikasi','status', null, null);
            $column_search = array('kode_monitor','nama_pengguna','nama_parent_dept','nama','jenis_monitor','spesifikasi','status' );

            $list = $this->m_querybuiler->get_datatables($table, $column_order, $column_search, $gid);
            $data = array();
            $no = $_POST['start'];
              foreach ($list as $aData) {
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = $aData->kode_monitor;
                    $row[] = $aData->nama_pengguna;
                    $row[] = $aData->nama_parent_dept;
                    $row[] = $aData->nama;
                    $row[] = $aData->jenis_monitor;
                    $row[] = $aData->spesifikasi;
                    if ($aData->status =="RUSAK/NOT FIXABLE"){
                        $row[]="<span class='label label-danger'>" . $aData->status. "</span>";
                    }elseif($aData->status =="HILANG/DICURI") {
                        $row[]="<span class='label label-danger'>" .$aData->status."</span>";
                    }elseif($aData->status =="ARSIP/DISIMPAN") {
                        $row[]="<span class='label label-warning'>" .$aData->status."</span>";
                    } else {
                        $row[]="<span class='label label-success'>" .$aData->status."</span>";
                    }

                    $row[] = anchor('monitor/detail/' . $aData->kode_monitor, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="View Detail"></i>');
                    $row[] = anchor('monitor/delete/' . $aData->id_monitor, '<i class="btn btn-danger btn-sm icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')"));
                    $data[] = $row;
                }
            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->m_querybuiler->count_all($table),
                    "recordsFiltered" => $this->m_querybuiler->count_filtered($table, $column_order, $column_search),
                    "data" => $data,
                );
            echo json_encode($output);
    }


        function show_printer(){
                if ($this->session->userdata('role')=='Administrator'){
                    $gid="";
                }else{
                    $gid= $this->session->userdata('gid');
                }

                $table = 'v_printer';
                $x = 1;
                $column_order = array(null, 'kode_printer','nama_pengguna','nama_parent_dept','nama','jenis_printer', 'spesifikasi','status', null, null);
                $column_search = array('kode_printer','nama_pengguna','nama_parent_dept','nama','jenis_printer','spesifikasi','status' );

                $list = $this->m_querybuiler->get_datatables($table, $column_order, $column_search, $gid);
                $data = array();
                $no = $_POST['start'];
                  foreach ($list as $aData) {
                        $no++;
                        $row = array();
                        $row[] = $no;
                        $row[] = $aData->kode_printer;
                        $row[] = $aData->nama_pengguna;
                        $row[] = $aData->nama_parent_dept;
                        $row[] = $aData->nama;
                        $row[] = $aData->jenis_printer;
                        $row[] = $aData->spesifikasi;
                        if ($aData->status =="RUSAK/NOT FIXABLE"){
                            $row[]="<span class='label label-danger'>" . $aData->status. "</span>";
                        }elseif($aData->status =="HILANG/DICURI") {
                            $row[]="<span class='label label-danger'>" .$aData->status."</span>";
                        }elseif($aData->status =="ARSIP/DISIMPAN") {
                            $row[]="<span class='label label-warning'>" .$aData->status."</span>";
                        } else {
                            $row[]="<span class='label label-success'>" .$aData->status."</span>";
                        }

                        $row[] = anchor('printer/detail/' . $aData->kode_printer, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="View Detail"></i>');
                        $row[] = anchor('printer/delete/' . $aData->id_printer, '<i class="btn btn-danger btn-sm icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')"));
                        $data[] = $row;
                    }
                $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_querybuiler->count_all($table),
                        "recordsFiltered" => $this->m_querybuiler->count_filtered($table, $column_order, $column_search),
                        "data" => $data,
                    );
                echo json_encode($output);
        }

        function show_device(){
                if ($this->session->userdata('role')=='Administrator'){
                    $gid="";
                }else{
                    $gid= $this->session->userdata('gid');
                }

                $table = 'v_device';
                $x = 1;
                $column_order = array(null, 'kode_network','jenis_network', 'spesifikasi','lokasi','status', null, null);
                $column_search = array('kode_network','jenis_network','spesifikasi','lokasi','status' );

                $list = $this->m_querybuiler->get_datatables($table, $column_order, $column_search, $gid);
                $data = array();
                $no = $_POST['start'];
                  foreach ($list as $aData) {
                        $no++;
                        $row = array();
                        $row[] = $no;
                        $row[] = $aData->kode_network;
                        $row[] = $aData->jenis_network;
                        $row[] = $aData->spesifikasi;
                        $row[] = $aData->lokasi;
                        if ($aData->status =="RUSAK/NOT FIXABLE"){
                            $row[]="<span class='label label-danger'>" . $aData->status. "</span>";
                        }elseif($aData->status =="HILANG/DICURI") {
                            $row[]="<span class='label label-danger'>" .$aData->status."</span>";
                        }elseif($aData->status =="ARSIP/DISIMPAN") {
                            $row[]="<span class='label label-warning'>" .$aData->status."</span>";
                        } else {
                            $row[]="<span class='label label-success'>" .$aData->status."</span>";
                        }

                        $row[] = anchor('device/detail/' . $aData->kode_network, '<i class="btn btn-info btn-sm icon-eye-open" data-toggle="tooltip" title="View Detail"></i>');
                        $row[] = anchor('device/delete/' . $aData->id_network, '<i class="btn btn-danger btn-sm icon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')"));
                        $data[] = $row;
                    }
                $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_querybuiler->count_all($table),
                        "recordsFiltered" => $this->m_querybuiler->count_filtered($table, $column_order, $column_search),
                        "data" => $data,
                    );
                echo json_encode($output);
        }


    function view_laptop(){
      if ($this->session->userdata('role')=='Administrator'){
          $data=$this->m_laptop->semua_arsip()->result();
      }else{
          $data=$this->m_laptop->semuagid_arsip()->result();
      }

        $no=1;
        foreach($data as $r) {
        $dept=$this->db->get_where('tb_departemen',array('id_dept'=>$r->parent))->row_array();
            if($r->parent==0){
                    $deptnama=$r->nama;
            }else{
                    $deptnama=$dept['nama'];
            }
            if ($r->status =="RUSAK/NOT FIXABLE"){
                $status="<span class='label label-danger'>" . $r->status. "</span>";
            }elseif($r->status =="HILANG/DICURI") {
                $status="<span class='label label-danger'>" .$r->status."</span>";
            }elseif($r->status =="ARSIP/DISIMPAN") {
                $status="<span class='label label-warning'>" .$r->status."</span>";
            }else{
				$status="<span class='label label-warning'>" .$r->status."</span>";
			}
            $query[] = array(
                'no'=>$no++,
                'kode_laptop'=>$r->kode_laptop,
                'nama_pengguna'=>$r->nama_pengguna,
                'dept'=>$deptnama,
                'subdept'=>$r->nama,
                'tgl_inv'=>tgl_indo($r->tgl_inv),
                'nama_laptop'=>$r->nama_laptop,
                'spesifikasi'=>$r->spesifikasi,
                'sn'=>$r->serial_number,
                'ip'=>$r->network,
                'status'=>$status,
                'view'=>anchor('laptop/detail/' . $r->kode_laptop, '<i class="btn btn-info btn-sm fa fa-eye" data-toggle="tooltip" title="View Detail"></i>'),
                'delete'=>anchor('laptop/delete/' . $r->id_laptop, '<i class="btn-sm btn-info glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")),
            );
        }
        $result=array('data'=>$query);
        echo json_encode($result);
    }



    function view_komputer(){
        if ($this->session->userdata('role')=='Administrator'){
            $ambildata=$this->m_komputer->semua_arsip()->result();
        }else{
            $ambildata=$this->m_komputer->semuagid_arsip()->result();
        }
        $no=1;
        foreach($ambildata as $r) {
        $dept=$this->db->get_where('tb_departemen',array('id_dept'=>$r->parent))->row_array();
            if($r->parent==0){
                    $deptnama=$r->nama;
            }else{
                    $deptnama=$dept['nama'];
            }
            if ($r->status =="RUSAK/NOT FIXABLE"){
                $status="<span class='label label-danger'>" . $r->status. "</span>";
            }elseif($r->status =="HILANG/DICURI") {
                $status="<span class='label label-danger'>" .$r->status."</span>";
            }elseif($r->status =="ARSIP/DISIMPAN") {
                $status="<span class='label label-warning'>" .$r->status."</span>";
            }
            $query[] = array(
                'no'=>$no++,
                'kode_komputer'=>$r->kode_komputer,
                'nama_pengguna'=>$r->nama_pengguna,
                'dept'=>$deptnama,
                'subdept'=>$r->nama,
                'tgl_inv'=>tgl_indo($r->tgl_inv),
                'nama_komputer'=>$r->nama_komputer,
                'spesifikasi'=>$r->spesifikasi,
                'sn'=>$r->serial_number,
                'ip'=>$r->network,
                'status'=>$status,
                'edit'=>anchor('komputer/detail/' . $r->kode_komputer, '<i class="btn btn-info btn-sm fa fa-eye" data-toggle="tooltip" title="View Detail"></i>'),
                'delete'=>''.anchor('komputer/delete/' . $r->id_komputer, '<i class="btn-sm btn-info glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")).'',
            );
        }
        $result=array('data'=>$query);
        echo  json_encode($result);
    }




    function view_monitor(){
        if ($this->session->userdata('role')=='Administrator'){
            $ambildata=$this->m_monitor->semua_arsip()->result();
        }else{
            $ambildata=$this->m_monitor->semuagid_arsip()->result();
        }
        $no=1;
        foreach($ambildata as $r) {
        $dept=$this->db->get_where('tb_departemen',array('id_dept'=>$r->parent))->row_array();
            if($r->parent==0){
                    $deptnama=$r->nama;
            }else{
                    $deptnama=$dept['nama'];
            }
            if ($r->status =="RUSAK/NOT FIXABLE"){
                $status="<span class='label label-danger'>" . $r->status. "</span>";
            }elseif($r->status =="HILANG/DICURI") {
                $status="<span class='label label-danger'>" .$r->status."</span>";
            }elseif($r->status =="ARSIP/DISIMPAN") {
                $status="<span class='label label-warning'>" .$r->status."</span>";
            }
            $query[] = array(
                'no'=>$no++,
                'kode_monitor'=>$r->kode_monitor,
                'nama_pengguna'=>$r->nama_pengguna,
                'dept'=>$deptnama,
                'subdept'=>$r->nama,
                'tgl_inv'=>tgl_indo($r->tgl_inv),
                'jenis_monitor'=>$r->jenis_monitor,
                'spesifikasi'=>$r->spesifikasi,
                'status'=>$status,
                'edit'=>anchor('monitor/detail/' . $r->kode_monitor, '<i class="btn btn-info btn-sm fa fa-eye" data-toggle="tooltip" title="View Detail"></i>'),
                'delete'=>''.anchor('monitor/delete/' . $r->id_monitor, '<i class="btn-sm btn-info glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")).'',
            );
        }
        $result=array('data'=>$query);
        echo  json_encode($result);
    }




    function view_printer(){
        if ($this->session->userdata('role')=='Administrator'){
            $ambildata=$this->m_printer->semua_arsip()->result();
        }else{
            $ambildata=$this->m_printer->semuagid_arsip()->result();
        }
        $no=1;
        foreach($ambildata as $r) {
        $dept=$this->db->get_where('tb_departemen',array('id_dept'=>$r->parent))->row_array();
            if($r->parent==0){
                    $deptnama=$r->nama;
            }else{
                    $deptnama=$dept['nama'];
            }
            if ($r->status =="RUSAK/NOT FIXABLE"){
                $status="<span class='label label-danger'>" . $r->status. "</span>";
            }elseif($r->status =="HILANG/DICURI") {
                $status="<span class='label label-danger'>" .$r->status."</span>";
            }elseif($r->status =="ARSIP/DISIMPAN") {
                $status="<span class='label label-warning'>" .$r->status."</span>";
            }
            $query[] = array(
                'no'=>$no++,
                'kode_printer'=>$r->kode_printer,
                'nama_pengguna'=>$r->nama_pengguna,
                'dept'=>$deptnama,
                'subdept'=>$r->nama,
                'tgl_inv'=>tgl_indo($r->tgl_inv),
                'jenis_printer'=>$r->jenis_printer,
                'spesifikasi'=>$r->spesifikasi,
                'status'=>$status,
                'edit'=>anchor('printer/detail/' . $r->kode_printer, '<i class="btn btn-info btn-sm fa fa-eye" data-toggle="tooltip" title="View Detail"></i>'),
                'delete'=>anchor('printer/delete/' . $r->id_printer, '<i class="btn-sm btn-info glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")),
            );
        }
        $result=array('data'=>$query);
        echo  json_encode($result);
    }




    function view_device(){
        if ($this->session->userdata('role')=='Administrator'){
            $ambildata=$this->m_network->semua_arsip()->result();
        }else{
            $ambildata=$this->m_network->semuagid_arsip()->result();
        }
        $no=1;
        foreach($ambildata as $r) {
            if ($r->status =="RUSAK/NOT FIXABLE"){
                $status="<span class='label label-danger'>" . $r->status. "</span>";
            }elseif($r->status =="HILANG/DICURI") {
                $status="<span class='label label-danger'>" .$r->status."</span>";
            }elseif($r->status =="ARSIP/DISIMPAN") {
                $status="<span class='label label-warning'>" .$r->status."</span>";
            }
            $query[] = array(
                'no'=>$no++,
                'kode_network'=>$r->kode_network,
                'lokasi'=>strtoupper($r->lokasi),
                'tgl_inv'=>tgl_indo($r->tgl_inv),
                'jenis_network'=>strtoupper($r->jenis_network),
                'spesifikasi'=>$r->spesifikasi,
                'status'=>$status,
                'edit'=>anchor('device/detail/' . $r->kode_network, '<i class="btn btn-info btn-sm fa fa-eye" data-toggle="tooltip" title="View Detail"></i>'),
                'delete'=>anchor('device/delete/' . $r->id_network, '<i class="btn-sm btn-info glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")),
            );
        }
        $result=array('data'=>$query);
        echo  json_encode($result);
    }


}
