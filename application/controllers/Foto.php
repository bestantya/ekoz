<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Foto extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('m_foto');
        chek_session();
    }
	public function index() {
        $data['title'] = "Gallery";
		$gid=$this->session->userdata('gid');
		$data['group'] = $this->db->get_where('tb_group',array('gid'=>$gid))->row_array();
        $this->template->display('dashboard/index', $data);      
    }		
   function foto() {
		$gid=$this->session->userdata('gid');
		$data['group'] = $this->db->get_where('tb_group',array('gid'=>$gid))->row_array();
		$this->template->display('foto/foto');

    }
	
		function maps() {
		$gid=$this->session->userdata('gid');
		$data['group'] = $this->db->get_where('tb_group',array('gid'=>$gid))->row_array();
		$this->template->display('foto/maps');

    }
   

}
