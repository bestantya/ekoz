-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 30, 2019 at 06:17 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsimaster`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

DROP TABLE IF EXISTS `tb_barang`;
CREATE TABLE IF NOT EXISTS `tb_barang` (
  `kode_barang` varchar(30) NOT NULL DEFAULT '',
  `id_kategori` int(30) DEFAULT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `merek_barang` varchar(30) DEFAULT NULL,
  `spesifikasi` varchar(250) DEFAULT NULL,
  `satuan` enum('PCS','PACK','UNIT','ROLL','METER','BUAH') DEFAULT 'PCS',
  `gid` int(10) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`kode_barang`, `id_kategori`, `nama_barang`, `merek_barang`, `spesifikasi`, `satuan`, `gid`) VALUES
('B013.0007', 5, 'WIFI ', 'Cisco Linksys', 'Linksys Dual-Band N900 Router, Model: EA4500-Ap', 'PCS', 1),
('B013.0008', 4, 'POWER SUPLY', 'ADVANCE', 'ADVANCE 450W', 'PCS', 1),
('B013.0009', 4, 'RAM DDR2 V-GEN', 'V-GEN', 'V-GEN DDR2 1GB', 'PCS', 1),
('B013.0010', 1, 'PERSONAL COMPUTER', 'GIGABYTE', 'DUAL CORE 3,0 GHZ, GIGABYTE, HDD 500 GB, RAM DDR3 1 GB ', 'UNIT', 1),
('B013.0011', 7, 'LED', 'LG', 'LG FLATRON E1642 15,6\"', 'UNIT', 1),
('B013.0012', 3, 'UPS', 'ICA', 'ICA UPS 2000 VA', 'UNIT', 1),
('B013.0013', 3, 'MOUSE PS2', 'GENIUS', 'GENIUS PS/2 MOUSE', 'PCS', 1),
('B013.0014', 3, 'MOUSE USB', 'GENIUS', 'GENIUS USB MOUSE', 'PCS', 1),
('B013.0015', 5, 'KONEKTOR RJ45', 'AMP', 'CONECTOR RJ45 AMP (50)', 'PACK', 1),
('B013.0017', 8, 'DESJET', 'CANNON', 'PRINTER CANON MP237', 'UNIT', 1),
('B013.0018', 3, 'FLASHDISK', 'KINGSTONE', 'KINGSTONE 8GB', 'PCS', 1),
('B013.0019', 5, 'SWITCH', 'TP-LINK', 'TP-LINK TL-SG1008D GIGABIT 8 PORT', 'UNIT', 1),
('B013.0020', 3, 'UPS', 'ICA', 'ICA 1200VA', 'UNIT', 1),
('B013.0022', 4, 'RAM DDR3 4 GB server', 'V-GEN', 'DDR3 4GB, v-GEN', 'PCS', 1),
('B013.0023', 1, 'PC+LCD+keybrd,Mouse', 'GIGABYTE', 'Mb; GA_H61M-DS2, RAM DDR3 1GB, HDD 500GB WD, LED LG 16EN33, Keyboard+mouse EPRAIZER', 'UNIT', 1),
('B013.0024', 4, 'RAM DDR3', 'V-GEN', 'V-GEN DDR3 1GB', 'PCS', 1),
('B013.0026', 8, 'PRINTER  L110', 'Epson', 'Epson L110', 'UNIT', 1),
('B013.0027', 3, 'FLASDISK 16 GB', 'Kingston', 'Kingston 16 GB', 'PCS', 1),
('B013.0028', 3, 'Keyboard PS/2', 'Genius', 'PS/2 Genius', 'PCS', 1),
('B013.0029', 3, 'Keyboard USB', 'Genius', 'keyboard Genius USB', 'PCS', 1),
('B013.0030', 4, 'HARDISK SATA 500 GB', 'WESTERN DIGITAL', 'WESTERN DIGITAL SATA 500 GB', 'PCS', 1),
('B013.0031', 5, 'KABEL UTP CAT 5E', 'BELDEN', 'BELDEN UTP CATEGORY 5E USA', 'ROLL', 1),
('B013.0032', 9, 'LAN TESTER', 'CABLE TESTER', 'CABLE TESTER NETWORK ', 'PCS', 1),
('B013.0033', 4, 'HARDISK LAPTOP', 'HGST', 'HGST 500 GB', 'PCS', 1),
('B013.0034', 4, 'RAM LAPTOP DDR3 4 GB', 'V-GEN', 'V-GEN DDR 3 4 GB', 'PCS', 1),
('B013.0035', 10, 'REFILL EPSON LXXX', 'EPSON ', 'EPSON BK T6641, C T6642, M T6643, Y T6644', 'ROLL', 1),
('B013.0036', 10, 'REFILL TINTA T6641 BK', 'EPSON', 'EPSON T6641 BLACK', 'PCS', 1),
('B013.0038', 2, 'LAPTOP ASUS', 'ASUS X450 CC', 'CORE i3 , HDD 500 GB, RAM 2 GB, 14\"', 'UNIT', 1),
('B013.0039', 4, 'CHARGER LAPTOP', 'ACER 4732Z', 'ACER 4732Z MODEL PA-1650-02 19V OUTPUT', 'UNIT', 1),
('B013.0040', 8, 'DOTMATRIX', 'EPSON ', 'EPSON LX-310', 'UNIT', 1),
('B013.0041', 7, 'LED LG 16EN33', 'LG ', 'LG FLATRON 16EN33 15.6\"', 'UNIT', 1),
('B013.0042', 5, 'LAN CARD GIGABIT', 'D-LINK', 'DGE-528T GIGABIT', 'UNIT', 1),
('B013.0044', 5, 'WIFI CISCO', 'CISCO LINKSYS EA4500 ', 'LINKSYS EA4500 GIGABIT DUAL-BAND N900', 'UNIT', 1),
('B013.0046', 4, 'FAN COOLER SCORPION', 'SCORPION KING', 'SCORPION KING FAN COOLER HF-560', 'UNIT', 1),
('B013.0047', 4, 'KABEL VGA 1.5M', 'DIGILINK', 'DIGILINK VGA CABLE 1.5M', 'PCS', 1),
('B013.0048', 7, 'LED LG 19EN33', 'LG', 'LG FLATRON 16EN33 19.6\"', 'UNIT', 1),
('B013.0050', 2, 'LAPTOP SONY VAIO', 'SONY VAIO SVT11215SG', 'Intel Core i5 4210Y 1.5GHz, RAM DDR3 4GB, HDD SSD 128 GB', 'UNIT', 1),
('B013.0051', 3, 'UPS 1200VA', 'ICA', 'ICA CE 1200VA', 'UNIT', 1),
('B013.0052', 8, 'PRINTER L800', 'EPSON', 'EPSON L800', 'UNIT', 1),
('B013.0053', 8, 'PRINTER L210', 'EPSON', 'EPSON L210 ALL IN PRINTER', 'UNIT', 1),
('B013.0054', 3, 'KEYBOARD , MOUSE WIFI', 'LOGITECH', 'LOGITECH WIRELESS COMBO MK220', 'UNIT', 1),
('B013.0058', 3, 'MOUSE WIFI', 'LOGITECH', 'LOGITECH WIRELESS MOUSE M185', 'UNIT', 1),
('B013.0059', 10, 'CATRIDGE TX110 B', 'EPSON', 'EPSON CATRIDGE TX110 BLACK', 'UNIT', 1),
('B013.0060', 10, 'CATRIDGE TX110 M', 'EPSON', 'EPSON CATRIDGE TX1100 MAGENTA', 'UNIT', 1),
('B013.0061', 10, 'CATRIDGE TX110 Y', 'EPSON', 'EPSON CATRIDGE TX1100 YELLOW', 'UNIT', 1),
('B013.0062', 10, 'CATRIDGE TX110 C', 'EPSON', 'EPSON CATRIDGE TX1100 CYAN', 'UNIT', 1),
('B013.0063', 5, 'SWITCH 16 PORT', 'D-LINK', 'D-LINK DGS 1015D 16 PORT', 'UNIT', 1),
('B013.0064', 5, 'WIFI CARD TP-LINK', 'TP-LINK', '150Mbps Wifi PCI Card TL-WN781ND', 'UNIT', 1),
('B013.0065', 3, 'FLASHDISK TOSHIBA ', 'TOSHIBA', 'TOSHIBA 4 GB', 'UNIT', 1),
('B013.0066', 1, 'COMPUTER CORE 2 DUO', 'Gigabyte', 'INTEL CORE 2 DUO 3.00 GHz ; HDD 500 GB ; RAM 1 GB', 'UNIT', 1),
('B013.0067', 8, 'PRINTER EPSON L550', 'EPSON', 'EPSON L550', 'UNIT', 1),
('B013.0068', 2, 'LAPTOP ASUS', 'ASUS X451C', 'Intel Core i3-3217U CPU @1.80 GHz, RAM 2 GB, HDD 500 GB', 'UNIT', 1),
('B013.0069', 4, 'HDD IBM 600GB', 'IBM ', 'IBM 600 GB 15 K SAS 3.5 Inch', 'UNIT', 1),
('B013.0070', 4, 'RAM DDR3 4GB', 'V-GEN', 'DDR3 4GB', 'UNIT', 1),
('B013.0071', 10, 'DVD BLANK', 'MAXELL', 'DVD-R 4.7GB', 'PCS', 1),
('B013.0072', 5, 'LAN CARD PCI EXPRESS GIGABIT', 'TP-LINK', 'GIGABIT PCI EXPRESS TG-3468', 'PCS', 1),
('B013.0073', 3, 'FLASHDISK 16GB', 'TRANSCEND', 'TRANSCEND 16 GB USB ', 'PCS', 1),
('B013.0074', 3, 'SCANNER CANON 110', 'CANON', 'CANONSCAN LIDE 110', 'UNIT', 1),
('B013.0075', 5, 'KABEL UTP CAT 6', 'AMP', 'UTP AMP CATEGORY 6', 'ROLL', 1),
('B013.0076', 1, 'COMPUTER BUILTUP LENOVO', 'LENOVO', 'Lenovo E93-1A Desktop\r Core i3 4130 ( 3,46 Ghz , 3M cache )\r 4 GB DDR3 PC3 12800 of RAM\r 500 GB HDD SATA 7200 rpm, Memory Card Reader\r DVDRW Multiburner ', 'UNIT', 2),
('B013.0077', 7, 'LED LENOVO', 'LENOVO', 'LED LENOVO 18.5\"', 'UNIT', 1),
('B013.0078', 4, 'BATERAI BIOS', 'MAXELL', 'MAXELL MICRO LITHIUM CELL', 'UNIT', 1),
('B013.0079', 8, 'PRINTER L120', 'EPSON', 'EPSON L120', 'UNIT', 1),
('B013.0080', 3, 'PROJECTOR NEC', 'NEC', 'NEC VE281 HDMI', 'UNIT', 1),
('B013.0081', 9, 'STORAGE SERVER', 'LENOVO EMC Storcenter ix2 (355', 'LENOVO EMC Storcenter ix2 (35552) 2 TB', 'UNIT', 1),
('B013.0082', 3, 'MOUSE PEN WACOOM', 'WACOM ', 'WACOM INTUOS ', 'UNIT', 1),
('B013.0083', 5, 'WIFI ROUTER TP-LINK TL-WA5110G', 'TP-LINK', '54 Mbps', 'PCS', 1),
('B013.0084', 10, 'CD KOSONG', 'MEDIATECH', '700 MB', 'PCS', 1),
('B013.0085', 4, 'Memory V-GEN SO-DIMM DDR3 4GB', 'V-GEN', '4GB PC12800', 'PCS', 1),
('B013.0087', 5, 'MIKROTIK ROUTHERBOARD ', 'MIKROTIK', 'CRS 125-24G-1S-RM', 'UNIT', 1),
('B013.0088', 5, 'Wallmountrack', 'Wallmountrack', 'Wallmountrack 19\" 8U-450mm', 'PCS', 1),
('B013.0089', 3, 'MOUSE USB', 'LOGITECH', 'LOGITECH B100', 'PCS', 1),
('B016.0001', 4, 'HARDDISK SATA 2TB SEAGATE', 'SEAGATE', '2TB', 'PCS', 1),
('B016.0002', 4, 'MOTHERBOARD', 'GIGABYTE', 'LG-775', 'PCS', 1),
('B016.0003', 9, 'CONVERTER USB TO IDE/SATA', 'CABLEMAX', 'USB 2.0 TO SATA IDE CABLE', 'PCS', 1),
('B016.0004', 4, 'PROCESSOR', 'Intel', 'CORE 2 DUO', 'PCS', 1),
('B016.0005', 10, 'TERMAL PASTA', 'HC131', 'Headsink Compounds HC131', 'PCS', 1),
('B016.0006', 5, 'MIKROTIK 2S+RM', 'Mikrotik', 'MIKROTIKROUTHERBOARD CRS 226-24G-2S+RM', 'UNIT', 1),
('B016.0007', 8, 'PRINTER IP2770', 'CANON', 'CANON PIXMA IP 2770', 'UNIT', 1),
('B016.0009', 8, 'PRINTER', 'HP LASER JET PRO', 'HP LASER JET PRO P1102', 'UNIT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_departemen`
--

DROP TABLE IF EXISTS `tb_departemen`;
CREATE TABLE IF NOT EXISTS `tb_departemen` (
  `id_dept` int(10) NOT NULL AUTO_INCREMENT,
  `gid` int(10) DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `parent` int(20) DEFAULT NULL,
  PRIMARY KEY (`id_dept`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_departemen`
--

INSERT INTO `tb_departemen` (`id_dept`, `gid`, `nama`, `parent`) VALUES
(1, 1, 'UTILITY & POWER SUPPLY', 0),
(2, 1, 'TECHNIC', 1),
(3, 1, 'CIVIL', 1),
(4, 1, 'IPAL', 1),
(5, 1, 'QUALITY ASS & FINISH GOODS', 0),
(6, 1, 'QUALITY CONTROL', 5),
(7, 1, 'PROSES CONTROL', 5),
(8, 1, 'JAHIT/ KEMAS', 5),
(9, 1, 'KEMAS', 8),
(10, 1, 'DISTRIBUSI', 8),
(11, 1, 'HRD & GA', 0),
(12, 1, 'HRD & PERSONAL', 11),
(13, 1, 'INDUSTRIAL RELATIONSHIP', 11),
(14, 1, 'GENERAL AFFAIR', 11),
(15, 1, 'FINISHING', 0),
(16, 1, 'FINISHING', 15),
(17, 1, 'PRINTING PREP', 15),
(18, 1, 'PRINTING PROD', 15),
(19, 1, 'DESIGNER PRINTING', 15),
(20, 1, 'DYEING', 0),
(21, 1, 'DYEING', 20),
(22, 1, 'LOG & PURCH', 0),
(23, 1, 'WAREHOUSE', 22),
(24, 1, 'PURCHASING', 22),
(25, 1, 'MATERIAL CONTROL', 22),
(26, 1, 'PPIC', 0),
(27, 1, 'PPIC PPC', 26),
(28, 1, 'MATERIAL CONTROL', 26),
(32, 1, 'PRODUKSI', 0),
(33, 1, 'PRODUKSI PPIC', 32),
(35, 1, 'WEAVING 2 DAN 3 ', 32),
(36, 1, 'QC WEAVING', 32),
(37, 1, 'FINANCE & ACCOUNTING', 0),
(38, 1, 'FINANCE', 37),
(39, 1, 'ACCOUNTING', 37),
(40, 1, 'ICT', 0),
(41, 1, 'SYS DEV', 40),
(42, 1, 'NET WARE', 40),
(43, 1, 'WEB', 40),
(47, 1, 'SALES', 0),
(48, 1, 'SALES 1', 47),
(49, 1, 'SALES 2', 47),
(57, 2, 'TEKNIK', 0),
(58, 2, 'HRD & GA', 0),
(59, 2, 'FINANCE & ACCOUNTING', 0),
(70, 2, 'ICT', 0),
(71, 2, 'NETWARE', 70);

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

DROP TABLE IF EXISTS `tb_group`;
CREATE TABLE IF NOT EXISTS `tb_group` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pt` varchar(50) DEFAULT 'Nama Perusahaan',
  `nama_group` varchar(20) NOT NULL,
  `nama_alias` varchar(10) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `logo` varchar(50) NOT NULL DEFAULT 'kantor.jpg',
  `logo_dashboard` varchar(30) NOT NULL DEFAULT 'kantor.jpg',
  PRIMARY KEY (`gid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`gid`, `nama_pt`, `nama_group`, `nama_alias`, `alamat`, `logo`, `logo_dashboard`) VALUES
(1, 'PT.Sadua Indo', 'Kantor Factory 1', 'SA_F1', 'Gintungan, Kec.Tengaran, Kab. Semarang', 'Computer1.png', 'Computer1.png'),
(2, 'PT.Sadua Indo', 'Kantor Factory 2', 'SA_F2', 'Gintungan, Kec.Tengaran, Kab. Semarang', 'Computer2.png', 'Computer2.png'),
(3, 'PT.Sadua Indo', 'Kantor Factory 3', 'SA_F3', 'Gintungan, Kec.Tengaran, Kab. Semarang', 'Computer3.png', 'Computer3.png'),
(4, 'PT.Sadua Indo', 'Kantor Factory 4', 'SA_F4', 'Gintungan, Kec.Tengaran, Kab. Semarang', 'Computer4.png', 'Computer4.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_history`
--

DROP TABLE IF EXISTS `tb_inv_history`;
CREATE TABLE IF NOT EXISTS `tb_inv_history` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `no_inventaris` varchar(20) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `status` enum('Buat Baru','Dipinjamkan','Kembali','Mutasi') DEFAULT 'Buat Baru',
  `admin` varchar(30) DEFAULT NULL,
  `id_pengguna_awal` varchar(30) DEFAULT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `lokasi` varchar(50) NOT NULL,
  `note` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_history`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_history`
--

INSERT INTO `tb_inv_history` (`id_history`, `no_inventaris`, `tgl_update`, `status`, `admin`, `id_pengguna_awal`, `id_pengguna`, `lokasi`, `note`) VALUES
(1, 'LAP-KA1-17001', '2017-02-03 10:44:56', 'Buat Baru', 'admin kantor1', 'U016.0004', 'U016.0004', '', 'Inventory Baru'),
(2, 'LAP-KA1-17002', '2017-02-03 10:46:03', 'Buat Baru', 'admin kantor1', 'U016.0002', 'U016.0002', '', 'Inventory Baru'),
(3, 'CPU-KA1-17001', '2017-02-03 10:47:51', 'Buat Baru', 'admin kantor1', 'U016.0001', 'U016.0001', '', 'Inventory Baru'),
(4, 'CPU-KA1-17002', '2017-02-03 10:48:41', 'Buat Baru', 'admin kantor1', 'U016.0007', 'U016.0007', '', 'Inventory Baru'),
(5, 'LAP-KA2-17001', '2017-02-03 12:46:37', 'Buat Baru', 'Kantor2', 'U017.0001', 'U017.0001', '', 'Inventory Baru'),
(6, 'CPU-KA2-17001', '2017-02-03 13:27:13', 'Buat Baru', 'Kantor2', 'U017.0002', 'U017.0002', '', 'Inventory Baru'),
(7, 'LAP-KA1-17003', '2017-02-17 11:26:59', 'Buat Baru', 'administrator', 'U016.0005', 'U016.0005', '', 'Inventory Baru'),
(8, 'LAP-KA1-17004', '2017-02-17 11:27:32', 'Buat Baru', 'administrator', 'U016.0006', 'U016.0006', '', 'Inventory Baru'),
(9, 'PRI-KA1-17001', '2017-02-17 15:23:48', 'Buat Baru', 'admin kantor1', NULL, 'U016.0005', '', 'New Inventory'),
(10, 'PRI-KA1-17002', '2017-02-17 15:24:09', 'Buat Baru', 'admin kantor1', NULL, 'U016.0002', '', 'New Inventory'),
(11, 'MON-KA1-17001', '2017-02-17 15:28:17', 'Buat Baru', 'admin kantor1', 'U016.0007', 'U016.0007', '', 'Inventory Baru'),
(12, 'LAP-Sadua F1-19001', '2019-08-02 13:19:49', 'Buat Baru', 'Administrator', 'U016.0001', 'U016.0001', '', 'Inventory Baru'),
(13, 'LAP-SA_F1-19191', '2019-08-02 15:52:21', 'Buat Baru', 'Administrator', 'U016.0007', 'U016.0007', '', 'Inventory Baru'),
(14, 'LAP-SA_F1-19192', '2019-09-23 08:59:54', 'Buat Baru', 'Administrator', 'U016.0004', 'U016.0004', '', 'Inventory Baru'),
(15, 'LAP-SA_F1-19191', '2012-07-11 00:00:00', 'Mutasi', 'Eko Setiawan', 'U016.0007', 'U016.0004', '', 'retytrytry'),
(16, 'LAP-SA_F1-19191', '2019-09-23 00:00:00', 'Mutasi', 'Eko Setiawan', 'U016.0004', 'U016.0001', '', 'okey'),
(17, 'LAP-SA_F1-19001', '2019-09-23 00:00:00', 'Mutasi', 'Eko Setiawan', 'U016.0001', 'U016.0005', '', 'oke'),
(18, 'LAP-SA_F1-19001', '2019-09-23 00:00:00', 'Mutasi', 'Eko Setiawan', 'U016.0005', 'U016.0004', '', 'okey'),
(19, 'LAP-SA_F1-19192', '2019-09-25 00:00:00', 'Mutasi', 'Eko Setiawan', 'U016.0005', 'U016.0001', '', 'pinjam'),
(20, 'LAP-KA2-17001', '2019-09-25 00:00:00', 'Mutasi', 'Eko Setiawan', 'U017.0001', 'U016.0007', '', 'siip'),
(21, 'CPU-SA_F1-19001', '2019-09-26 07:55:30', 'Buat Baru', 'Administrator', 'U016.0004', 'U016.0004', '', 'Inventory Baru'),
(22, 'CPU-SA_F1-19001', '2019-09-26 00:00:00', 'Dipinjamkan', 'Eko Setiawan', 'U016.0004', 'U016.0006', '', 'okey'),
(23, 'MON-SA_F1-19001', '2019-09-26 13:43:33', 'Buat Baru', 'Administrator', 'U016.0001', 'U016.0001', '', 'Inventory Baru'),
(24, 'MON-SA_F1-19001', '2019-09-26 00:00:00', 'Mutasi', 'Administrator', 'U016.0001', 'U016.0006', '', 'mutasi'),
(25, 'PRI-SA_F1-19001', '2019-09-26 14:30:34', 'Buat Baru', 'Administrator', NULL, 'U016.0004', '', 'New Inventory'),
(26, 'PRI-SA_F1-19001', '2019-09-26 00:00:00', 'Mutasi', 'Administrator', 'U016.0004', 'U016.0007', '', 'siap pkai tik'),
(27, 'DVC-SA-001', '2019-09-26 16:16:27', 'Buat Baru', 'Administrator', NULL, NULL, 'server', 'New Inventory'),
(28, 'DVC-SA-001', '2019-09-26 00:00:00', 'Mutasi', 'Administrator', NULL, NULL, 'Server Room F2', 'okey siap'),
(29, 'PRI-SA_F1-19191', '2019-09-26 17:18:57', 'Buat Baru', 'Administrator', NULL, 'U016.0001', '', 'New Inventory'),
(30, 'zxcxczx', '2019-09-26 17:22:48', 'Buat Baru', 'Administrator', NULL, NULL, 'zxczxczx', 'New Inventory'),
(31, 'CPU-SA_F1-19191', '2019-10-16 12:53:43', 'Buat Baru', 'Administrator', 'U016.0006', 'U016.0006', '', 'Inventory Baru');

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_komputer`
--

DROP TABLE IF EXISTS `tb_inv_komputer`;
CREATE TABLE IF NOT EXISTS `tb_inv_komputer` (
  `id_komputer` int(20) NOT NULL AUTO_INCREMENT,
  `kode_komputer` varchar(20) NOT NULL,
  `barcode` varchar(30) NOT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `nama_komputer` varchar(50) DEFAULT NULL,
  `spesifikasi` varchar(200) DEFAULT NULL,
  `serial_number` varchar(20) DEFAULT NULL,
  `id_lisence` varchar(30) DEFAULT NULL,
  `network` varchar(30) DEFAULT NULL,
  `tgl_inv` date DEFAULT NULL,
  `harga_beli` decimal(20,0) NOT NULL,
  `status` enum('DIGUNAKAN','SIAP DIGUNAKAN','DIPERBAIKI','DIPINJAMKAN','ARSIP/DISIMPAN','RUSAK/NOT FIXABLE','HILANG/DICURI') DEFAULT 'DIGUNAKAN',
  `note` varchar(30) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_komputer`) USING BTREE,
  UNIQUE KEY `kode_komputer` (`kode_komputer`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_komputer`
--

INSERT INTO `tb_inv_komputer` (`id_komputer`, `kode_komputer`, `barcode`, `id_pengguna`, `nama_komputer`, `spesifikasi`, `serial_number`, `id_lisence`, `network`, `tgl_inv`, `harga_beli`, `status`, `note`, `gid`) VALUES
(1, 'CPU-KA1-17001', 'CPU-KA1-17001.png', 'U016.0001', 'GIGABITE', 'INTEL CORE 2 DUO @ 3.00 GHz ; HDD 500 GB ; RAM 1 GB', '819392183', NULL, '0.0.0.0', '2016-03-04', '3500000', 'HILANG/DICURI', 'ok', 1),
(2, 'CPU-KA1-17002', 'CPU-KA1-17002.png', 'U016.0007', 'GIGABITE', 'INTEL CORE 2 CPU E5800@3.20GHZ ; HDD 500 GB ; RAM 1 GB DDR3', '718378', NULL, '0.0.0.0', '2016-06-02', '3600000', 'DIGUNAKAN', 'okeyy', 1),
(3, 'CPU-KA2-17001', '', 'U017.0002', 'HP TOWER', 'Intel CORE 2 DUO @3,0Ghz , HDD 500GB, RAM 4GB', '1234567890', NULL, '192.168.0.100', '2019-09-25', '3500000', 'SIAP DIGUNAKAN', 'okey', 2),
(4, 'CPU-SA_F1-19001', '', 'U016.0006', 'GIGABYTE', 'Core i3', '00000000', NULL, '125.154.0.2', '2019-09-26', '4000000', 'SIAP DIGUNAKAN', 'okeyy', 1),
(5, 'CPU-SA_F1-19191', '', 'U016.0006', 'ASUS', 'Intel Core i5 3.0', '3434512', NULL, '0.0.0.0', '2019-10-16', '4500000', 'DIGUNAKAN', 'ok', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_laptop`
--

DROP TABLE IF EXISTS `tb_inv_laptop`;
CREATE TABLE IF NOT EXISTS `tb_inv_laptop` (
  `id_laptop` int(30) NOT NULL AUTO_INCREMENT,
  `kode_laptop` varchar(30) NOT NULL,
  `barcode` varchar(20) NOT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `nama_laptop` varchar(50) DEFAULT NULL,
  `spesifikasi` varchar(200) DEFAULT NULL,
  `serial_number` varchar(20) DEFAULT NULL,
  `id_lisence` varchar(30) DEFAULT NULL,
  `network` varchar(30) DEFAULT NULL,
  `tgl_inv` date DEFAULT NULL,
  `harga_beli` decimal(10,0) NOT NULL,
  `status` enum('DIGUNAKAN','SIAP DIGUNAKAN','DIPERBAIKI','DIPINJAMKAN','ARSIP/DISIMPAN','RUSAK/NOT FIXABLE','HILANG/DICURI') DEFAULT 'DIGUNAKAN',
  `note` varchar(100) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_laptop`) USING BTREE,
  UNIQUE KEY `kode_laptop` (`kode_laptop`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_laptop`
--

INSERT INTO `tb_inv_laptop` (`id_laptop`, `kode_laptop`, `barcode`, `id_pengguna`, `nama_laptop`, `spesifikasi`, `serial_number`, `id_lisence`, `network`, `tgl_inv`, `harga_beli`, `status`, `note`, `gid`) VALUES
(1, 'LAP-KA1-17001', 'LAP-KA1-17001.png', 'U016.0004', 'ASUS', 'ASUS EE PC 2 GB RAM 320HDD, intel Atom ', '0012381', NULL, '0.0.0.0', '2016-03-02', '2500000', 'DIGUNAKAN', NULL, 1),
(2, 'LAP-KA1-17002', 'LAP-KA1-17002.png', 'U016.0002', 'LENOVO', 'LENOVO G480, Intel Core-i3, 2Gb Ram 500HDD, 14\"', '9183921839', NULL, '0.0.0.0', '2015-04-10', '4500000', 'DIGUNAKAN', 'dicuri', 1),
(3, 'LAP-KA2-17001', 'LAP-KA2-17001.png', 'U016.0007', 'DELL', 'DELL Inspiron Corei3, 2GB Ram, 500HDD', '89238192389', NULL, '0.0.0.0', '2016-07-06', '4000000', 'DIGUNAKAN', NULL, 2),
(4, 'LAP-KA1-17003', 'LAP-KA1-17003.png', 'U016.0005', 'DELL', 'DELL coare i3 ram 4 GB 500 HD', '21389138', NULL, '0.0.0.0', '2016-06-24', '4000000', 'HILANG/DICURI', 'hilang', 1),
(5, 'LAP-KA1-17004', 'LAP-KA1-17004.png', 'U016.0006', 'LENOVO', 'Thinkpad core i3', '81923729', NULL, '0.0.0.0', '2017-02-10', '50000000', 'DIGUNAKAN', NULL, 1),
(6, 'LAP-SA_F1-19001', '', 'U016.0004', 'HP', 'dfdfdfdfd', '1544854e435', NULL, '125.154.51.54', '2019-08-01', '3000000', 'RUSAK/NOT FIXABLE', 'Oke', 1),
(7, 'LAP-SA_F1-19191', '', 'U016.0001', 'ASUS ', 'Core i3 Ram 4gb', '343451', NULL, '125.154.51.55', '2019-09-23', '4000000', 'SIAP DIGUNAKAN', 'Oke', 1),
(8, 'LAP-SA_F1-19192', '', 'U016.0001', 'HP', 'Ram 4GB', '00000000', NULL, '125.154.51.54', '2019-09-20', '6900000', 'DIGUNAKAN', 'rtrtrtrt', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_monitor`
--

DROP TABLE IF EXISTS `tb_inv_monitor`;
CREATE TABLE IF NOT EXISTS `tb_inv_monitor` (
  `id_monitor` int(30) NOT NULL AUTO_INCREMENT,
  `kode_monitor` varchar(30) NOT NULL,
  `barcode` varchar(30) NOT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `jenis_monitor` enum('LED','LCD','CRT','TOUCH SCREEN') DEFAULT 'LED',
  `spesifikasi` varchar(200) DEFAULT NULL,
  `tgl_inv` date DEFAULT NULL,
  `harga_beli` decimal(20,0) NOT NULL,
  `status` enum('DIGUNAKAN','SIAP DIGUNAKAN','DIPERBAIKI','DIPINJAMKAN','ARSIP/DISIMPAN','RUSAK/NOT FIXABLE','DIJUAL/HILANG') DEFAULT 'DIGUNAKAN',
  `note` varchar(100) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_monitor`) USING BTREE,
  UNIQUE KEY `kode_monitor` (`kode_monitor`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_monitor`
--

INSERT INTO `tb_inv_monitor` (`id_monitor`, `kode_monitor`, `barcode`, `id_pengguna`, `jenis_monitor`, `spesifikasi`, `tgl_inv`, `harga_beli`, `status`, `note`, `gid`) VALUES
(1, 'MON-KA1-17001', 'MON-KA1-17001.png', 'U016.0007', 'LED', 'LG 15\"', '2017-02-01', '1000000', 'DIPERBAIKI', 'oke', 1),
(2, 'MON-SA_F1-19001', '', 'U016.0006', 'LED', '22\" Tuch', '2019-09-26', '1700000', 'DIGUNAKAN', 'siip', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_network`
--

DROP TABLE IF EXISTS `tb_inv_network`;
CREATE TABLE IF NOT EXISTS `tb_inv_network` (
  `id_network` int(20) NOT NULL AUTO_INCREMENT,
  `kode_network` varchar(30) NOT NULL,
  `lokasi` varchar(50) DEFAULT NULL,
  `jenis_network` varchar(50) DEFAULT NULL,
  `spesifikasi` varchar(200) DEFAULT NULL,
  `tgl_inv` date DEFAULT NULL,
  `harga_beli` decimal(20,0) NOT NULL,
  `status` enum('DIGUNAKAN','SIAP DIGUNAKAN','DIPERBAIKI','DIPINJAMKAN','ARSIP/DISIMPAN','RUSAK/NOT FIXABLE','HILANG/DICURI') DEFAULT 'DIGUNAKAN',
  `gid` int(20) DEFAULT NULL,
  PRIMARY KEY (`id_network`) USING BTREE,
  UNIQUE KEY `kode_network` (`kode_network`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_network`
--

INSERT INTO `tb_inv_network` (`id_network`, `kode_network`, `lokasi`, `jenis_network`, `spesifikasi`, `tgl_inv`, `harga_beli`, `status`, `gid`) VALUES
(1, 'DVC-SA-001', 'Server Room F2', 'DVR CCTV Factory 2', '32 Port', '2019-09-26', '12450000', 'SIAP DIGUNAKAN', 1),
(2, 'zxcxczx', 'zxczxczx', 'xzczxc', 'zxczxczxc', '2019-09-26', '0', 'RUSAK/NOT FIXABLE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_inv_printer`
--

DROP TABLE IF EXISTS `tb_inv_printer`;
CREATE TABLE IF NOT EXISTS `tb_inv_printer` (
  `id_printer` int(20) NOT NULL AUTO_INCREMENT,
  `kode_printer` varchar(30) DEFAULT NULL,
  `barcode` varchar(30) NOT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `jenis_printer` enum('DESKJET','LASERJET','DOTMATRIX','ALL-IN','SCANER','FAX') DEFAULT 'DESKJET',
  `spesifikasi` varchar(200) DEFAULT NULL,
  `tgl_inv` date DEFAULT NULL,
  `harga_beli` decimal(20,0) NOT NULL,
  `status` enum('DIGUNAKAN','SIAP DIGUNAKAN','DIPERBAIKI','DIPINJAMKAN','ARSIP/DISIMPAN','RUSAK/NOT FIXABLE','HILANG/DICURI') DEFAULT 'DIGUNAKAN',
  `note` varchar(100) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_printer`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_inv_printer`
--

INSERT INTO `tb_inv_printer` (`id_printer`, `kode_printer`, `barcode`, `id_pengguna`, `jenis_printer`, `spesifikasi`, `tgl_inv`, `harga_beli`, `status`, `note`, `gid`) VALUES
(1, 'PRI-KA1-17001', 'PRI-KA1-17001.png', 'U016.0005', 'DESKJET', 'Epson l210', '2017-02-09', '1200000', 'DIGUNAKAN', NULL, 1),
(2, 'PRI-KA1-17002', 'PRI-KA1-17002.png', 'U016.0002', 'DESKJET', 'Epson L800', '2017-02-04', '3500000', 'DIGUNAKAN', NULL, 1),
(3, 'PRI-SA_F1-19001', '', 'U016.0007', 'LASERJET', 'Epson L645 (print scan copy)', '2019-09-26', '1200000', 'DIGUNAKAN', 'okey bos', 1),
(4, 'PRI-SA_F1-19191', '', 'U016.0001', 'DOTMATRIX', 'dsfsdf', '2019-09-10', '342423', 'DIGUNAKAN', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

DROP TABLE IF EXISTS `tb_jabatan`;
CREATE TABLE IF NOT EXISTS `tb_jabatan` (
  `id_jabatan` int(30) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(50) NOT NULL,
  `jobdes` varchar(200) NOT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jabatan`, `nama_jabatan`, `jobdes`) VALUES
(1, 'DIRECTUR', ''),
(2, 'GENERAL MANAGER', ''),
(3, 'DEPT HEAD', ''),
(4, 'SUB DEPT HEAD', ''),
(5, 'SECTION HEAD', ''),
(6, 'GROUP LEADER', ''),
(7, 'STAFF', ''),
(8, 'ADMIN', ''),
(9, 'OPERATOR', ''),
(10, 'HELP DESK', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

DROP TABLE IF EXISTS `tb_kategori`;
CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE,
  UNIQUE KEY `nama_kategori` (`nama_kategori`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'KOMPUTER'),
(2, 'LAPTOP'),
(3, 'PERIFERAL'),
(4, 'SPAREPART'),
(5, 'NETWORK DEVICE'),
(6, 'MONITOR'),
(7, 'PRINTER'),
(8, 'ALAT/TOOL'),
(9, 'ATK'),
(10, 'HABIS PAKAI');

-- --------------------------------------------------------

--
-- Table structure for table `tb_level`
--

DROP TABLE IF EXISTS `tb_level`;
CREATE TABLE IF NOT EXISTS `tb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lavel` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`) USING BTREE,
  UNIQUE KEY `nama_kategori` (`nama_lavel`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_lavel`) VALUES
(1, 'LOW'),
(2, 'NORMAL'),
(3, 'HIGH');

-- --------------------------------------------------------

--
-- Table structure for table `tb_maintenance`
--

DROP TABLE IF EXISTS `tb_maintenance`;
CREATE TABLE IF NOT EXISTS `tb_maintenance` (
  `no_permohonan` varchar(15) NOT NULL DEFAULT '',
  `tgl_permohonan` datetime DEFAULT NULL,
  `tgl_selesai` datetime DEFAULT NULL,
  `jenis_permohonan` varchar(50) DEFAULT NULL,
  `nama_kategori` varchar(20) DEFAULT NULL,
  `no_inventaris` varchar(20) NOT NULL,
  `catatan_pemohon` varchar(100) NOT NULL,
  `nama_pemohon` varchar(50) NOT NULL DEFAULT 'Admin',
  `catatan_perbaikan` varchar(100) DEFAULT NULL,
  `nama_supplier` varchar(50) DEFAULT NULL,
  `biaya` decimal(10,0) DEFAULT NULL,
  `status` enum('OPEN','PROCESS','PENDING','CLOSED') DEFAULT 'OPEN',
  `level` enum('LOW','NORMAL','HIGH') DEFAULT 'NORMAL',
  `gambar` varchar(50) DEFAULT 'ticket.jpg',
  `gid` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_permohonan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_maintenance`
--

INSERT INTO `tb_maintenance` (`no_permohonan`, `tgl_permohonan`, `tgl_selesai`, `jenis_permohonan`, `nama_kategori`, `no_inventaris`, `catatan_pemohon`, `nama_pemohon`, `catatan_perbaikan`, `nama_supplier`, `biaya`, `status`, `level`, `gambar`, `gid`) VALUES
('T1.0707.0001', '2019-07-07 20:01:29', '0000-00-00 00:00:00', 'Hardware', 'Komputer', 'CPU-KA1-17002', 'Komputer saya error pak restar-restar sendiri minta tolong cek ya,.', 'MAYA', 'ok', 'CV. SURYA JAYA PRATA', '0', 'OPEN', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0002', '2019-07-07 20:02:12', '2019-09-26 00:00:00', 'Software', 'Monitor', 'MON-KA1-17001', 'Monitor ada garisnya dan kadang mati sendiri,.!', 'NURUL', 'sudah bisa,.monitor sudah saya ganti,..', '', '2000', 'OPEN', 'LOW', 'ticket.jpg', 1),
('T1.0707.0003', '2019-07-07 21:00:34', '0000-00-00 00:00:00', 'Hardware', 'Printer', 'PRI-KA1-17001', 'Laptop kelihatannya ada virusnya minta tolong di bersihkan,.', 'EKO', 'sedang proses', 'PT. MITRA INFOSARANA', '0', 'CLOSED', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0004', '2019-07-07 21:00:54', NULL, 'Hardware', 'Printer', 'PRI-KA1-17001', 'Laptop kelihatannya ada virusnya minta tolong di bersihkan,.', 'EKO', NULL, NULL, NULL, 'OPEN', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0005', '2019-07-07 21:01:52', '2019-07-04 15:08:00', 'Software', 'Printer', 'PRI-KA1-17002', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'TINA', 'okey sedang saya carikan sparpart', '', '0', 'PROCESS', 'HIGH', 'ticket.jpg', 1),
('T1.0707.0006', '2019-07-07 21:03:12', NULL, 'Hardware', 'Laptop', 'LAP-KA1-17003', 'Laptop hardisk penuh minta tolong beckup ya pak', 'ANGGA', NULL, NULL, NULL, 'OPEN', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0007', '2019-07-07 21:05:41', '2019-07-06 15:08:00', 'Hardware & Software', 'Komputer', 'CPU-KA1-17002', 'Pak Layar Monitor bermasalah ini gimana', 'LUSI', 'akan saya ganti monitornya nanti,.', '', '0', 'PROCESS', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0008', '2019-07-07 21:07:10', '2019-09-26 00:00:00', 'Hardware', 'Printer', 'PRI-KA1-17001', 'Ptinter tinta habis pak,.bisa diisikan sekarang mau buat ngprint banyak,.', 'TITIN', 'oke', 'Blintzar Computer', '1', 'OPEN', 'NORMAL', 'ticket.jpg', 1),
('T1.0707.0009', '2019-07-07 21:08:45', NULL, 'Hardware', 'Laptop', 'LAP-KA1-17004', 'Batrai laptop drop minta tolong cek downg pak,.', 'FUAD', NULL, NULL, NULL, 'OPEN', 'HIGH', 'ticket.jpg', 1),
('T1.0711.0001', '2019-07-11 16:27:49', NULL, 'Hardware', 'Laptop', 'LAP-KA1-17002', 'Rusak lagi bosku', 'DITA', NULL, NULL, NULL, 'OPEN', 'LOW', 'ticket.jpg', 1),
('T1.0926.0001', '2019-09-26 00:00:00', '2019-09-26 00:00:00', 'Hardware', 'Monitor', 'MON-KA1-17001', 'cek dong', 'Admin', 'oke saya cek dulu', 'PT. MITRA INFOSARANA', '230000', 'PROCESS', 'NORMAL', 'ticket.jpg', 1),
('T1.1007.0001', '2019-10-07 07:09:18', NULL, 'Hardware', 'Komputer', 'CPU-KA1-17001', 'Pak Komputer saya rusak bluescreen', 'BARA', NULL, NULL, NULL, 'OPEN', 'HIGH', 'ticket.jpg', 1),
('T1.1017.0001', '2019-10-17 15:47:34', '0000-00-00 00:00:00', 'Software', 'Laptop', 'LAP-SA_F1-19191', 'Komputer saya tidak bisa terima email pak minta tolong di cek ya ,.', 'BAMBANG', 'Akan saya cek', '', '0', 'PROCESS', 'NORMAL', 'ticket.jpg', 1),
('T1.1024.0001', '2019-10-24 20:15:02', NULL, NULL, 'Komputer', 'CPU-SA_F1-19001', 'test', 'SASONGKO', NULL, NULL, NULL, 'OPEN', 'NORMAL', 'ticket.jpg', 1),
('T2.0926.0001', '2019-09-26 07:46:34', NULL, 'Hardware', 'Komputer', 'CPU-KA2-17001', 'Pak Komputer saya mati,.minta tolong cek ya segera,.', 'EKO', NULL, NULL, NULL, 'OPEN', 'HIGH', 'ticket.jpg', 2),
('T2.1017.0001', '2019-10-17 15:54:45', NULL, NULL, 'Laptop', 'LAP-KA2-17001', 'Pak komputer saya layar garis-garis cek ya,.', 'DITA', NULL, NULL, NULL, 'OPEN', 'HIGH', 'ticket.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_maintenance_detail`
--

DROP TABLE IF EXISTS `tb_maintenance_detail`;
CREATE TABLE IF NOT EXISTS `tb_maintenance_detail` (
  `id_detail` int(50) NOT NULL AUTO_INCREMENT,
  `no_permohonan` varchar(15) NOT NULL DEFAULT '',
  `tgl_proses` datetime DEFAULT NULL,
  `catatan` varchar(100) DEFAULT NULL,
  `status` enum('OPEN','PROCESS','PENDING','CLOSED') DEFAULT 'OPEN',
  `level` enum('LOW','NORMAL','HIGH') DEFAULT 'NORMAL',
  `user` varchar(80) NOT NULL,
  `foto` varchar(50) DEFAULT 'Guest.jpg',
  PRIMARY KEY (`id_detail`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_maintenance_detail`
--

INSERT INTO `tb_maintenance_detail` (`id_detail`, `no_permohonan`, `tgl_proses`, `catatan`, `status`, `level`, `user`, `foto`) VALUES
(40, 'T1.0707.0001', '2019-07-07 20:01:29', 'Komputer saya error pak restar-restar sendiri minta tolong cek ya,.', 'OPEN', 'NORMAL', 'MAYA', 'Guest.jpg'),
(41, 'T1.0707.0002', '2019-07-07 20:02:12', 'Monitor ada garisnya dan kadang mati sendiri,.', 'OPEN', 'NORMAL', 'NURUL', 'Guest.jpg'),
(42, 'T1.0707.0004', '2019-07-07 21:00:54', 'Laptop kelihatannya ada virusnya minta tolong di bersihkan,.', 'OPEN', 'NORMAL', 'EKO', 'Guest.jpg'),
(43, 'T1.0707.0005', '2019-07-07 21:01:52', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'OPEN', 'NORMAL', 'TINA', 'Guest.jpg'),
(44, 'T1.0707.0006', '2019-07-07 21:03:12', 'Laptop hardisk penuh minta tolong beckup ya pak', 'OPEN', 'NORMAL', 'ANGGA', 'Guest.jpg'),
(45, 'T1.0707.0007', '2019-07-07 21:05:41', 'Pak Layar Monitor bermasalah ini gimana', 'OPEN', 'NORMAL', 'LUSI', 'Guest.jpg'),
(46, 'T1.0707.0008', '2019-07-07 21:07:10', 'Ptinter tinta habis pak,.bisa diisikan sekarang mau buat ngprint banyak,.', 'OPEN', 'NORMAL', 'TITIN', 'Guest.jpg'),
(47, 'T1.0707.0009', '2019-07-07 21:08:45', 'Batrai laptop drop minta tolong cek downg pak,.', NULL, 'HIGH', 'FUAD', 'Guest.jpg'),
(48, 'T1.0707.0007', '2019-07-07 21:20:46', 'ganti,.ganti,.hahhah', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(49, 'T1.0711.0001', '2019-07-11 16:27:49', 'Rusak lagi bosku', NULL, 'LOW', 'DITA', 'Guest.jpg'),
(50, 'T1.0711.0001', '2019-09-26 07:38:44', 'oke', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(51, 'T2.0926.0001', '2019-09-26 07:46:34', 'Pak Komputer saya mati,.minta tolong cek ya segera,.', NULL, 'HIGH', 'EKO', 'Guest.jpg'),
(52, 'T1.0926.0001', '2019-09-26 00:00:00', 'cek dong', 'OPEN', 'NORMAL', 'User', 'Guest.jpg'),
(53, 'T1.0707.0007', '2019-09-30 16:47:27', 'Pak Layar Monitor bermasalah ini gimana', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(54, 'T1.0707.0007', '2019-09-30 17:23:48', 'Pak Layar Monitor bermasalah ini gimana', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(55, 'T1.0707.0007', '2019-09-30 17:28:23', 'Pak Layar Monitor bermasalah ini gimana', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(56, 'T1.0707.0005', '2019-09-30 17:32:18', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(57, 'T1.0707.0005', '2019-09-30 17:54:13', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(58, 'T1.0707.0008', '2019-09-30 17:54:33', 'Ptinter tinta habis pak,.bisa diisikan sekarang mau buat ngprint banyak,.', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(59, 'T1.0707.0006', '2019-09-30 17:56:59', 'oke', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(60, 'T1.0707.0008', '2019-10-01 07:27:04', 'okey', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(61, 'T1.0707.0008', '2019-10-01 09:10:56', 'ya segera ya', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(62, 'T1.0707.0005', '2019-10-01 09:42:14', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(63, 'T1.0707.0005', '2019-10-01 09:44:15', 'Pak Printer saya tidak bisa narik kertas minta tolong cek segera ya,..', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(64, 'T1.0707.0009', '2019-10-01 09:55:57', 'ghghgh', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(65, 'T1.0707.0009', '2019-10-01 09:56:25', 'ghghghg', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(66, 'T1.0707.0009', '2019-10-01 09:56:42', 'hjhjhj', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(67, 'T1.0707.0002', '2019-10-01 10:03:32', 'oke', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(68, 'T1.0707.0002', '2019-10-01 10:04:27', 'coba', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(69, 'T1.0707.0002', '2019-10-01 10:06:17', 'coba 2', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(70, 'T1.0707.0002', '2019-10-01 10:06:38', 'coba 3', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(71, 'T1.0707.0002', '2019-10-01 10:08:11', 'oke', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(72, 'T1.0707.0002', '2019-10-01 10:09:03', 'Monitor ada garisnya dan kadang mati sendiri,.!', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(73, 'T1.0707.0002', '2019-10-01 10:10:05', 'Monitor ada garisnya dan kadang mati sendiri,.!', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(74, 'T1.0707.0004', '2019-10-01 10:13:58', 'fdfdf', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(75, 'T1.0707.0002', '2019-10-01 10:14:26', 'okey saya cek sekarang', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(76, 'T1.0707.0002', '2019-10-01 10:14:49', 'siap pak', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(77, 'T1.1007.0001', '2019-10-07 07:09:18', 'Pak Komputer saya rusak bluescreen', NULL, 'HIGH', 'BARA', 'Guest.jpg'),
(78, 'T1.0707.0001', '2019-10-17 12:29:39', 'sama ', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(79, 'T1.0707.0001', '2019-10-17 12:34:53', 'piye\"', 'OPEN', 'NORMAL', 'username', 'Guest.jpg'),
(80, 'T1.0707.0002', '2019-10-17 12:35:46', 'ya', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(81, 'T1.0707.0002', '2019-10-17 12:37:13', 'okey-okey', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(82, 'T1.0707.0001', '2019-10-17 12:50:14', 'yaaa nanti cek\r\n', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(83, 'T1.0707.0001', '2019-10-17 12:52:05', 'okey okey', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(84, 'T1.0707.0001', '2019-10-17 12:55:01', 'oke', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(85, 'T1.0707.0009', '2019-10-17 12:55:32', 'siap', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(86, 'T1.0707.0007', '2019-10-17 12:56:46', 'ya cepat ya', 'OPEN', 'NORMAL', 'me', 'Guest.jpg'),
(87, 'T1.0707.0007', '2019-10-17 12:57:25', 'cepat pak\r\n', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(88, 'T1.0707.0003', '2019-10-17 13:01:02', 'okey', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(89, 'T1.1017.0001', '2019-10-17 15:47:34', 'Komputer saya tidak bisa terima email pak minta tolong di cek ya ,.', NULL, 'NORMAL', 'BAMBANG', 'Guest.jpg'),
(90, 'T1.1017.0001', '2019-10-17 15:53:27', 'ya akan saya cek\r\n', 'OPEN', 'NORMAL', 'Admin', 'Guest.jpg'),
(91, 'T2.1017.0001', '2019-10-17 15:54:45', 'Pak komputer saya layar garis-garis cek ya,.', NULL, 'HIGH', 'DITA', 'Guest.jpg'),
(92, 'T1.0707.0001', '2019-10-21 23:29:54', 'okey', 'OPEN', 'NORMAL', 'Guest', 'Guest.jpg'),
(93, 'T1.1024.0001', '2019-10-24 20:15:02', 'test', NULL, 'NORMAL', 'SASONGKO', 'Guest.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu`
--

DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE IF NOT EXISTS `tb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) NOT NULL,
  `icon` varchar(40) NOT NULL,
  `link` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL,
  `role` enum('Administrator','Staff','Manager','User') DEFAULT 'Administrator',
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `nama_menu`, `icon`, `link`, `parent`, `role`, `aktif`) VALUES
(1, 'Home', 'icon-home', '#', 0, 'User', 'Y'),
(2, 'Master', 'icon-atom', '#', 0, 'Staff', 'Y'),
(3, 'Barang', 'icol-box', 'barang', 2, 'Staff', 'Y'),
(4, 'Supplier', 'icol-user-business', 'supplier', 2, 'Staff', 'Y'),
(5, 'Pengguna', 'icol-user', 'pengguna', 2, 'Staff', 'Y'),
(6, 'Inventory', 'icon-archive', '#', 0, 'Staff', 'Y'),
(7, 'Laptop', 'icol-briefcase', 'laptop', 6, 'Staff', 'Y'),
(8, 'Komputer', 'icol-computer', 'komputer', 6, 'Staff', 'Y'),
(9, 'Monitor', 'icol-monitor', 'monitor', 6, 'Staff', 'Y'),
(10, 'Printer', 'icol-printer', 'printer', 6, 'Staff', 'Y'),
(11, 'Device Support', 'icol-plugin', 'device', 6, 'Staff', 'Y'),
(12, 'Transaksi', 'icon-list', '#', 0, 'Staff', 'Y'),
(13, 'Barang Masuk', 'icol-cart', 'masuk', 12, 'User', 'Y'),
(14, 'Barang Keluar', 'icol-zone-money', 'keluar', 12, 'User', 'Y'),
(16, 'Stok Barang', 'icol-bricks', 'stok', 12, 'User', 'Y'),
(17, 'Ticket', 'icon-paper-airplane', '#', 0, 'Manager', 'Y'),
(19, 'Dedpartemen', 'icol-shape-aling-left', 'departemen', 2, 'Staff', 'Y'),
(20, 'Maintenance', 'icol-application-cascade', 'maintenance', 17, 'Manager', 'Y'),
(22, 'Setting', 'icon-cogs', '#', 0, 'Administrator', 'Y'),
(23, 'Menu Setting', 'icol-color-swatch-2', 'menu', 22, 'Administrator', 'Y'),
(24, 'User Setting', 'icol-group', 'user', 22, 'Administrator', 'Y'),
(25, 'Archived', 'icol-drawer', 'archived', 6, 'User', 'Y'),
(26, 'Group Setting', 'icol-hammer-screwdriver', 'group', 22, 'Administrator', 'Y'),
(27, 'Dashboard', 'icol-eye', 'dashboard/index', 1, 'User', 'Y'),
(28, 'Laporan', 'icon-bars', '#', 0, 'User', 'Y'),
(29, 'All Ticket', 'icol-ticket', 'maintenance/view_saja', 28, 'User', 'Y'),
(30, 'Statistik', 'icol-chart-pie', 'dashboard/statistik', 1, 'User', 'Y'),
(31, 'Penjadwalan', 'icol-calendar-1', 'dashboard/jadwal', 1, 'User', 'Y'),
(32, 'Gallery', 'icon-pictures', '#', 0, 'Manager', 'Y'),
(34, 'Maps Factory', 'icol-application-home', 'foto/maps', 32, 'User', 'Y'),
(35, 'Foto - Foto', 'icol-photo', 'foto/foto', 32, 'User', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengguna`
--

DROP TABLE IF EXISTS `tb_pengguna`;
CREATE TABLE IF NOT EXISTS `tb_pengguna` (
  `id_pengguna` varchar(30) NOT NULL DEFAULT '',
  `nik` varchar(30) NOT NULL,
  `nama_pengguna` varchar(30) NOT NULL,
  `id_dept` int(30) NOT NULL,
  `id_jabatan` int(30) NOT NULL,
  `ruang_kantor` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pengguna`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_pengguna`
--

INSERT INTO `tb_pengguna` (`id_pengguna`, `nik`, `nama_pengguna`, `id_dept`, `id_jabatan`, `ruang_kantor`) VALUES
('U016.0001', '0001', 'Andrian', 42, 1, 'Kontor ICT'),
('U016.0002', '003', 'Joko', 12, 5, 'Kantor HRD'),
('U016.0003', '0003', 'Budi ', 34, 7, 'Kantor Produksi 1'),
('U016.0004', '004', 'Jojon', 13, 6, 'Kantor HRD'),
('U016.0005', '005', 'elia', 48, 7, 'Kontor Sales'),
('U016.0006', '006', 'Sulis', 38, 8, 'Kantor Akunting'),
('U016.0007', '007', 'Tutik', 16, 7, 'Kantor Finishing'),
('U017.0001', '123456', 'BUDI', 57, 3, 'R. Teknik');

-- --------------------------------------------------------

--
-- Table structure for table `tb_status`
--

DROP TABLE IF EXISTS `tb_status`;
CREATE TABLE IF NOT EXISTS `tb_status` (
  `id_status` int(10) NOT NULL AUTO_INCREMENT,
  `nama_status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_status`
--

INSERT INTO `tb_status` (`id_status`, `nama_status`) VALUES
(1, 'DIGUNAKAN'),
(2, 'SIAP DIGUNAKAN'),
(3, 'DIPERBAIKI'),
(4, 'ARSIP/DISIMPAN'),
(5, 'RUSAK/NOT FIXABLE'),
(6, 'HILANG/DICURI'),
(7, 'DIPINJAMKAN');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

DROP TABLE IF EXISTS `tb_supplier`;
CREATE TABLE IF NOT EXISTS `tb_supplier` (
  `id_supplier` int(20) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(50) NOT NULL,
  `alamat_supplier` varchar(100) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  PRIMARY KEY (`id_supplier`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id_supplier`, `nama_supplier`, `alamat_supplier`, `telepon`) VALUES
(1, 'SKI-Komputer', 'Pekalongan', ''),
(2, 'MItra komputer', 'Jl. Agus Salim pekalongan', ''),
(3, 'PT. MITRA INFOSARANA', 'JL. Nginden Semolo 101 Kav.15 ', ''),
(5, 'PT. SGA', 'Surabaya', ''),
(6, 'MS COMPUTER', 'JL. GAJAHMADA PEKALONGAN', ''),
(7, 'Blintzar Computer', 'Jl. Baros Pekalongan', ''),
(8, 'PT BINAREKA TATAMAND', 'JL. Tanah Abang IV No.32 Jakar', ''),
(9, 'PISMA SURABAYA', 'JL. WR.SUPRATMAN', ''),
(10, 'LAZADO', 'JL.PEMUDA NO.91 C SEMARANG', ''),
(11, 'CV. SURYA JAYA PRATA', 'JL. Dr.Sutomo Ruko Grosir MM B', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trans_detail`
--

DROP TABLE IF EXISTS `tb_trans_detail`;
CREATE TABLE IF NOT EXISTS `tb_trans_detail` (
  `id_trans_detail` int(30) NOT NULL,
  `kode_transaksi` varchar(30) DEFAULT NULL,
  `tgl_transaksi` date NOT NULL,
  `kode_barang` varchar(30) DEFAULT NULL,
  `harga` decimal(10,0) DEFAULT NULL,
  `qty_masuk` int(10) DEFAULT NULL,
  `qty_keluar` int(10) DEFAULT NULL,
  `catatan` varchar(100) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `gid` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tb_trans_keluar`
--

DROP TABLE IF EXISTS `tb_trans_keluar`;
CREATE TABLE IF NOT EXISTS `tb_trans_keluar` (
  `id_transaksi` int(30) NOT NULL,
  `kode_transaksi` varchar(30) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `id_pengguna` varchar(30) DEFAULT NULL,
  `gid` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tb_trans_masuk`
--

DROP TABLE IF EXISTS `tb_trans_masuk`;
CREATE TABLE IF NOT EXISTS `tb_trans_masuk` (
  `id_transaksi` int(30) NOT NULL,
  `kode_transaksi` varchar(30) DEFAULT NULL,
  `no_po` varchar(30) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `id_supplier` int(20) DEFAULT NULL,
  `gid` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_trans_masuk`
--

INSERT INTO `tb_trans_masuk` (`id_transaksi`, `kode_transaksi`, `no_po`, `tgl_transaksi`, `id_supplier`, `gid`) VALUES
(0, 'BM-SA_F1-10.001', '45454545', '0000-00-00', 11, 1),
(0, 'BM-SA_F1-10.001', '45454545', '0000-00-00', 11, 1),
(0, 'BM-SA_F1-10.001', '45454545', '0000-00-00', 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `role` enum('Administrator','Staff','Manager','User') COLLATE latin1_general_ci DEFAULT 'User',
  `last_login` datetime NOT NULL,
  `gid` int(11) NOT NULL DEFAULT '1',
  `foto` varchar(25) COLLATE latin1_general_ci DEFAULT 'user.jpg',
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `username`, `password`, `role`, `last_login`, `gid`, `foto`) VALUES
(5, 'Administrator', 'admin', '$2a$08$wXpv1kdQbcbYvOwq2hsjAOBVGhSH.YBPCfNUjPxv4tbpiIpenTH4i', 'Administrator', '2019-10-29 22:15:15', 1, 'admin.png'),
(8, 'Eko Setiawan', 'staff', '$2a$08$TYhQ8Vc41apobneJaJZLduimSFZ6rdxx4j73gCi9lo54I30vJaOQ6', 'Staff', '2019-10-24 21:27:47', 1, 'staff.jpg'),
(14, 'Manager', 'manager', '$2a$08$Yx6n6uRUtC0Eg3nvVEnNZOINPHEOZZgZ1rQmTpy7oHL7YDq5gKtNW', 'Manager', '2019-10-17 12:56:00', 1, 'manager.jpg'),
(15, 'User Saja', 'user', '$2a$08$JhPus4DnAdBkdwJmHmi78uI5p/Ft5wW.nls/Jvfo6tnHbjf0i3DC.', 'User', '2019-10-17 12:55:20', 1, 'user.jpg'),
(21, 'Eko Setiawan', 'eko', '$2a$08$71Q9ZnzzeFcMwD5o8Zbii.r.Lcc5jbWrE/Y5r3qE7xhjuIUX9Njm2', 'Staff', '2019-10-17 16:00:36', 2, 'user.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
