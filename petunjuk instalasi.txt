1. Extrak File SiMASTER.zip yang sudah di copy.
2. Copy folder project tersebut ke xampp->htdocs atau di folder public server anda.
3. Buat database di mysql dengan nama database default dbsimaster.
4. Import file dbsimaster.sql yang ada di folder SiMASTER ke dalam database yang telah dibuat.
5. Buka dan edit file di folder application-> config -> database.php
    'hostname' => 'localhost',
	
    'username' => 'root',
	
    'password' => '',
	
    'database' => 'dbsimaster',
6. Atau sesuaikan username dan password dengan database anda.
7. Jalankan dengan browser (GoogleCrome) ketik http://localhost/simaster. 
8. Selesai
    